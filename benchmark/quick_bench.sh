#!/usr/bin/env bash

########################################################################
# This bash script is a one-click-do-all solution for running the
# 'run_benchmark.sh' benchmarks in multiple configurations.
# Keep an eye on the output for the first minute where the execution
# environment will be tested for compatibility.
#
# The latest available source tag will be used for benchmarking.
#
# There are no command line arguments. Instead it is required to set
# three variables. 'WDIR' and 'SAVEDIR' are the paths to the working
# directory and output directory respectively. The paths will not be
# expanded (eg.: '~' will not translate to the user home). 'VERSION'
# has to be set to a valid existing tag of the repository.
########################################################################


# TODO: change this values according to your preferences
# the working directory, "scratch space" the running benchmark will use
WDIR="REPLACE/ME"
# the save location for the benchmark results
SAVEDIR="REPLACE/ME"
# the version tag for the source code to use for the benchmark
VERSION="latest"

# exit script on error
set -eE

START=$(date +%Y-%m-%d_%H-%M-%S)

echo -e "\n####################\nStarting 'quick_bench' now!\n####################\n\n"

# keep always uncommented!
# run --test flag for environment compatibility
./run_benchmark.sh -w "${WDIR}" -o "${SAVEDIR}" -s "${VERSION}" -d cuda --test

sleep 5
echo -e "\n\nstart: ${START}\nnow: $(date +%Y-%m-%d_%H-%M-%S)"
echo -e "\n####################\nBENCH SETUP TEST DONE!\n####################\n\n"
sleep 5

# run benchmark with compute device set to cuda (GPU accelerated)
# COMMENT TO SKIP
./run_benchmark.sh -w "${WDIR}" -o "${SAVEDIR}" -s "${VERSION}" -d cuda

sleep 5
echo -e "\n\nstart: ${START}\nnow: $(date +%Y-%m-%d_%H-%M-%S)"
echo -e "\n####################\nCUDA BENCH DONE!\n####################\n\n"
sleep 5

# run benchmark with compute device set to numba (CPU only)
# COMMENT TO SKIP
./run_benchmark.sh -w "${WDIR}" -o "${SAVEDIR}" -s "${VERSION}" -d numba

sleep 5
echo -e "\n\nstart: ${START}\nnow: $(date +%Y-%m-%d_%H-%M-%S)"
echo -e "\n####################\nNumba BENCH DONE!\n####################\n\n"
sleep 5
