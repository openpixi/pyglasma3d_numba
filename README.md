# Pyglasma3d_Numba
[![pipeline status](https://gitlab.com/openpixi/pyglasma3d_numba/badges/master/pipeline.svg)](https://gitlab.com/openpixi/pyglasma3d_numba/commits/master)
[![coverage report](https://gitlab.com/openpixi/pyglasma3d_numba/badges/master/coverage.svg)](https://gitlab.com/openpixi/pyglasma3d_numba/commits/master)
[![wiki](https://img.shields.io/badge/wiki-GitLab%20Pages-ff69b4)](https://openpixi.gitlab.io/pyglasma3d_numba/)
[![release](https://img.shields.io/badge/dynamic/json?color=blue&label=release&query=%24%5B0%5D.name&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F20930583%2Freleases)](https://gitlab.com/openpixi/pyglasma3d_numba/-/releases)


### A 3+1D Glasma simulation implemented in Python with parallelization and GPU acceleration using the [Numba][numba-ref] framework.

This code is a port of [pyglasma3d](https://gitlab.com/monolithu/pyglasma3d).
The ported feature set is limited to:
- SU(2) gauge group
- McLerran-Venugopalan model initial conditions without randomness in the longitudinal extent
- Leap-frog solver for Yang-Mills fields on a lattice and nearest-grid-point (NGP) interpolation for color currents
- Computing various energy-momentum tensor components
- CPU parallelization using [Numba][numba-ref]
- GPU parallelization using [Numba][numba-ref] (limited to NVIDIA GPUs)

#### Full documentation and help is located [here](https://openpixi.gitlab.io/pyglasma3d_numba/).

---


## Quick-start guide

1. Install dependencies:
   - for [conda][conda-ref] environments use:  
     ```
     $ conda env create -f conda-environment.yml
     $ conda activate pyglasma3d_numba
     ```
   - for [pip][pip-ref] Python venvs use:  
     ```
     $ python3 -m venv .venv
     $ source .venv/bin/activate
     $ pip install -r pip-requirements.txt
     ```
   - GPU acceleration on NVIDIA GPUs additionally requires the [proprietary driver](https://www.nvidia.com/en-us/drivers/unix/) and the [CUDA tookit](https://developer.nvidia.com/cuda-toolkit) (already included in `conda-environment.yml`)  
     <br>

2. Launch a setup:
   - From the root directory of the repository launch the setup module:  
     ```
     $ python3 -m examples.mv [options]
     ```
   - To list all available command line arguments use:  
     ```
     $ python3 -m examples.mv --help
     ```


## Roadmap

  - v0.6  
    full documentation of what is implemented done with GitLab Pages  
    code documentation generated with [pdoc](https://pdoc3.github.io/pdoc/) and included in wiki

  - v0.X  
    optimizations:  
      - interpolate charges loops fixed  
      - fix for em tensor parallelization  
      - optimal init on cupy  
        <br>

  - v1.0  
    final optimizations,  
    perfect code

  - v1.X / vX.X  
    adding more features from the original pyglasma3d code  
    porting necessary modules to python


[//]: # (references: )  
[numba-ref]: http://numba.pydata.org/
[conda-ref]: https://docs.conda.io/en/latest/
[pip-ref]: https://packaging.python.org/tutorials/installing-packages/
