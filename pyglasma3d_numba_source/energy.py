"""Compute the components of the energy-momentum tensor.

`energy_momentum_tensor` returns arrays with:

- EL: Longitudinal color-electric field avg over each transverse plane.
- BL: Longitudinal color-magnetic field avg over each transverse plane.
- ET: Transverse color-electric field avg over each transverse plane.
- BT: Transverse color-magnetic field avg over each transverse plane.
- SL: Longitudinal Poynting Vector part avg over each transverse plane.

All values of the arrays are comparable for the same transverse plane.
"""

# TODO: this code is partially complete
#       it does contain everything necessary for the leapfrog solver
#       with mv initial conditions but lacks other code used for other
#       solvers and initial conditions


import numpy as np

from . import lattice
from .numba_target import default_jit
from .parallel import make_parallel_loop, set_field_zero_kernel


@default_jit
def electric_sq(electric, x, d):
    """Squared electric field component."""

    field_index = lattice.get_field_index(x, d)
    return lattice.algebra_sq(
        (electric[field_index], electric[field_index + 1], electric[field_index + 2])
    )


@default_jit
def magnetic_sq5(links0, links1, x, d, dims, acc):
    """Squared magnetic field component (fifth variant)."""

    d1 = lattice.mod(d + 1, 3)
    d2 = lattice.mod(d + 2, 3)

    plaq_buffer0 = lattice.plaq_pos(links0, x, d1, d2, dims, acc)
    proj_buffer0 = lattice.proj(plaq_buffer0)

    plaq_buffer1 = lattice.plaq_pos(links1, x, d1, d2, dims, acc)
    proj_buffer1 = lattice.proj(plaq_buffer1)

    return 0.5 * (lattice.algebra_sq(proj_buffer0) + lattice.algebra_sq(proj_buffer1))


@default_jit
def poynting3(links0, links1, electric, x, dims, acc):
    """Poynting vector on Yee-grid (different way to average B)."""

    # init result
    result = 0.0

    # E_y * B_z
    f_i = lattice.get_field_index(x, 1)

    buffer0 = lattice.plaq_pos(links0, x, 1, 0, dims, acc)
    buffer1 = lattice.proj(buffer0)
    result -= lattice.algebra_dot(
        (electric[f_i], electric[f_i + 1], electric[f_i + 2]), buffer1
    )

    buffer0 = lattice.plaq_neg(links0, x, 1, 0, dims, acc)
    buffer1 = lattice.proj(buffer0)
    result += lattice.algebra_dot(
        (electric[f_i], electric[f_i + 1], electric[f_i + 2]), buffer1
    )

    buffer0 = lattice.plaq_pos(links1, x, 1, 0, dims, acc)
    buffer1 = lattice.proj(buffer0)
    result -= lattice.algebra_dot(
        (electric[f_i], electric[f_i + 1], electric[f_i + 2]), buffer1
    )

    buffer0 = lattice.plaq_neg(links1, x, 1, 0, dims, acc)
    buffer1 = lattice.proj(buffer0)
    result += lattice.algebra_dot(
        (electric[f_i], electric[f_i + 1], electric[f_i + 2]), buffer1
    )

    # E_z * B_y
    f_i = lattice.get_field_index(x, 2)

    buffer0 = lattice.plaq_pos(links0, x, 2, 0, dims, acc)
    buffer1 = lattice.proj(buffer0)
    result -= lattice.algebra_dot(
        (electric[f_i], electric[f_i + 1], electric[f_i + 2]), buffer1
    )

    buffer0 = lattice.plaq_neg(links0, x, 2, 0, dims, acc)
    buffer1 = lattice.proj(buffer0)
    result += lattice.algebra_dot(
        (electric[f_i], electric[f_i + 1], electric[f_i + 2]), buffer1
    )

    buffer0 = lattice.plaq_pos(links1, x, 2, 0, dims, acc)
    buffer1 = lattice.proj(buffer0)
    result -= lattice.algebra_dot(
        (electric[f_i], electric[f_i + 1], electric[f_i + 2]), buffer1
    )

    buffer0 = lattice.plaq_neg(links1, x, 2, 0, dims, acc)
    buffer1 = lattice.proj(buffer0)
    result += lattice.algebra_dot(
        (electric[f_i], electric[f_i + 1], electric[f_i + 2]), buffer1
    )

    return 0.25 * result


@default_jit
def shift_array(a):
    """Avg adjacent array elements.

    Return avgs in array with last element as avg of `a[-1]` and `a[0]`.
    """
    return (a + np.roll(a, -1)) / 2.0


def energy_momentum_tensor(sim):
    """Calculate EM tensor components as avg over each transverse plane.

    Returns:
        Tuple of (EL, BL, ET, BT, SL) for the EM tensor components where
        each element is an array with the values for each plane.
    """
    _, _, ny0, ny1, nz0, nz1 = sim.iter_dims
    nx = sim.dims[0] - 2

    # set field aliases to enable device memory control for
    # returning values to host
    EL = sim.EL
    BL = sim.BL
    ET = sim.ET
    BT = sim.BT
    SL = sim.SL

    # field.size == nx
    set_field_zero_kernel(0, nx, EL)
    set_field_zero_kernel(0, nx, BL)
    set_field_zero_kernel(0, nx, ET)
    set_field_zero_kernel(0, nx, BT)
    set_field_zero_kernel(0, nx, SL)

    energy_momentum_tensor_kernel_1(
        1,
        nx + 1,
        ny0,
        ny1,
        nz0,
        nz1,
        sim.dims,
        sim.acc,
        sim.u0,
        sim.u1,
        sim.e,
        EL,
        BL,
        ET,
        BT,
        SL,
    )

    plane_avg_factor = 1.0 / (sim.dims[1] * sim.dims[2])
    g2 = sim.g * sim.g
    aL = sim.a[0]
    aT = sim.a[1]

    EL_factor = 0.5 / (g2 * aL ** 2)
    BL_factor = 0.5 / (g2 * aT ** 4)
    ET_factor = 0.5 / (g2 * aT ** 2)
    BT_factor = 0.5 / (g2 * (aL * aT) ** 2)
    SL_factor = 1.0 / (g2 * aT * aT * aL)

    energy_momentum_tensor_kernel_2(
        0,
        nx,
        EL,
        BL,
        ET,
        BT,
        SL,
        EL_factor,
        BL_factor,
        ET_factor,
        BT_factor,
        SL_factor,
        plane_avg_factor,
    )

    # copy to host
    if sim.use_device == "cuda":
        EL = EL.copy_to_host()
        BL = BL.copy_to_host()
        ET = ET.copy_to_host()
        BT = BT.copy_to_host()
        SL = SL.copy_to_host()

    return EL, shift_array(BL), shift_array(ET), BT, shift_array(SL)


@make_parallel_loop
@default_jit
def energy_momentum_tensor_kernel_1(
    ix,
    ny0,
    ny1,
    nz0,
    nz1,
    dims,
    acc,
    links0,
    links1,
    electric,
    EL,
    BL,
    ET,
    BT,
    SL,
):
    """`pyglasma3d_numba_source.parallel.make_parallel_loop`\
    CHANGES THE USAGE AND ARGUMENTS!.

    Signature:
    ```
        energy_momentum_tensor_kernel_1(
            iter_min, iter_max, ny0, ny1, nz0, nz1, dims, acc, links0,
            links1, electric, EL, BL, ET, BT, SL, force_dev=None
        )
    ```
    """
    for iy in range(ny0, ny1):
        for iz in range(nz0, nz1):
            x = lattice.get_index_nm(ix, iy, iz, dims)
            EL[ix - 1] += electric_sq(electric, x, 0)
            BL[ix - 1] += magnetic_sq5(links0, links1, x, 0, dims, acc)

            SL[ix - 1] += poynting3(
                links0,
                links1,
                electric,
                x,
                dims,
                acc,
            )

            for d in range(1, 3):
                ET[ix - 1] += electric_sq(electric, x, d)
                BT[ix - 1] += magnetic_sq5(
                    links0,
                    links1,
                    x,
                    d,
                    dims,
                    acc,
                )


@make_parallel_loop
@default_jit
def energy_momentum_tensor_kernel_2(
    ix,
    EL,
    BL,
    ET,
    BT,
    SL,
    EL_factor,
    BL_factor,
    ET_factor,
    BT_factor,
    SL_factor,
    plane_avg_factor,
):
    """`pyglasma3d_numba_source.parallel.make_parallel_loop`\
        CHANGES THE USAGE AND ARGUMENTS!.

    Signature:
    ```
        energy_momentum_tensor_kernel_2(
            iter_min, iter_max, EL, BL, ET, BT, SL, EL_factor,
            BL_factor, ET_factor, BT_factor, SL_factor,
            plane_avg_factor, force_dev=None
        )
    ```
    """
    EL[ix] *= plane_avg_factor * EL_factor
    BL[ix] *= plane_avg_factor * BL_factor
    ET[ix] *= plane_avg_factor * ET_factor
    BT[ix] *= plane_avg_factor * BT_factor
    SL[ix] *= plane_avg_factor * SL_factor
