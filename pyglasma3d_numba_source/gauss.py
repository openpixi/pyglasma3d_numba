"""Gauss constraint functions."""

# TODO: this code is partially complete
#       it does contain everything necessary for the leapfrog solver
#       with mv initial conditions but lacks other code used for other
#       solvers and initial conditions


import math

from . import lattice
from .numba_target import default_jit
from .parallel import make_parallel_loop


def gauss_density(links, electric, rho, spacings, dims, acc, iter_dims, g, result):
    """Compute the gauss constraint at each lattice point.

    Write the results to the `result` array.

    .. note::This functions uses `force_dev='numba'` for parallel loops!
    """
    nx0, nx1, ny0, ny1, nz0, nz1 = iter_dims

    # this writes directly to the 'result' array
    # parallelize along x
    gauss_density_kernel(
        nx0 + 1,
        nx1 - 1,
        ny0,
        ny1,
        nz0,
        nz1,
        links,
        electric,
        rho,
        spacings,
        dims,
        acc,
        g,
        result,
        force_dev="numba",
    )


@make_parallel_loop
@default_jit
def gauss_density_kernel(
    ix,
    ny0,
    ny1,
    nz0,
    nz1,
    links,
    electric,
    rho,
    spacings,
    dims,
    acc,
    g,
    result,
):
    """`pyglasma3d_numba_source.parallel.make_parallel_loop`\
    CHANGES THE USAGE AND ARGUMENTS!.

    Signature:
    ```
        gauss_density_kernel(
            iter_min, iter_max, ny0, ny1, nz0, nz1, links, electric,
            rho, spacings, dims, acc, g, result, force_dev=None
        )
    ```
    """
    for iy in range(ny0, ny1):
        for iz in range(nz0, nz1):
            x = lattice.get_index_nm(ix, iy, iz, dims)
            (result[3 * x], result[3 * x + 1], result[3 * x + 2]) = gauss(
                links,
                electric,
                rho,
                x,
                spacings,
                dims,
                acc,
                g,
            )


# FIXME: is docstring correct?, together with other fixmes
@default_jit
def gauss(links, electric, rho, x, spacings, dims, acc, g):
    """Compute gauss constraint.

    Return GC value as an algebra element at lattice point `x`.
    Only works if supplied arrays reside in host memory. Using pointers
    to device memory will fail.
    """
    r = lattice.algebra_zero()

    for d in range(3):

        factor = 1.0 / (g * spacings[d] * spacings[d])
        field_index = lattice.get_field_index(x, d)

        buffer0 = (
            electric[field_index + 0],
            electric[field_index + 1],
            electric[field_index + 2],
        )

        buffer1 = lattice.transport_neg(links, electric, x, d, dims, acc)
        buffer0 = lattice.algebra_add(buffer0, buffer1, -1)

        r = lattice.algebra_add(r, buffer0, factor)

    return lattice.algebra_add(r, (rho[3 * x], rho[3 * x + 1], rho[3 * x + 2]), -1)


# compute gauss constraint squared at a certain lattice point x
# this function does only get used for the unittests, its not part of
# the actual simulation code
# TODO: _h limitation fixes?
def gauss_sq(sim, x):
    """Squared gauss constraint at lattice point `x`.

    Works on host arrays (`_h` suffix)!
    If using CUDA for the current run make sure to use:
    `pyglasma3d_numba_source.core.Simulation.copy_to_host()`
    prior to using this function and afterwards:
    `pyglasma3d_numba_source.core.Simulation.copy_to_device()`
    """
    # gauss() cannot work with cuda ndarrays on device memory
    # so use host arrays
    buffer = gauss(sim.u1_h, sim.e_h, sim.r_h, x, sim.a, sim.dims, sim.acc, sim.g)

    return lattice.algebra_sq(buffer)


def gauss_sq_total(sim):
    """Compute squared gauss constraint for the simulation box."""

    nx0, nx1, ny0, ny1, nz0, nz1 = sim.iter_dims

    result = gauss_sq_total_kernel(
        nx0 + 1,
        nx1 - 1,
        ny0,
        ny1,
        nz0,
        nz1,
        sim.dims,
        sim.u0,
        sim.e,
        sim.r,
        sim.a,
        sim.acc,
        sim.g,
    )

    return result / (sim.dims[0] * sim.dims[1] * sim.dims[2])


@make_parallel_loop(sums=1)
@default_jit
def gauss_sq_total_kernel(
    ix,
    ny0,
    ny1,
    nz0,
    nz1,
    dims,
    links0,
    electric,
    rho,
    spacings,
    acc,
    g,
):
    """`pyglasma3d_numba_source.parallel.make_parallel_loop`\
    CHANGES THE USAGE AND ARGUMENTS!.

    Signature:
    ```
        gauss_sq_total_kernel(
            iter_min, iter_max, ny0, ny1, nz0, nz1, dims, links0,
            electric, rho, spacings, acc, g, force_dev=None
        ) -> float
    ```
    """
    result = 0.0
    for iy in range(ny0, ny1):
        for iz in range(nz0, nz1):
            x = lattice.get_index_nm(ix, iy, iz, dims)
            buff = gauss(
                links0,
                electric,
                rho,
                x,
                spacings,
                dims,
                acc,
                g,
            )
            result += lattice.algebra_sq(buff)
    return result


# FIXME: partly duplicate of gauss(), except for minus rho
@default_jit
def de(links, electric, x, spacings, dims, acc, g):
    """Compute divE."""

    result = lattice.algebra_zero()
    for d in range(3):
        factor = 1.0 / (g * spacings[d] * spacings[d])
        field_index = lattice.get_field_index(x, d)
        buffer0 = (
            electric[field_index],
            electric[field_index + 1],
            electric[field_index + 2],
        )
        buffer1 = lattice.transport_neg(links, electric, x, d, dims, acc)
        buffer0 = lattice.algebra_add(buffer0, buffer1, -1)
        result = lattice.algebra_add(result, buffer0, factor)

    return result


# FIXME: the calculations here are wrong
def rel_gauss_violation(sim):
    """Compute relative gauss violation for the simulation box."""

    nx0, nx1, ny0, ny1, nz0, nz1 = sim.iter_dims

    DE_sq, rho_sq = rel_gauss_violation_kernel(
        nx0 + 1,
        nx1 - 1,
        ny0,
        ny1,
        nz0,
        nz1,
        sim.dims,
        sim.u0,
        sim.e,
        sim.a,
        sim.acc,
        sim.g,
        sim.r,
    )

    result = math.sqrt(math.fabs(DE_sq - rho_sq) / rho_sq)
    return result, math.sqrt(DE_sq), math.sqrt(rho_sq)


# FIXME: the calculations here are wrong
@make_parallel_loop(sums=2)
@default_jit
def rel_gauss_violation_kernel(
    ix,
    ny0,
    ny1,
    nz0,
    nz1,
    dims,
    links0,
    electric,
    spacings,
    acc,
    g,
    rho,
):
    """`pyglasma3d_numba_source.parallel.make_parallel_loop`\
    CHANGES THE USAGE AND ARGUMENTS!.

    Signature:
    ```
        rel_gauss_violation_kernel(
            iter_min, iter_max, ny0, ny1, nz0, nz1, dims, links0,
            electric, spacings, acc, g, rho, force_dev=None
        ) -> tuple(float, float)
    ```
    """
    DE_sq = 0.0
    rho_sq = 0.0
    for iy in range(ny0, ny1):
        for iz in range(nz0, nz1):
            x = lattice.get_index_nm(ix, iy, iz, dims)
            buff = de(
                links0,
                electric,
                x,
                spacings,
                dims,
                acc,
                g,
            )
            DE_sq += lattice.algebra_sq(buff)
            rho_sq += lattice.algebra_sq((rho[3 * x], rho[3 * x + 1], rho[3 * x + 2]))

    return DE_sq, rho_sq
