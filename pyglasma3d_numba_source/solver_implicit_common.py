"""Operations for implicit solvers.

A module implementing various 'common' operations for implicit solvers.
In particular:
The initial step from leapfrog, but with the fields used in the
SimulationImplicit class (i.e. u0, u1, u2, e0, e1) and the evolution
step of the links, which is the same for the implicit, semi-implicit
and the semi-implicit (early) solver.
"""

# TODO: this code is partially complete
#       it does contain everything necessary for the leapfrog solver
#       with mv intital conditions but lacks other code used for other
#       solvers and initial conditions

from . import lattice
from .numba_target import default_jit
from .parallel import make_parallel_loop


def evolve_initial(sim):
    """Perform combined field and link update."""
    # TODO: this code does not get used
    # the parallel version might be slower with cuda

    inv_dt = -1 / sim.dt
    nx0, nx1, ny0, ny1, nz0, nz1 = sim.iter_dims

    # parallelize along x direction
    evolve_u_inv_kernel(
        nx0,
        nx1,
        ny0,
        ny1,
        nz0,
        nz1,
        sim.dims,
        sim.u0,
        sim.u1,
        sim.e,
        inv_dt,
    )


@make_parallel_loop
@default_jit(parallel=False)
def evolve_u_inv_kernel(ix, ny0, ny1, nz0, nz1, dims, links0, links1, electric, inv_dt):
    """`pyglasma3d_numba_source.parallel.make_parallel_loop`\
    CHANGES THE USAGE AND ARGUMENTS!.

    Signature:
    ```
        evolve_u_inv_kernel(
            iter_min, iter_max, ny0, ny1, nz0, nz1, dims, links0,
            links1, electric, inv_dt, force_dev=None
        )
    ```
    """
    for iy in range(ny0, ny1):
        for iz in range(nz0, nz1):
            cell_index = lattice.get_index_nm(ix, iy, iz, dims)
            for d in range(3):
                field_index = lattice.get_field_index(cell_index, d)
                group_index = lattice.get_group_index(cell_index, d)
                buffer = lattice.mul2(
                    (
                        links1[group_index],
                        links1[group_index + 1],
                        links1[group_index + 2],
                        links1[group_index + 3],
                    ),
                    (
                        links0[group_index],
                        links0[group_index + 1],
                        links0[group_index + 2],
                        links0[group_index + 3],
                    ),
                    1,
                    -1,
                )
                (
                    electric[field_index],
                    electric[field_index + 1],
                    electric[field_index + 2],
                ) = lattice.proj(buffer)

                (
                    electric[field_index],
                    electric[field_index + 1],
                    electric[field_index + 2],
                ) = lattice.algebra_mul(
                    (
                        electric[field_index],
                        electric[field_index + 1],
                        electric[field_index + 2],
                    ),
                    inv_dt,
                )


def evolve_u_inv2_not_parallel(sim):
    """Perform combined field and link update.

    Not parallelized version of
    `pyglasma3d_numba_source.solver_implicit_common.evolve_u_inv_kernel`
    """
    inv_dt = -1 / sim.dt
    nx0, nx1, ny0, ny1, nz0, nz1 = sim.iter_dims

    for ix in range(nx0, nx1):
        for iy in range(ny0, ny1):
            for iz in range(nz0, nz1):
                cell_index = lattice.get_index_nm(ix, iy, iz, sim.dims)
                for d in range(3):
                    field_index = lattice.get_field_index(cell_index, d)
                    group_index = lattice.get_group_index(cell_index, d)
                    buffer = lattice.mul2(
                        (
                            sim.u1[group_index],
                            sim.u1[group_index + 1],
                            sim.u1[group_index + 2],
                            sim.u1[group_index + 3],
                        ),
                        (
                            sim.u0[group_index],
                            sim.u0[group_index + 1],
                            sim.u0[group_index + 2],
                            sim.u0[group_index + 3],
                        ),
                        1,
                        -1,
                    )

                    (
                        sim.e[field_index],
                        sim.e[field_index + 1],
                        sim.e[field_index + 2],
                    ) = lattice.proj(buffer)

                    (
                        sim.e[field_index],
                        sim.e[field_index + 1],
                        sim.e[field_index + 2],
                    ) = lattice.algebra_mul(
                        (
                            sim.e[field_index],
                            sim.e[field_index + 1],
                            sim.e[field_index + 2],
                        ),
                        inv_dt,
                    )
