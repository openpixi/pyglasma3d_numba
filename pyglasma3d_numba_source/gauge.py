"""Gauge transformations.

This code is not used by the current simulation setups.
It is only used for the unit tests.
"""


from . import lattice
from .numba_target import default_jit
from .parallel import make_parallel_loop


def apply_gauge(sim, gauge):
    """Apply gauge transformation."""

    n = sim.dims[0] * sim.dims[1] * sim.dims[2]
    gauge_kernel(0, n, sim.e, sim.u0, sim.u1, gauge, sim.dims, sim.acc)


@make_parallel_loop
@default_jit
def gauge_kernel(x, e, u0, u1, gauge, dims, acc):
    """`pyglasma3d_numba_source.parallel.make_parallel_loop`\
    CHANGES THE USAGE AND ARGUMENTS!.

    Signature:
    ```
        gauge_kernel(
            iter_min, iter_max, e, u0, u1, gauge, dims, acc,
            force_dev=None
        )
    ```
    """
    for i in range(3):
        (
            e[3 * (3 * x + i)],
            e[3 * (3 * x + i) + 1],
            e[3 * (3 * x + i) + 2],
        ) = lattice.act(
            (gauge[4 * x], gauge[4 * x + 1], gauge[4 * x + 2], gauge[4 * x + 3]),
            (e[3 * (3 * x + i)], e[3 * (3 * x + i) + 1], e[3 * (3 * x + i) + 2]),
            1,
        )

        xs = lattice.shift2(x, i, 1, dims, acc)
        buffer = lattice.mul2(
            (gauge[4 * x], gauge[4 * x + 1], gauge[4 * x + 2], gauge[4 * x + 3]),
            (
                u0[4 * (3 * x + i)],
                u0[4 * (3 * x + i) + 1],
                u0[4 * (3 * x + i) + 2],
                u0[4 * (3 * x + i) + 3],
            ),
            -1,
            1,
        )
        (
            u0[4 * (3 * x + i)],
            u0[4 * (3 * x + i) + 1],
            u0[4 * (3 * x + i) + 2],
            u0[4 * (3 * x + i) + 3],
        ) = lattice.mul2(
            buffer,
            (gauge[4 * xs], gauge[4 * xs + 1], gauge[4 * xs + 2], gauge[4 * xs + 3]),
            1,
            1,
        )
        buffer = lattice.mul2(
            (gauge[4 * x], gauge[4 * x + 1], gauge[4 * x + 2], gauge[4 * x + 3]),
            (
                u1[4 * (3 * x + i)],
                u1[4 * (3 * x + i) + 1],
                u1[4 * (3 * x + i) + 2],
                u1[4 * (3 * x + i) + 3],
            ),
            -1,
            1,
        )
        (
            u1[4 * (3 * x + i)],
            u1[4 * (3 * x + i) + 1],
            u1[4 * (3 * x + i) + 2],
            u1[4 * (3 * x + i) + 3],
        ) = lattice.mul2_fast(
            buffer,
            (gauge[4 * xs], gauge[4 * xs + 1], gauge[4 * xs + 2], gauge[4 * xs + 3]),
        )
