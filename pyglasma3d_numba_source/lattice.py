"""Functions for working on the lattice."""

import math

from numba.typed import List

from .numba_target import default_jit


# SU(2) group & algebra functions


@default_jit
def mul2(a, b, ta, tb):
    """Multiply `a` and `b` with signs `ta` and `tb`."""

    r0 = (
        a[0] * b[0]
        - (ta * a[1]) * (tb * b[1])
        - (ta * a[2]) * (tb * b[2])
        - (ta * a[3]) * (tb * b[3])
    )
    r1 = (
        (ta * a[1]) * b[0]
        + a[0] * (tb * b[1])
        + (ta * a[3]) * (tb * b[2])
        - (ta * a[2]) * (tb * b[3])
    )
    r2 = (
        (ta * a[2]) * b[0]
        - (ta * a[3]) * (tb * b[1])
        + a[0] * (tb * b[2])
        + (ta * a[1]) * (tb * b[3])
    )
    r3 = (
        (ta * a[3]) * b[0]
        + (ta * a[2]) * (tb * b[1])
        - (ta * a[1]) * (tb * b[2])
        + a[0] * (tb * b[3])
    )

    return r0, r1, r2, r3


@default_jit
def mul2_fast(a, b):
    """Multiply `a` and `b`."""

    r0 = a[0] * b[0] - a[1] * b[1] - a[2] * b[2] - a[3] * b[3]
    r1 = a[1] * b[0] + a[0] * b[1] + a[3] * b[2] - a[2] * b[3]
    r2 = a[2] * b[0] - a[3] * b[1] + a[0] * b[2] + a[1] * b[3]
    r3 = a[3] * b[0] + a[2] * b[1] - a[1] * b[2] + a[0] * b[3]

    return r0, r1, r2, r3


@default_jit
def mul4(a, b, c, d, ta, tb, tc, td):
    """Multiply `a`, `b`, `c`, `d` with signs `ta`, `tb`, `tc`, `td`."""

    r1 = mul2(a, b, ta, tb)
    r2 = mul2(c, d, tc, td)

    return mul2_fast(r1, r2)


@default_jit
def mexp(a, f):
    """Matrix exponent."""

    norm = math.sqrt(a[0] * a[0] + a[1] * a[1] + a[2] * a[2])

    if norm > 10e-18:

        sin_factor = math.sin(f * norm * 0.5)
        r0 = math.cos(f * norm * 0.5)
        r1 = sin_factor * a[0] / norm
        r2 = sin_factor * a[1] / norm
        r3 = sin_factor * a[2] / norm
    else:

        r0 = math.sqrt(1.0 - 0.25 * norm * norm)
        r1 = 0.5 * a[0]
        r2 = 0.5 * a[1]
        r3 = 0.5 * a[2]

    return r0, r1, r2, r3


@default_jit
def proj(u):
    """Projection."""

    r0 = 2.0 * u[1]
    r1 = 2.0 * u[2]
    r2 = 2.0 * u[3]

    return r0, r1, r2


@default_jit
def mlog(u):
    """Matrix logarithm."""

    norm = 1.0 - u[0] * u[0]

    if norm > 10e-18:

        # norm = math.sqrt(norm)
        acosf = 2.0 * math.acos(u[0]) / math.sqrt(norm)

        r0 = acosf * u[1]
        r1 = acosf * u[2]
        r2 = acosf * u[3]

        return r0, r1, r2
    # else:
    return proj(u)


@default_jit
def act(u, a, t):
    r"""Calculate \(u^{\dagger} a u\)."""

    buffer1 = (0.0, a[0] * 0.5, a[1] * 0.5, a[2] * 0.5)
    buffer2 = mul2(u, buffer1, -t, 1)
    buffer1 = mul2(buffer2, u, 1, t)

    return proj(buffer1)


# Useful functions for temporary fields
# (setting to zero, unit and addition, ...)


@default_jit
def zero():
    """Return zero group element."""
    return 0.0, 0.0, 0.0, 0.0


# this function does not get used
@default_jit
def unit():
    """Return unit group element."""
    return 1.0, 0.0, 0.0, 0.0


@default_jit
def add(g0, g1, f):
    """Return group addition."""

    r0 = g0[0] + g1[0] * f
    r1 = g0[1] + g1[1] * f
    r2 = g0[2] + g1[2] * f
    r3 = g0[3] + g1[3] * f

    return r0, r1, r2, r3


@default_jit
def load(g1):
    """Return group element."""
    # = group_set in lattice.pyx (pyglasma3d cython)
    # = load in su2.py & su3.py (curraun)

    return g1[0], g1[1], g1[2], g1[3]


@default_jit
def dagger(g0):
    """Return hermetian conjugate."""
    # dagger = hc in lattice.pyx

    return g0[0], -g0[1], -g0[2], -g0[3]


@default_jit
def algebra_zero():
    """Return zero algebra element."""
    return 0.0, 0.0, 0.0


@default_jit
def algebra_add(a0, a1, f):
    """Return algebra addition."""
    return a0[0] + a1[0] * f, a0[1] + a1[1] * f, a0[2] + a1[2] * f


@default_jit
def algebra_mul(a, f):
    """Return algebra multiplication."""
    return a[0] * f, a[1] * f, a[2] * f


@default_jit
def algebra_sq(a):
    """Return algebra square."""
    return a[0] * a[0] + a[1] * a[1] + a[2] * a[2]


@default_jit
def algebra_dot(a, b):
    """Return algebra dot."""
    return a[0] * b[0] + a[1] * b[1] + a[2] * b[2]


# Handy grid functions


@default_jit
def mod(i, n):
    """Return grid modulo."""
    return (i + n) % n


@default_jit
def get_index(ix, iy, iz, dims):
    """Return index from grid point (`ix`, `iy`, `iz`)."""
    return dims[2] * (dims[1] * mod(ix, dims[0]) + mod(iy, dims[1])) + mod(iz, dims[2])


@default_jit
def get_index_nm(ix, iy, iz, dims):
    """Return index from grid point (`ix`, `iy`, `iz`), no modulo."""
    return dims[2] * (dims[1] * ix + iy) + iz


# this function does only get used in unittest test_shift()
@default_jit
def get_point(x, dims):
    """Return grid point from index `x`."""

    # for i in range(2, -1, -1):
    #     r[i] = x % dims[i]
    #     x -= r[i]
    #     x /= dims[i]

    # range loop with r as list performs 2x worse
    r2 = x % dims[2]
    x //= dims[2]

    r1 = x % dims[1]
    x //= dims[1]

    r0 = x % dims[0]

    return r0, r1, r2


# this function does only get used in unittest test_shift()
@default_jit
def shift(x, i, o, dims):
    """Inefficient index shifting."""
    # use numba typed lists to enable compilation
    pos = List()
    pos.extend(get_point(x, dims))
    pos[i] = (pos[i] + o + dims[i]) % dims[i]

    # manually unpacking the list is necessary due to numba restriction
    return get_index_nm(pos[0], pos[1], pos[2], dims)


@default_jit
def shift2(x, i, o, dims, acc):
    """Efficient index shifting.

    Args:
        x: starting point
        i: direction
        o: sign as +1 or -1
        dims: lattice dimensions
        acc: grid factors from `pyglasma3d_numba_source.core.Simulation`
    """
    res = x
    di = x // acc[i + 1]
    wdi = di % dims[i]

    if o > 0:
        if wdi == dims[i] - 1:
            res -= acc[i]
        res += acc[i + 1]
    else:
        if wdi == 0:
            res += acc[i]
        res -= acc[i + 1]

    return res


@default_jit
def get_field_index(i, d):
    """Return field index."""
    return 3 * (3 * i + d)


@default_jit
def get_group_index(i, d):
    """Return group index."""
    return 4 * (3 * i + d)


# Plaquette functions


@default_jit
def plaq_pos(u, x, i, j, dims, acc):
    r"""Compute 'positive' plaquette \(U_{x, i j}\)."""

    x1 = shift2(x, i, 1, dims, acc)
    x2 = shift2(x, j, 1, dims, acc)

    g0 = get_group_index(x, i)
    g1 = get_group_index(x1, j)
    g2 = get_group_index(x2, i)
    g3 = get_group_index(x, j)

    u1 = (u[g0], u[g0 + 1], u[g0 + 2], u[g0 + 3])
    u2 = (u[g1], u[g1 + 1], u[g1 + 2], u[g1 + 3])
    u3 = (u[g2], u[g2 + 1], u[g2 + 2], u[g2 + 3])
    u4 = (u[g3], u[g3 + 1], u[g3 + 2], u[g3 + 3])

    return mul4(u1, u2, u3, u4, 1, 1, -1, -1)


@default_jit
def plaq_neg(u, x, i, j, dims, acc):
    r"""Compute 'negative' plaquette \(U_{x, i -j}\)."""

    x0 = x
    x1 = shift2(shift2(x0, i, 1, dims, acc), j, -1, dims, acc)
    x2 = shift2(x1, i, -1, dims, acc)
    x3 = x2

    g0 = get_group_index(x0, i)
    g1 = get_group_index(x1, j)
    g2 = get_group_index(x2, i)
    g3 = get_group_index(x3, j)

    u1 = (u[g0], u[g0 + 1], u[g0 + 2], u[g0 + 3])
    u2 = (u[g1], u[g1 + 1], u[g1 + 2], u[g1 + 3])
    u3 = (u[g2], u[g2 + 1], u[g2 + 2], u[g2 + 3])
    u4 = (u[g3], u[g3 + 1], u[g3 + 2], u[g3 + 3])

    return mul4(u1, u2, u3, u4, 1, -1, -1, 1)


# parallel transport


@default_jit
def transport_neg(links, field, x, d, dims, acc):
    """Parallel transport."""

    xs = shift2(x, d, -1, dims, acc)
    field_index = get_field_index(xs, d)
    group_index = get_group_index(xs, d)

    link_selection = (
        links[group_index],
        links[group_index + 1],
        links[group_index + 2],
        links[group_index + 3],
    )

    field_selection = (
        field[field_index],
        field[field_index + 1],
        field[field_index + 2],
    )

    return act(link_selection, field_selection, 1)
