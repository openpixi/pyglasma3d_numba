"""McLerran-Venugopalan initial conditions."""

# TODO: this code is partially complete
#       it does contain everything necessary for the leapfrog solver
#       with mv initial conditions but lacks other code used for other
#       solvers and initial conditions


import math

import numpy as np

from . import lattice
from . import solver_leapfrog as leapfrog
from .numba_target import default_jit
from .parallel import make_parallel_loop

# from .parallel import set_field_to_field_kernel


def compute_v(phi2d, g, a_l, x0, sigma, orientation, dims, v):
    """Compute Wilson line for coherent MV model.

    .. note::This functions uses `force_dev='numba'` for parallel loops.
    """
    profile = np.zeros(dims[0])

    for ix in range(dims[0]):
        arg = orientation * (x0 - ix * a_l) / (math.sqrt(2) * sigma)
        profile[ix] = -0.5 * g * (1 + math.erf(arg))

    compute_v_kernel(0, dims[1], dims, phi2d, profile, v, force_dev="numba")


@make_parallel_loop
@default_jit
def compute_v_kernel(iy, dims, phi2d, profile, v):
    """`pyglasma3d_numba_source.parallel.make_parallel_loop`\
    CHANGES THE USAGE AND ARGUMENTS!.

    Signature:
    ```
        compute_v_kernel(
            iter_min, iter_max, dims, phi2d, profile, v, force_dev=None
        )
    ```
    """
    for iz in range(dims[2]):
        for ix in range(dims[0]):
            x = lattice.get_index(ix, iy, iz, dims)
            xt = dims[2] * iy + iz
            (v[4 * x], v[4 * x + 1], v[4 * x + 2], v[4 * x + 3]) = lattice.mexp(
                (phi2d[3 * xt], phi2d[3 * xt + 1], phi2d[3 * xt + 2]), profile[ix]
            )


# Set gauge links according to Wilson line


@make_parallel_loop
@default_jit
def set_links_kernel(ix, v, dims0, dims1, dims2, acc0, acc1, acc2, acc3, u):
    """`pyglasma3d_numba_source.parallel.make_parallel_loop`\
    CHANGES THE USAGE AND ARGUMENTS!.

    Signature:
    ```
        set_links_kernel(
            iter_min, iter_max, v, dims0, dims1, dims2, acc0, acc1,
            acc2, acc3, u, force_dev=None
        )
    ```
    """
    for iy in range(dims1):
        for iz in range(dims2):
            x = lattice.get_index(ix, iy, iz, (dims0, dims1, dims2))

            for d in range(1, 3):
                xs = lattice.shift2(
                    x, d, 1, (dims0, dims1, dims2), (acc0, acc1, acc2, acc3)
                )

                group_index = lattice.get_group_index(x, d)
                buffer1 = lattice.mul2(
                    (v[4 * x], v[4 * x + 1], v[4 * x + 2], v[4 * x + 3]),
                    (v[4 * xs], v[4 * xs + 1], v[4 * xs + 2], v[4 * xs + 3]),
                    1,
                    -1,
                )

                buffer2 = lattice.mul2_fast(
                    buffer1,
                    (
                        u[group_index],
                        u[group_index + 1],
                        u[group_index + 2],
                        u[group_index + 3],
                    ),
                )
                (
                    u[group_index],
                    u[group_index + 1],
                    u[group_index + 2],
                    u[group_index + 3],
                ) = lattice.load(buffer2)


@default_jit
def k2_latt(x, y, nt, a_t):
    """Effective transverse lattice momentum squared."""

    result = 4.0
    result -= 2.0 * math.cos((2.0 * np.pi * x) / nt)
    result -= 2.0 * math.cos((2.0 * np.pi * y) / nt)
    return result / (a_t * a_t)


# McLerran-Venugopalan initial conditions with
# coherent longitudinal structure and finite width


def initialize_mv(sim, x0, mu, sigma, mass, uvt, orientation):
    """Apply McLerran-Venugopalan initial conditions.

    .. note::This functions uses `force_dev='numba'` for parallel loops.
    """
    # a_l = sim.a[0]
    # a_t = sim.a[1]
    # NL = sim.dims[0]
    # NT = sim.dims[1]

    # random 2D charge density
    phi_n = 3 * sim.dims[1] * sim.dims[1]
    phi2d = np.zeros(phi_n, dtype=np.float64)

    charge_factor = sim.g * mu / sim.a[1]

    for ic in range(3):
        field = np.random.normal(0, charge_factor, (sim.dims[1], sim.dims[1]))
        field = field.astype(np.complex128, casting="safe", copy=False)

        # solve poisson equation in momentum space
        field = np.fft.fft2(field)

        for x in range(sim.dims[1]):
            for y in range(sim.dims[1]):

                k2 = k2_latt(x, y, sim.dims[1], sim.a[1])
                if (x > 0 or y > 0) and k2 <= uvt * uvt:
                    field[x, y] *= 1.0 / (k2 + mass * mass)
                else:
                    field[x, y] = 0.0

        field = np.fft.ifft2(field)

        # np.real() returns float for complex arguments
        flat_field = np.real(field.flatten())
        # put results in phi2d array
        # phi2d[ic: phi_n: 3] = \
        # flat_field[0: sim.dims[1] * sim.dims[1]: 1]
        phi2d[ic:phi_n:3] = flat_field
        # the kernel shows no visible performance gains
        # set_field_to_field_kernel(0, flat_field.size,
        #   phi2d, flat_field, 3, ic, force_dev='numba')

    # compute Wilson line
    # at t = 0
    v = np.zeros(4 * sim.dims[0] * sim.dims[1] * sim.dims[2], dtype=np.float64)
    compute_v(phi2d, sim.g, sim.a[0], x0, sigma, orientation, sim.dims, v)

    set_links_kernel(0, sim.dims[0], v, *sim.dims, *sim.acc, sim.u0, force_dev="numba")

    # at t = dt
    compute_v(
        phi2d,
        sim.g,
        sim.a[0],
        x0 + orientation * sim.dt,
        sigma,
        orientation,
        sim.dims,
        v,
    )

    set_links_kernel(0, sim.dims[0], v, *sim.dims, *sim.acc, sim.u1, force_dev="numba")

    # compute electric field from gauge links
    # (inverse gauge link update)
    # make sure code works with old leapfrog and new implicit schemes
    # curently not necessary:
    # solver_leapfrog with mv initial condition is not implicit
    # if s.mode is None:
    #     leapfrog.evolve_u_inv(s)
    # else:
    #     common.evolve_u_inv2(s)

    leapfrog.evolve_u_inv(sim)

    return v
