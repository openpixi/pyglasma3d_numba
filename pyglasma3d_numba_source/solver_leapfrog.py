"""A module related to the leapfrog YM solver."""


from . import lattice
from .numba_target import default_jit
from .parallel import make_parallel_loop


@default_jit
def compute_staple_sum2(x, d, links, neighbours, plaq_factor):
    """Compute staple sum, second variant."""

    ni = 7 * (3 * x + d)
    ci1 = neighbours[ni]
    buffer_s = lattice.zero()
    index_c = 0

    for i in range(3):
        if i != d:
            ci2 = neighbours[ni + 1 + index_c * 3]
            ci3 = neighbours[ni + 2 + index_c * 3]
            ci4 = neighbours[ni + 3 + index_c * 3]

            ui1 = 4 * (3 * ci1 + i)
            u1 = (links[ui1], links[ui1 + 1], links[ui1 + 2], links[ui1 + 3])

            ui2 = 4 * (3 * ci2 + d)
            u2 = (links[ui2], links[ui2 + 1], links[ui2 + 2], links[ui2 + 3])

            buffer1 = lattice.mul2(u1, u2, 1, -1)

            ui3 = 4 * (3 * x + i)
            u3 = (links[ui3], links[ui3 + 1], links[ui3 + 2], links[ui3 + 3])

            buffer2 = lattice.mul2(buffer1, u3, 1, -1)

            buffer_s = lattice.add(buffer_s, buffer2, plaq_factor[i])

            ui4 = 4 * (3 * ci3 + i)
            u4 = (links[ui4], links[ui4 + 1], links[ui4 + 2], links[ui4 + 3])

            ui5 = 4 * (3 * ci4 + d)
            u5 = (links[ui5], links[ui5 + 1], links[ui5 + 2], links[ui5 + 3])

            buffer1 = lattice.mul2(u4, u5, -1, -1)

            ui6 = 4 * (3 * ci4 + i)
            u6 = (links[ui6], links[ui6 + 1], links[ui6 + 2], links[ui6 + 3])

            buffer2 = lattice.mul2_fast(buffer1, u6)

            buffer_s = lattice.add(buffer_s, buffer2, plaq_factor[i])

            index_c += 1

    ui = 4 * (3 * x + d)
    u = (links[ui], links[ui + 1], links[ui + 2], links[ui + 3])

    buffer = lattice.mul2_fast(u, buffer_s)

    return lattice.proj(buffer)


def evolve_u(sim):
    """Gauge link update."""

    nx0, nx1, ny0, ny1, nz0, nz1 = sim.iter_dims

    # parallelize along x direction
    evolve_u_kernel(
        nx0, nx1, ny0, ny1, nz0, nz1, sim.dims, sim.dt, sim.u0, sim.u1, sim.e
    )


@make_parallel_loop
@default_jit
def evolve_u_kernel(ix, ny0, ny1, nz0, nz1, dims, dt, links0, links1, electric):
    """`pyglasma3d_numba_source.parallel.make_parallel_loop`\
    CHANGES THE USAGE AND ARGUMENTS!.

    Signature:
    ```
        evolve_u_kernel(
            iter_min, iter_max, ny0, ny1, nz0, nz1, dims, dt, links0,
            links1, electric, force_dev=None
        )
    ```
    """
    for iy in range(ny0, ny1):
        for iz in range(nz0, nz1):
            cell_index = lattice.get_index_nm(ix, iy, iz, dims)

            for d in range(3):
                field_index = lattice.get_field_index(cell_index, d)
                group_index = lattice.get_group_index(cell_index, d)

                # exponentiate electric field "exp(-i dt E)"
                # and write to buffer
                buffer = lattice.mexp(
                    (
                        electric[field_index],
                        electric[field_index + 1],
                        electric[field_index + 2],
                    ),
                    -dt,
                )

                # multiply "exp(-i dt E)" with old gauge link
                # and write to new gauge link
                (
                    links1[group_index],
                    links1[group_index + 1],
                    links1[group_index + 2],
                    links1[group_index + 3],
                ) = lattice.mul2(
                    buffer,
                    (
                        links0[group_index],
                        links0[group_index + 1],
                        links0[group_index + 2],
                        links0[group_index + 3],
                    ),
                    1,
                    1,
                )


def evolve(sim):
    """Perform combined field and link update."""

    plaq_factor = tuple(sim.dt / (sim.a[dj] * sim.a[dj]) for dj in range(3))

    current_factor = -sim.g * sim.a[0] * sim.dt
    num_transverse = sim.dims[1] * sim.dims[2]

    nx0, nx1, *_ = sim.iter_dims

    # passing tuples to numba.jit(parallel=True) compiled functions
    # does not work. see:
    # https://github.com/numba/numba/issues/2625
    # https://github.com/numba/numba/issues/2651
    # w/a: the tuple 'plaq_factor' gets unpacked before passing it to
    # the function. later the function accepts as many scalar arguments
    # as the tuple had elements.

    # parallelize along x direction
    evolve_kernel(
        nx0 * num_transverse,
        nx1 * num_transverse,
        sim.u0,
        sim.staple_neighbours,
        *plaq_factor,
        sim.e,
        sim.j,
        current_factor,
        sim.dt,
        sim.u1
    )


@make_parallel_loop
@default_jit
def evolve_kernel(
    cell_index,
    links0,
    neighbours,
    plaq_factor1,
    plaq_factor2,
    plaq_factor3,
    electric,
    current,
    current_factor,
    dt,
    links1,
):
    """`pyglasma3d_numba_source.parallel.make_parallel_loop`\
    CHANGES THE USAGE AND ARGUMENTS!.

    Signature:
    ```
        evolve_kernel(
            iter_min, iter_max, links0, neighbours, plaq_factor1,
            plaq_factor2, plaq_factor3, electric, current,
            current_factor, dt, links1, force_dev=None
        )
    ```
    """
    for di in range(3):
        # sum over plaquettes (positive and negative)
        algebra_buffer = compute_staple_sum2(
            cell_index,
            di,
            links0,
            neighbours,
            (plaq_factor1, plaq_factor2, plaq_factor3),
        )

        # field index for electric field (and current)
        field_index = lattice.get_field_index(cell_index, di)

        # add sum over plaquettes to electric field
        (
            electric[field_index],
            electric[field_index + 1],
            electric[field_index + 2],
        ) = lattice.algebra_add(
            (
                electric[field_index],
                electric[field_index + 1],
                electric[field_index + 2],
            ),
            algebra_buffer,
            1,
        )

        if di == 0:
            # add current to electric field
            # current j consists only of jx
            # 3*c_i gives the right index in this case
            (
                electric[field_index],
                electric[field_index + 1],
                electric[field_index + 2],
            ) = lattice.algebra_add(
                (
                    electric[field_index],
                    electric[field_index + 1],
                    electric[field_index + 2],
                ),
                (
                    current[3 * cell_index],
                    current[3 * cell_index + 1],
                    current[3 * cell_index + 2],
                ),
                current_factor,
            )

        # update link
        group_index = lattice.get_group_index(cell_index, di)

        # exponentiate electric field "exp(-i dt E)" and write to buffer
        group_buffer = lattice.mexp(
            (
                electric[field_index],
                electric[field_index + 1],
                electric[field_index + 2],
            ),
            -dt,
        )

        # multiply "exp(-i dt E)" with old gauge link
        # and write to new gauge link
        (
            links1[group_index],
            links1[group_index + 1],
            links1[group_index + 2],
            links1[group_index + 3],
        ) = lattice.mul2_fast(
            group_buffer,
            (
                links0[group_index],
                links0[group_index + 1],
                links0[group_index + 2],
                links0[group_index + 3],
            ),
        )


def evolve_u_inv(sim):
    """Compute electric field from gauge links.

    .. note::This functions uses `force_dev='numba'` for parallel loops.
    """
    inv_dt = -1.0 / sim.dt
    nx0, nx1, ny0, ny1, nz0, nz1 = sim.iter_dims

    # parallelize along x direction
    evolve_u_inv_kernel(
        nx0,
        nx1,
        ny0,
        ny1,
        nz0,
        nz1,
        sim.dims,
        sim.u0,
        sim.u1,
        sim.e,
        inv_dt,
        force_dev="numba",
    )


@make_parallel_loop
@default_jit
def evolve_u_inv_kernel(ix, ny0, ny1, nz0, nz1, dims, links0, links1, electric, inv_dt):
    """`pyglasma3d_numba_source.parallel.make_parallel_loop`\
    CHANGES THE USAGE AND ARGUMENTS!.

    Signature:
    ```
        evolve_u_inv_kernel(
            iter_min, iter_max, ny0, ny1, nz0, nz1, dims, links0,
            links1, electric, inv_dt, force_dev=None
        )
    ```
    """
    for iy in range(ny0, ny1):
        for iz in range(nz0, nz1):
            cell_index = lattice.get_index_nm(ix, iy, iz, dims)

            for d in range(3):
                field_index = lattice.get_field_index(cell_index, d)
                group_index = lattice.get_group_index(cell_index, d)

                buffer = lattice.mul2(
                    (
                        links1[group_index],
                        links1[group_index + 1],
                        links1[group_index + 2],
                        links1[group_index + 3],
                    ),
                    (
                        links0[group_index],
                        links0[group_index + 1],
                        links0[group_index + 2],
                        links0[group_index + 3],
                    ),
                    1,
                    -1,
                )
                (
                    electric[field_index],
                    electric[field_index + 1],
                    electric[field_index + 2],
                ) = lattice.mlog(buffer)

                (
                    electric[field_index],
                    electric[field_index + 1],
                    electric[field_index + 2],
                ) = lattice.algebra_mul(
                    (
                        electric[field_index],
                        electric[field_index + 1],
                        electric[field_index + 2],
                    ),
                    inv_dt,
                )


def compute_staple_neighbours(sim):
    """Compute staple neighbours."""

    n = sim.dims[0] * sim.dims[1] * sim.dims[2]
    compute_staple_neighbours_kernel(0, n, sim.dims, sim.acc, sim.staple_neighbours)


@make_parallel_loop
@default_jit
def compute_staple_neighbours_kernel(x, dims, acc, neighbours):
    """`pyglasma3d_numba_source.parallel.make_parallel_loop`\
    CHANGES THE USAGE AND ARGUMENTS!.

    Signature:
    ```
        compute_staple_neighbours_kernel(
            iter_min, iter_max, dims, acc, neighbours, force_dev=None
        )
    ```
    """
    for d in range(3):
        index_c = 0
        ci1 = lattice.shift2(x, d, 1, dims, acc)
        neighbours[7 * (3 * x + d) + 0] = ci1

        for i in range(3):
            if i != d:
                ci2 = lattice.shift2(x, i, 1, dims, acc)
                ci3 = lattice.shift2(ci1, i, -1, dims, acc)
                ci4 = lattice.shift2(x, i, -1, dims, acc)

                neighbours[7 * (3 * x + d) + 1 + index_c * 3] = ci2
                neighbours[7 * (3 * x + d) + 2 + index_c * 3] = ci3
                neighbours[7 * (3 * x + d) + 3 + index_c * 3] = ci4

                index_c += 1
