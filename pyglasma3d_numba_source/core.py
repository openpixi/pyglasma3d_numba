"""The `Simulation` class used to control simulation flow and data.

Use as part of a setup script that explicitly calculates initial
conditions and populates existing `Simulation` instances.
"""

import os
import numpy as np

from . import energy, gauss, interpolate
from . import solver_leapfrog as leapfrog
from .numba_target import use_device
from .parallel import set_field_to_constant_kernel, set_field_zero_kernel

if use_device == "cuda":
    from .numba_target import cuda


class Simulation:
    """Simulation class used for the leapfrog solver.

    First create the class instance and then explicitly generate the
    initial conditions using the `pyglasma3d_numba_source.mv` and
    `pyglasma3d_numba_source.interpolate` modules. Then call
    `Simulation.init` to sync all data arrays.
    Use `Simulation.evolve` to advance the simulation time by one time
    step `dt`. Use `Simulation.write_energy_momentum` to save
    the EM tensor.

    Attributes:
        data (live): `u0`, `u1`, `e`, `j`, `r`, `staple_neighbours`
          always point to the 'working set', which can be in host or
          device memory (live data).
        data (host): `u0_h`, `u1_h`, `e_h`, `j_h`, `r_h`,
          `staple_neighbours_h` always point to host memory and are out
          of date for CUDA runs, unless `Simulation.copy_to_host` is
          used immediately before access (host data).
    """

    def __init__(self, dims, iter_dims, a, dt, g, debug=False):
        """Constructor sets all attributes and allocates memory on host.

        Args:
            dims: 3-Tuple (nl, nt, nt) for the lattice size.
            iter_dims: 6-Tuple (nl_0, nl_1, nt_0, nt_1, nt_0, nt_1) that
              sets the start (_0) and stop (_1) indices for iterations
              over the lattice. Usually nl_0 = 1, nl_1 = nl-1 which
              excludes the fixed boundary conditions.
            a: 3-Tuple (aL, aT, aT) for the lattice spacings.
            dt: Value of the time step.
            g: YM coupling constant.
            debug: Optional; Enable logging to stdout with
              `Simulation.log`.
        """

        self.dims = np.array(dims, dtype=np.int64)
        self.iter_dims = np.array(iter_dims, dtype=np.int64)
        self.a = np.array(a, dtype=np.float64)
        self.dt = np.float64(dt)
        self.g = np.float64(g)
        self.acc = np.array(
            [dims[0] * dims[1] * dims[2], dims[1] * dims[2], dims[2], 1], dtype=np.int64
        )

        self.N = dims[0] * dims[1] * dims[2]
        self.NL = dims[0]
        self.NT = dims[1]

        # create arrays on host
        self.u0_h = np.zeros(self.N * 3 * 4, dtype=np.float64)
        self.u1_h = np.zeros(self.N * 3 * 4, dtype=np.float64)
        self.e_h = np.zeros(self.N * 3 * 3, dtype=np.float64)
        self.j_h = np.zeros(self.N * 3, dtype=np.float64)
        self.r_h = np.zeros(self.N * 3, dtype=np.float64)
        self.staple_neighbours_h = np.zeros(self.N * 3 * 7, dtype=np.int64)

        # set zero element for links
        self.u0_h[::4] = 1.0
        self.u1_h[::4] = 1.0
        # set_field_to_constant_kernel(
        #     0, int(np.ceil(self.u0.size / 4)), self.u0, 1.0, 4, 0,
        #     force_dev="numba"
        # )
        # set_field_to_constant_kernel(
        #     0, int(np.ceil(self.u1.size / 4)), self.u1, 1.0, 4, 0,
        #     force_dev="numba"
        # )

        self.charges = []
        self.t = 0.0

        self.write_counter = 0
        self.debug = debug

        # this is used for checks in mv, interpolate to decide if we are
        # running the old leapfrog code or a new implicit scheme
        # the current implementation does not work with implicit scheme:
        # this property is useless
        self.mode = None

        # execution device: numba -> cpu, cuda -> gpu
        # do not edit this property, use READONLY
        self.use_device = use_device

        # memory on device (gpu)
        # for target=numba: pointer to host arrays
        # for target=cuda: pointer to device (gpu) memory
        self.u0 = self.u0_h
        self.u1 = self.u1_h
        self.e = self.e_h
        self.j = self.j_h
        self.r = self.r_h
        self.staple_neighbours = self.staple_neighbours_h

        # for energy-momentum tensor
        nx = self.dims[0] - 2
        self.EL_h = np.zeros(nx, dtype=np.float64)
        self.BL_h = np.zeros(nx, dtype=np.float64)
        self.ET_h = np.zeros(nx, dtype=np.float64)
        self.BT_h = np.zeros(nx, dtype=np.float64)
        self.SL_h = np.zeros(nx, dtype=np.float64)

        self.EL = self.EL_h
        self.BL = self.BL_h
        self.ET = self.ET_h
        self.BT = self.BT_h
        self.SL = self.SL_h

        # initialize staple neighbours
        leapfrog.compute_staple_neighbours(self)

    def copy_to_device(self):
        """Move live data from host memory to compute device."""

        self.u0 = cuda.to_device(self.u0_h)
        self.u1 = cuda.to_device(self.u1_h)
        self.e = cuda.to_device(self.e_h)
        self.j = cuda.to_device(self.j_h)
        self.r = cuda.to_device(self.r_h)
        self.staple_neighbours = cuda.to_device(self.staple_neighbours_h)

        # the charges
        for pl in self.charges:
            pl.q = cuda.to_device(pl.q_h)

        # energy_momentum_tensor
        self.EL = cuda.to_device(self.EL_h)
        self.BL = cuda.to_device(self.BL_h)
        self.ET = cuda.to_device(self.ET_h)
        self.BT = cuda.to_device(self.BT_h)
        self.SL = cuda.to_device(self.SL_h)

    def copy_to_host(self):
        """Copy live data from compute device to host memory."""

        self.u0.copy_to_host(self.u0_h)
        self.u1.copy_to_host(self.u1_h)
        self.e.copy_to_host(self.e_h)
        self.j.copy_to_host(self.j_h)
        self.r.copy_to_host(self.r_h)
        self.staple_neighbours.copy_to_host(self.staple_neighbours_h)

        # the charges
        for pl in self.charges:
            pl.q.copy_to_host(pl.q_h)

        # energy_momentum_tensor
        self.EL.copy_to_host(self.EL_h)
        self.BL.copy_to_host(self.BL_h)
        self.ET.copy_to_host(self.ET_h)
        self.BT.copy_to_host(self.BT_h)
        self.SL.copy_to_host(self.SL_h)

    def init(self):
        """Interpolate to charge density and update gauge links.

        For runs using CUDA, also move live data from host to compute
        device memory.
        """
        # Copy arrays to device after __init__() but before compute
        if self.use_device == "cuda":
            self.copy_to_device()

        interpolate.interpolate_to_charge_density(self)
        leapfrog.evolve_u(self)

    def reset(self):
        """Reset all live data arrays to initial values."""

        set_field_zero_kernel(0, self.u0.size, self.u0)
        set_field_zero_kernel(0, self.u1.size, self.u1)
        set_field_zero_kernel(0, self.e.size, self.e)
        set_field_zero_kernel(0, self.j.size, self.j)
        set_field_zero_kernel(0, self.r.size, self.r)

        # for cuda mode, this will lead to long memory copy times
        # executing the kernel on the device will prevent memory copy
        # self.u0[::4] = 1.0
        # self.u1[::4] = 1.0
        set_field_to_constant_kernel(
            0, int(np.ceil(self.u0.size / 4)), self.u0, 1.0, 4, 0
        )
        set_field_to_constant_kernel(
            0, int(np.ceil(self.u1.size / 4)), self.u1, 1.0, 4, 0
        )

    def reset_charges_and_currents(self):
        """Reset only charges and currents to initial values."""

        set_field_zero_kernel(0, self.j.size, self.j)
        set_field_zero_kernel(0, self.r.size, self.r)

    def swap(self):
        """Swap gauge link pointers."""

        self.u1, self.u0 = self.u0, self.u1
        # swap host pointers
        self.u1_h, self.u0_h = self.u0_h, self.u1_h

    def evolve(self):
        """Evolve one time step `dt`."""

        self.reset_charges_and_currents()
        interpolate.advance_and_interpolate_currents(self)
        interpolate.interpolate_to_charge_density(self)
        self.swap()
        leapfrog.evolve(self)
        self.t += self.dt

    def get_nbytes(self):
        """Return estimate of memory used by all live data."""

        grid_nbytes = (
            self.u0.nbytes
            + self.u1.nbytes
            + self.e.nbytes
            + self.j.nbytes
            + self.r.nbytes
        )
        grid_nbytes += self.staple_neighbours.nbytes
        charges_nbytes = sum(c.get_nbytes() for c in self.charges)
        observables_nbytes = (
            self.ET.nbytes
            + self.EL.nbytes
            + self.BT.nbytes
            + self.BL.nbytes
            + self.SL.nbytes
        )
        return grid_nbytes + charges_nbytes + observables_nbytes

    def mem_info(self):
        """Return memory usage based on `Simulation.get_nbytes`."""

        info = ""
        if self.use_device == "cuda":
            meminfo = cuda.current_context().get_memory_info()
            info += (
                f"CUDA free memory: {meminfo[0] / 1024 ** 3:.2f} GB of "
                f"{meminfo[1] / 1024 ** 3:.2f} GB.\n"
            )
        info += f"Current memory usage: {self.get_nbytes() / 1024 ** 3:.2f} GB."
        return info

    def log(self, *msg):
        """Log for `debug=True` enabled runs."""

        # only prints if debug property is set to True
        if self.debug:
            print(*msg)

    def write_energy_momentum(self, max_writes, file_path):
        """Write energy-momentum tensor to binary encoded file.

        Args:
            max_writes: Maximum number of times the EM tensor is
              calculated during one simulation. This value will be
              recorded in the header of the output file.
            file_path: Valid absolute or relative (to the CWD) file path
              for the output file.
        """

        EL, BL, ET, BT, SL = energy.energy_momentum_tensor(self)
        nx = self.NL - 2

        # data format:
        # header (2 lines): NL-2 <br> max_t
        # body: EL, BL, ET, BT, SL
        def toint(i):
            return np.array([i], dtype=np.int64).tobytes()

        # TODO: rewrite with context py3 style
        # check if all `if` are necessary
        # check performance after changes!
        # open file
        if not os.path.isfile(file_path):
            dirname = os.path.dirname(file_path)
            if not os.path.exists(dirname):
                os.makedirs(os.path.dirname(file_path))
        # write header
        if self.write_counter == 0:
            f = open(file_path, mode="w+b")
            f.write(toint(nx))
            f.write(toint(max_writes))
        elif self.write_counter < max_writes:
            f = open(file_path, mode="a+b")
        else:
            f = None

        if f is not None:
            # write body
            f.write(EL.tobytes())
            f.write(BL.tobytes())
            f.write(ET.tobytes())
            f.write(BT.tobytes())
            f.write(SL.tobytes())
            f.close()
            self.write_counter += 1

    # methods returning observables
    # Simulation.gauss_constraint_squared()
    #       <==> gauss.gauss_sq_total(Simulation)
    # Simulation.energy_momentum_tensor()
    #       <==> energy.energy_momentum_tensor(Simulation)
    gauss_constraint_squared = gauss.gauss_sq_total
    energy_momentum_tensor = energy.energy_momentum_tensor
