"""Colored Particle-in-Cell (CPIC) interpolation routines.

- `pyglasma3d_numba_source.interpolate.ChargePlane` class
- initialization of charge planes
    - charge refinement
- charge movement and current interpolation
- charge density interpolation
"""


import math

import numpy as np

from . import gauss, lattice
from .numba_target import default_jit
from .parallel import make_parallel_loop


class ChargePlane:
    """Charge planes carry all charges for a transverse plane.

    Attributes:
        orientation: +1 (-1) for charges moving in +x (-x) direction.
        q: Live charges array. `q_h` is the corresponding host data.
        N: Transverse plane dimensions (NT x NT).
        index_offset: Position of plane in (NL x NT x NT) sized array
          representing the entire lattice.
        subcell_shift: Position of plane inside grid cell.
        active: `True` if plane is inside sim boundaries, else `False`.
    """

    def __init__(self, dims, index_offset, subcell_shift, orientation):
        """Constructor sets all attributes and allocates memory."""

        self.N = dims[1] * dims[2]
        self.index_offset = index_offset
        self.subcell_shift = subcell_shift
        self.orientation = orientation
        self.q_h = np.zeros(self.N * 3, dtype=np.float64)
        # memory on gpu
        # for target=numba: pointer to host arrays
        # for target=cuda: pointer to device (gpu) memory
        self.q = self.q_h
        # charge planes become inactive once they leave the simulation
        # box. they aren't removed automatically.
        self.active = True

    def get_nbytes(self):
        """Return memory usage estimate for charges array."""
        return self.q.nbytes


def initialize_charge_planes(sim, orientation, nx0, nx1):
    """Sample gauss density and create charge planes.

    .. note::This functions uses `force_dev='numba'` for parallel loops.
    """
    # restrict region
    iter_dims = sim.iter_dims.copy()
    iter_dims[0] = nx0
    iter_dims[1] = nx1

    gauss_d = np.zeros(3 * sim.dims[0] * sim.dims[1] * sim.dims[2])

    # compute gauss density, put it into gauss_d
    # if s.mode is defined then we are dealing with an implicit scheme
    # if s.mode is None:
    #     gauss_density(&links[0], &electric[0], &rho[0],
    #                   &spacings[0], &dims[0], &acc[0], iter_dims, s.g,
    #                   &gauss_d[0])
    # else:
    #     gauss_d = s.gauss_density()
    #
    # for solver_leapfrog and mv initial conditions there is no
    # implicit scheme

    gauss.gauss_density(
        sim.u0, sim.e, sim.r, sim.a, sim.dims, sim.acc, iter_dims, sim.g, gauss_d
    )

    # average gauss density across transverse plane
    # and determine cutoff charge
    gauss_d_avg = np.zeros(3 * sim.dims[0])

    gauss_avg_kernel(
        nx0, nx1, iter_dims, sim.dims, gauss_d_avg, gauss_d, force_dev="numba"
    )

    gauss_d_avg /= sim.dims[1] * sim.dims[2]

    gauss_avg_max = np.max(gauss_d_avg)
    gauss_cutoff = gauss_avg_max * 1.0e-10

    # restrict region according to gauss_cutoff
    # TODO: rewrite with np fancy indexing:
    # gauss_d_avg[gauss_d_avg >= gauss_cutoff]
    for ix in range(nx0, nx1):
        if gauss_d_avg[ix] >= gauss_cutoff:
            nx0 = ix
            break

    for ix in range(nx1, nx0, -1):
        if gauss_d_avg[ix] >= gauss_cutoff:
            nx1 = ix
            break

    # perform charge refinement
    # particles_per_cell (ppc) typecast to int
    # to be able to use it as array index
    ppc = int(np.round(sim.a[0] / sim.dt))
    particles_per_plane = sim.dims[1] * sim.dims[2]

    # reorder gauss density array for easier refinement
    gauss_d = np.swapaxes(
        gauss_d.reshape((sim.dims[0], sim.dims[1], sim.dims[2], 3)), 0, -1
    ).flatten()

    gauss_d = np.repeat(gauss_d, ppc) / ppc

    # order is now (3, dims[1], dims[2], dims[0] * ppc)
    # this means that the charge lines of length 'dims[0] * ppc'
    # are laid out correctly in memory

    # apply refinement
    n_line = sim.dims[0] * ppc
    _, ny, nz = sim.dims

    gauss_refinement_kernel(
        0, ny, nx0, nx1, nz, n_line, sim.dims, gauss_d, ppc, force_dev="numba"
    )

    # put array back into order
    gauss_d = np.swapaxes(
        gauss_d.reshape((3, sim.dims[1], sim.dims[2], sim.dims[0], ppc)), 3, 0
    ).flatten()

    gauss_d = np.moveaxis(
        gauss_d.reshape((sim.dims[0], sim.dims[1], sim.dims[2], 3, ppc)), -1, 0
    ).flatten()

    # place charge planes
    n = 3 * sim.dims[0] * sim.dims[1] * sim.dims[2]
    for i in range(ppc):
        for ix in range(nx0, nx1):
            pl = ChargePlane(sim.dims, ix * particles_per_plane, i, orientation)
            plane_charges = pl.q
            for iy in range(0, sim.dims[1]):
                for iz in range(0, sim.dims[2]):
                    x = lattice.get_index(ix, iy, iz, sim.dims)
                    xt = iy * sim.dims[2] + iz
                    plane_charges[3 * xt + 0] = gauss_d[i * n + 3 * x + 0]
                    plane_charges[3 * xt + 1] = gauss_d[i * n + 3 * x + 1]
                    plane_charges[3 * xt + 2] = gauss_d[i * n + 3 * x + 2]

            sim.charges.append(pl)


@make_parallel_loop
@default_jit
def gauss_avg_kernel(ix, iter_dims, dims, gauss_d_avg, gauss_d):
    """`pyglasma3d_numba_source.parallel.make_parallel_loop`\
    CHANGES THE USAGE AND ARGUMENTS!.

    Signature:
    ```
        gauss_avg_kernel(
            iter_min, iter_max, iter_dims, dims, gauss_d_avg, gauss_d,
            force_dev=None
        )
    ```
    """
    for iy in range(iter_dims[2], iter_dims[3]):
        for iz in range(iter_dims[4], iter_dims[5]):
            x = lattice.get_index(ix, iy, iz, dims)
            gauss_d_avg[ix] += math.sqrt(
                lattice.algebra_sq(
                    (gauss_d[3 * x], gauss_d[3 * x + 1], gauss_d[3 * x + 2])
                )
            )


@make_parallel_loop
@default_jit
def gauss_refinement_kernel(iy, nx0, nx1, nz, n_line, dims, gauss_d, ppc):
    """`pyglasma3d_numba_source.parallel.make_parallel_loop`\
    CHANGES THE USAGE AND ARGUMENTS!.

    Signature:
    ```
        gauss_refinement_kernel(
            iter_min, iter_max, nx0, nx1, nz, n_line, dims, gauss_d,
            ppc, force_dev=None
        )
    ```
    """
    for iz in range(nz):
        xt = iy * dims[2] + iz
        for j in range(3):
            i = j * dims[1] * dims[2] * n_line + xt * n_line
            refine2(gauss_d, i, ppc * nx0, ppc * nx1, 100, ppc)
            refine4(gauss_d, i, ppc * nx0, ppc * nx1, 100, ppc)


@default_jit
def refine2(gl, index, n0, n1, n_iter, ppc):
    """Second order charge refinement."""

    for _ in range(n_iter):
        for i in range(n0 + 1, n1 - 2):
            if i % ppc < ppc - 1:
                dq = 0.25 * (
                    -gl[index + i - 1]
                    + 3 * gl[index + i]
                    - 3 * gl[index + i + 1]
                    + gl[index + i + 2]
                )
                gl[index + i] -= dq
                gl[index + i + 1] += dq


@default_jit
def refine4(gl, index, n0, n1, n_iter, ppc):
    """Fourth order charge refinement."""

    for _ in range(n_iter):
        for i in range(n0 + 2, n1 - 3):
            if i % ppc < ppc - 1:
                dq = (
                    gl[index + i - 2]
                    - 5 * gl[index + i - 1]
                    + 10 * gl[index + i]
                    - 10 * gl[index + i + 1]
                    + 5 * gl[index + i + 2]
                    - gl[index + i + 3]
                ) / 12.0
                gl[index + i] -= dq
                gl[index + i + 1] += dq


def advance_and_interpolate_currents(sim):
    """Move charge planes and interpolate currents on the lattice."""

    f = sim.a[0] / sim.dt
    # particles_per_cell (ppc): typecast to int
    # to be able to use it for array indices
    particles_per_cell = int(np.round(f))

    # not very elegant, but this makes sure that the interpolation works
    # for both the old leapfrog code and the implicit schemes
    # the currently implemented scheme is not implicit
    # if s.mode is not None:
    #     links0, links1 = links1, links0

    # check if charge plane has moved and interpolate if needed
    ppcmo = particles_per_cell - 1
    pln = sim.dims[1] * sim.dims[2]
    nmax = (sim.dims[0] - 1) * pln
    nmin = pln

    for pl in sim.charges:
        if pl.active:
            charges = pl.q
            pl.subcell_shift = lattice.mod(
                pl.subcell_shift + pl.orientation, particles_per_cell
            )

            # positive orientation
            if pl.orientation > 0:
                if pl.subcell_shift == 0:

                    # interpolate to grid and do parallel transport
                    interpolate_currents_pos_kernel(
                        0, pl.N, sim.u1, charges, pl.index_offset, f, sim.j
                    )

                    pl.index_offset += pln
                    if pl.index_offset >= nmax:
                        pl.active = False
                        continue

            # negative orientation
            else:
                if pl.subcell_shift == ppcmo:

                    # interpolate to grid and do parallel transport
                    pl.index_offset -= pln
                    interpolate_currents_neg_kernel(
                        0, pl.N, sim.u1, charges, pl.index_offset, -f, sim.j
                    )

                    if pl.index_offset <= nmin:
                        pl.active = False
                        continue


@make_parallel_loop
@default_jit
def interpolate_currents_pos_kernel(i, links, charges, x, f, current):
    """`pyglasma3d_numba_source.parallel.make_parallel_loop`\
    CHANGES THE USAGE AND ARGUMENTS!.

    Signature:
    ```
        interpolate_currents_pos_kernel(
            iter_min, iter_max, links, charges, x, f, current,
            force_dev=None
        )
    ```
    """
    (
        current[3 * (x + i)],
        current[3 * (x + i) + 1],
        current[3 * (x + i) + 2],
    ) = lattice.algebra_add(
        (current[3 * (x + i)], current[3 * (x + i) + 1], current[3 * (x + i) + 2]),
        (charges[3 * i], charges[3 * i + 1], charges[3 * i + 2]),
        f,
    )
    (charges[3 * i], charges[3 * i + 1], charges[3 * i + 2]) = lattice.act(
        (
            links[4 * 3 * (x + i)],
            links[4 * 3 * (x + i) + 1],
            links[4 * 3 * (x + i) + 2],
            links[4 * 3 * (x + i) + 3],
        ),
        (charges[3 * i], charges[3 * i + 1], charges[3 * i + 2]),
        1,
    )


@make_parallel_loop
@default_jit
def interpolate_currents_neg_kernel(i, links, charges, x, f, current):
    """`pyglasma3d_numba_source.parallel.make_parallel_loop`\
    CHANGES THE USAGE AND ARGUMENTS!.

    Signature:
    ```
        interpolate_currents_neg_kernel(
            iter_min, iter_max, links, charges, x, f, current,
            force_dev=None
        )
    ```
    """
    (charges[3 * i], charges[3 * i + 1], charges[3 * i + 2]) = lattice.act(
        (
            links[4 * 3 * (x + i)],
            links[4 * 3 * (x + i) + 1],
            links[4 * 3 * (x + i) + 2],
            links[4 * 3 * (x + i) + 3],
        ),
        (charges[3 * i], charges[3 * i + 1], charges[3 * i + 2]),
        -1,
    )
    (
        current[3 * (x + i)],
        current[3 * (x + i) + 1],
        current[3 * (x + i) + 2],
    ) = lattice.algebra_add(
        (current[3 * (x + i)], current[3 * (x + i) + 1], current[3 * (x + i) + 2]),
        (charges[3 * i], charges[3 * i + 1], charges[3 * i + 2]),
        f,
    )


def interpolate_to_charge_density(sim):
    """Interpolate charge planes to lattice charge density."""

    # original version
    # n = sim.dims[0] * sim.dims[1] * sim.dims[2]
    # for x in range(0, 3 * n):
    #     rho[x] = 0.0

    # interpolate all charge planes to rho array
    for pl in sim.charges:
        charges = pl.q
        index_offset = pl.index_offset
        num = pl.N
        # sim.r[3 * index_offset: 3 * index_offset + 3 * num] += charges
        interpolate_charges_kernel(0, 3 * num, charges, index_offset, sim.r)


@make_parallel_loop
@default_jit
def interpolate_charges_kernel(ix, charges, x, rho):
    """`pyglasma3d_numba_source.parallel.make_parallel_loop`\
    CHANGES THE USAGE AND ARGUMENTS!.

    Signature:
    ```
        interpolate_charges_kernel(
            iter_min, iter_max, charges, x, rho, force_dev=None
        )
    ```
    """
    rho[3 * x + ix] += charges[ix]
