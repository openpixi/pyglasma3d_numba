"""Numba compute device configuration and usage implementation.

The configuration of the target device (CPU with Numba or GPU with CUDA)
is read from the environment variable:

- `MY_NUMBA_TARGET`
    - 'numba' => default
    - 'cuda'
    - 'python'

A toggle for using "fastmath" for mathematical operations is provided
with the environment variable:

- `FASTMATH`
    - '0'
    - '1' => default

It is possible to print verbose system information when importing this
module by setting this environment variable:

- `SYSINFO`
    - '0' => default
    - '1'

During the import process the current configuration is printed.

`default_jit` is a custom numba decorator, that changes its
functionality based on the environment variables mentioned above and
is used to provide a default configuration for the Numba jit compiler.
"""

import os
import sys


# read the environment variables
target = os.environ.get("MY_NUMBA_TARGET", "numba").lower()
fastmath = bool(int(os.environ.get("FASTMATH", 1)))
sysinfo = bool(int(os.environ.get("SYSINFO", 0)))

# test available modules and return device information
try:
    import numba

    if sysinfo:
        from numba.misc import numba_sysinfo

        numba_sysinfo.display_sysinfo()

    try:
        from numba import cuda

        if target == "cuda" and not sysinfo:
            cuda.detect()

    # TODO: specify the exception that cuda throws when not available
    # this seems to be missing in the numba docs
    # on tests: CudaAPIError, CudaSupportError
    # Call to cuInit results in CUDA_ERROR_NO_DEVICE
    except:  # pylint: disable=bare-except # noqa: E722
        if target == "cuda":
            # this is only a problem if target was set to cuda
            print("\nsomething went wrong when importing cuda\n\n")
            raise
        # else:
        # warn the user: this has the consequence, that force_dev=cuda
        # in the code will not work
        print(
            "\nCUDA module not available\n"
            "Please note, that CUDA will not work for this run.\n\n"
        )

except ModuleNotFoundError:
    print(
        "Numba not installed! Please install Numba: http://numba.pydata.org/\n"
        "If you want to continue using standard Python (Warning very slow!)"
        "enter 'yes'\n[yes/no]  ",
        end="",
    )
    if input() == "yes":
        print("\nusing Python execution model\n\n")
        target = "python"
    else:
        sys.exit("\n\napplication quit\nnumba not installed")


# configure the target device
if target in ("cuda", "python", "numba"):
    use_device = target
    print(f"\nUsing {use_device.upper()}")

else:
    sys.exit("\napplication quit\nUnknown Numba target device: " + target)

print("FASTMATH is set to: " + str(fastmath) + "\n\n" + "#" * 58 + "\n\n")


# defining this decorator allows to have a custom jit with default
# arguments that also allows to overwrite these default values on a per
# call basis without having to use the full numba.jit(args) call with
# all required arguments # (including the custom defaults)


def default_jit(func_or_sig=None, **kwargs):
    """numba.jit() alias with specific default arguments.

    Usage is the same as for the numba.jit() decorator. Internally
    numba.jit() is called with the following keyword arguments
    pre-supplied:
    ```
    "nopython": True,
    "nogil": True,
    "fastmath": `FASTMATH`
    ```

    The key value "fastmath" will be the value of the environment
    variable `FASTMATH`.

    All keyword arguments supplied to `default_jit` will be passed on to
    numba.jit() and will overwrite the aforementioned set defaults. For
    any invalid arguments numba.jit() will raise the error.
    Using arguments specific to numba.cuda.jit() does not work.
    `default_jit` does not work with the Numba built-in cuda simulator.

    Args:
        func_or_sig: function or signature to "jit"
        kwargs: Keyword arguments usable with numba.jit() or
          numba.cuda.jit().

    Returns:
        compiler(func_or_sig, **args)
    """
    # first test for python mode
    if use_device == "python":
        if isinstance(func_or_sig, list) or numba.core.sigutils.is_signature(
            func_or_sig
        ):
            # A list of signatures or a single signature is passed;
            # ignore them
            return default_jit()
        # no func or sig, but possible arguments; ignore them
        if func_or_sig is None:
            return lambda a: a

        # a function is passed
        return func_or_sig

    # if cuda or numba mode return jitted func
    if use_device in ("cuda", "numba"):
        # copy args dict
        default_args = {"nopython": True, "nogil": True, "fastmath": fastmath}
        # apply supplied arguments from kwargs
        default_args.update(kwargs)

        # if func_or_sig is empty, numba.jit will return the appropriate
        # wrapper to perform the "decoration" of a decorated function
        # and will "jit" it
        return numba.jit(func_or_sig, **default_args)

    # something went wrong
    return None
