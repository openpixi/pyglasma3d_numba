"""Consistency test.

This package should not be imported. It is designed to work as a
command line script by calling `consistency_tests.py`.
However, if you know what you are doing, the module `evaluation.py` can
technically be imported and used in other code.
"""

import sys
from os import path

# set sys.path to make pyglasma3d_numba_source accessible
sys.path.append(path.dirname(path.dirname(__file__)))

# set __all__ for star imports
__all__ = ["evaluation"]
