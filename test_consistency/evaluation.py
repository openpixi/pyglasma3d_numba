"""Calculate difference values of data files.

A module to calculate differences of two supplied data sets and express
the differences in terms of the digits. The magnitudes of the difference
values represent the number of digits the data sets match.

includes:
function diff_statistics(): calculate value differences of input data
generator function read_textfile_iter(): iterate over a given file and
                                         return a numpy array with data
"""

import numpy as np

from pyglasma3d_numba_source.data_io import read_textfile_iter


def diff_statistics(files_dict, skip_scatter=False):
    """Calculate differences on supplied input files.

    The differences of the supplied data files are calculated by simple
    subtraction. Thereafter they differences are normed according to the
    magnitudes of the input data. The difference values thus can be
    interpreted as differences in digits rather than absolute value
    differences.
    For the calculations the `read_textfile_iter()` function is used.

    3 different statistics on the differences are calculated:
    i) for each iteration on the input files the mean, std and max
       of the value differences are appended the numpy array `results`
    ii) for each iteration on the input files a histogram of the
        differences is calculated and the frequency values are added up
    iii) if `skip_scatter` is set to False, a 2D numpy array is built
         that contains all magnitudes in its first index [0] and all
         difference values for the data in its second index 1.

    Args:
        files_dict: A dictionary containing the names and paths to the
          files to compare:
          ```
          files_dict = { "source1": path1,
                         "source2": path2 }
          ```
          The keys "source1" and "source2" are required.
        skip_scatter: If set to True, the third statistical evaluation
          (see iii)) is skipped. Use this if dealing with a large amount
          of values per iteration step. This will prevent storing all
          difference and data values in memory.

    Returns:
        A Tuple with:
        - its first element as a 2D numpy array with the values of i)
        - its second element as a tuple with the histogram of ii)
          and a list of the bin edges [in that order]
        - its third element as the 2D numpy array built in iii)
    """

    # collect mean, max, std
    results = np.zeros((0, 3))

    if skip_scatter:
        scatter = None
    else:
        # pairs of data1 and diff
        scatter = np.zeros((2, 0))

    # histogram for each sim step
    # bins edges are:
    # [0, 1e-18, 1e-17, 1e-16, 1e-15, 1e-14, 1e-13, 1e-12, 1e-11, 1e-10,
    # 1e-09, 1e-08, 1e-07, 1e-06, 1e-05, 1e-4, 1e-3, 1e-2, 1e-1, 1e+0,
    # 1e+1 ]
    bins = [pow(10, x) for x in range(-18, 2)]
    bins.insert(0, 0)
    hist = np.zeros(len(bins) - 1, dtype=np.int)

    try:

        # get the filepaths from the source dict
        file1 = files_dict["source1"]
        file2 = files_dict["source2"]

    except KeyError as e:
        raise KeyError(
            f"\n\nThe supplied 'files_dict' misses a required key: {e}\n\n"
        ) from e

    # iterate over data file in chunks and and work on the data
    for data1, data2 in zip(read_textfile_iter(file1), read_textfile_iter(file2)):

        # calculate differences
        diff = data2 - data1
        # mask data1 where it is 0
        data1 = np.ma.masked_equal(data1, 0, copy=False)

        # see issue #3342 for pylint, pylint: disable=no-member

        # change data1 values inplace to their magnitudes
        # ( 1e+-(10*x) ) as pure powers of 10
        data1 = np.log10(np.abs(data1, out=data1), where=~data1.mask)
        np.power(10, np.floor(data1, out=data1), out=data1)
        # this version has a bug with the np.log10() function
        # np.power(10, np.floor(np.log10(np.abs(data1, out=data1),
        # out=data1, where=~data1.mask), out=data1), out=data1)

        # normalize diff by the magnitude of data1, except where
        # data1 is masked (where it is 0)
        diff = diff / data1
        # remove diff's mask it has inherited from data1
        diff.mask = np.ma.nomask

        # now take the absolute value of diff
        np.abs(diff, out=diff)
        # set all values > 1 to 1 [= 0 matching digits]
        # less than 0 matching digits does not make sense
        diff[diff > 1] = 1

        # calculate statistics and append to results array
        # perfect matches (diff = 0) will be masked
        results = np.append(
            results,
            (
                (
                    np.mean(np.log10(diff)),
                    np.std(np.log10(diff)),
                    np.amax(np.log10(diff)),
                ),
            ),
            axis=0,
        )

        # sort diff array into bins and add up bin population
        new_hist, _ = np.histogram(diff, bins=bins)
        hist += new_hist

        if diff.size != np.sum(new_hist):
            print(
                "\nWARNING:\tsome values are outside of the bin edges and "
                "were discarded during the binning process\n"
            )

        if scatter is not None:
            # set data1 to 0.0 where masked
            # necessary because of a bug in np.log10 where
            # all masked values are set to 1.0
            data1[data1.mask] = 0.0
            data1.mask = False
            for d, v in zip(diff, data1):
                scatter = np.append(scatter, ((v,), (d,)), axis=1)

    return results, (hist, bins), scatter
