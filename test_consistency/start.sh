#!/usr/bin/env bash

########################################################################
# This script offers selection of different preconfigured testing
# scenarios. First the selected simulation setup will be run. Then the
# output data it produced will be checked using the
# `consistency_tests.py` script (except for option 5).
########################################################################

# exit script on error
set -eE

# change current working dir to base folder of pyglasma3d_numba
cd -P -- "$(dirname -- "${BASH_SOURCE[0]}")"
cd ..
# pwd
# [...]/pyglasma3d_numba

# a loop to select options
choice=11

while [ $choice -eq 11 ]; do

    echo -e "\nplease input the number of the setup to run...\n"
    echo "1 => test pyglasma3d_numba mv_simple setup with fastmath = True"
    echo "2 => test pyglasma3d_numba mv_simple setup with fastmath = False"
    echo "3 => test pyglasma3d_numba mv_simple setup in cuda mode with fastmath = True"
    echo "4 => test pyglasma3d_numba mv_simple setup in cuda mode with fastmath = False"
    echo "5 => time pyglasma3d_numba mv_debug setup in cuda mode with fastmath = True"
    echo "6 => test existing output against original/[...].dat with fastmath = True"
    echo "7 => test existing output against original/[...].dat with fastmath = False"
    echo -e "q => ABORT\n"

    read choice
    echo -n "your choice was: "
    echo $choice
    echo -e "\n\n\n\n"

    # default arguments for the consistency test
    args="-f test_consistency/original/T_mv_trial_simple_cython_textfile.dat \
output/T_mv_trial_simple_textfile.dat -v"
    run_test=True
    run_setup=True
    setup="examples.mv_simple"

    case $choice in
        1)
            args=$args" --fastmath"
            export MY_NUMBA_TARGET=numba
            ;;
        2)
            export FASTMATH=0
            export MY_NUMBA_TARGET=numba
            ;;
        3)
            args=$args" --fastmath"
            ;;
        4)
            export FASTMATH=0
            ;;
        5)
            run_test=False
            setup="examples.mv_debug"
            ;;
        6)
            run_setup=False
            args=$args" --fastmath"
            ;;
        7)
            run_setup=False
            ;;
        q)
            echo "aborting..."
            exit
            ;;
        *)
            echo "this does not match any inputs. please try again"
            echo "------------------------------------------------"
            choice=11
            ;;
    esac

done

# run setup if required
if [[ "$run_setup" == True ]]; then
  time python3 -m $setup
fi

# calc statistics if required
if [[ "$run_test" == True ]]; then

  echo -e "\ncalling 'consistency_test.py' with the following arguments:" \
"\n\t consistency_tests.py $args\n"
  test_consistency/consistency_tests.py $args

fi
