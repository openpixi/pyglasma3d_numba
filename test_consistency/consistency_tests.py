#!/usr/bin/env python

"""A python script to plot and test difference data.

This python script is meant to be executed from the command line, either
directly `./consistency_tests.py` or via `python consistency_tests.py`.
For available command line options use the '-h' or '--help' flags.

The command line argument '-f' / '--files' is required and takes two
paths to the data files to analyze as string arguments.

The differences are calculated using the `evaluation.py` module.
"""

import argparse
import math
import os
import sys
import unittest
import matplotlib.gridspec as gridspec
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import matplotlib.cm as cm

if __name__ != "__main__":
    raise ImportError(
        "\nWARNING!\nThis `test_consistency.py` script "
        "cannot be imported or called as a secondary module.\n",
        name="consistency_tests.py",
    )

try:
    # add repository root to sys.path
    sys.path.insert(0, os.path.dirname(sys.path[0]))
    import evaluation
    import TestRunner
except ModuleNotFoundError as e:
    raise ModuleNotFoundError(
        f'\n\nThe module "{e.name}" could not be found in\n'
        f"{sys.path=}\n"
        "This could be, because this script was called from "
        "the wrong working directory.\nPlease make sure to be "
        "located in the root folder of the repository.\n",
        name=e.name,
        path=e.path,
    ) from e


# deal with command line arguments
parser = argparse.ArgumentParser(
    description=__doc__,
    formatter_class=argparse.RawTextHelpFormatter,
    allow_abbrev=False,
    add_help=False,
)
parser.add_argument(
    "-h",
    "--help",
    action="help",
    default=argparse.SUPPRESS,
    help="show this help message and exit.\n ",
)
parser.add_argument(
    "-f",
    "--files",
    nargs=2,
    required=True,
    dest="files",
    metavar="path/to/file",
    help="specify two files to compare\nthis argument is required\n ",
)
# related to output
parser.add_argument(
    "-v",
    "--verbose",
    action="store_true",
    dest="verbose",
    help="print evaluation results to terminal\n" "default: not set\n ",
)
parser.add_argument(
    "--hide",
    action="store_true",
    dest="hide",
    help="set this flag to skip the third plot\n"
    "this is useful if there is not enough system memory "
    "to save all the data to compare\n"
    "default: not set\n ",
)
parser.add_argument(
    "-o",
    "--output",
    dest="output",
    metavar="path/to/file",
    help="specify a file to write evaluation data to\n" "default: None\n ",
)
parser.add_argument(
    "-g",
    "--graphs",
    dest="graphs",
    metavar="path/to/file or True/False",
    default=True,
    help="prepare 3 (or 2 with option --hide) plots of data\n"
    "if set to True, plots are shown interactively\n"
    "if set to a string representing a path, plots are "
    "saved to the given path\n"
    "default: True\n ",
)
# related to unittests
parser.add_argument(
    "--skiptests",
    action="store_true",
    dest="skiptests",
    help="skip all unittests\ndefault: not set\n ",
)
parser.add_argument(
    "-s",
    "--step",
    dest="index",
    type=int,
    default=-1,
    metavar="step",
    help="specify the iteration step on which to perform the unittests\n"
    "negative values count backwards\n"
    "default: -1, the last iteration step\n ",
)
group = parser.add_mutually_exclusive_group()
group.add_argument(
    "-p",
    "--precision",
    dest="digits",
    type=int,
    default=15,
    choices=range(1, 19),
    help='set exponent "p" used for calculating precision for unittests: 1e-"p"\n'
    "this option conflicts with --fastmath\n"
    "default: 15\n ",
)
group.add_argument(
    "--fastmath",
    action="store_const",
    const=10,
    dest="digits",
    help="change accuracy in unittests to 1e-10\n"
    "this option conflicts with -p / --precision\ndefault: not set\n ",
)
args = parser.parse_args()

# build a dict with filenames and paths for the supplied files
files_dict = {"source1": args.files[0], "source2": args.files[1]}

# calculate data from evaluation
all_results, hist, scatter = evaluation.diff_statistics(files_dict, args.hide)


# print / plot data
print(
    "\n\n",
    "#" * 107,
    "\n\n"
    "statistics on the differences of the input data for files:\n\n"
    f'{os.path.basename(files_dict["source1"])}',
    " " * 4,
    "and",
    " " * 4,
    f'{os.path.basename(files_dict["source2"])}' "\n\n",
)

if args.verbose:
    # figure out universal length of iter step count: 00XX
    max_iter = len(all_results)
    max_iter_mag = math.floor(math.log10(max_iter)) + 1
    iter_format = "0" + str(max_iter_mag) + "d"

    print(
        "iter step", " " * (max_iter_mag - 1), "mean", " " * 27, "min", " " * 28, "std"
    )
    for i, res in enumerate(all_results):
        print(
            f"{i + 1:{iter_format}}",
            " " * 8,
            f"{res[0]:.17e}",
            " " * 8,
            f"{res[2]:.17e}",
            " " * 8,
            f"{res[1]:.17e}",
        )

    print("\nhistorgram data:\nbin edges:", end=" ")
    for x in hist[1]:
        print(f"{x:1.0e}", end=" " * 4)

    print("\nfrequency:", end=" " * 6)
    for x in hist[0]:
        print(x, end=" " * (9 - len(str(x))))

    print("\n\n", "#" * 107, "\n")

try:
    if args.output is not None:
        with open(args.output, "w") as f:
            for i, res in enumerate(all_results):
                f.write(
                    f"{i + 1}"
                    + " " * 8
                    + f"{res[0]:.17e}"
                    + " " * 8
                    + f"{res[2]:.17e}"
                    + " " * 8
                    + f"{res[1]:.17e}\n"
                )

            for x in hist[1]:
                f.write(f"{x:1.0e}" + " " * 4)
            f.write("\n")
            for x in hist[0]:
                f.write(f"{x}" + " " * 4)
            f.write("\n")

    if args.graphs and args.graphs != "False":

        fig = plt.figure(figsize=(20, 12))
        gs = gridspec.GridSpec(nrows=2, ncols=2, hspace=1 / 3)

        # plot diff for each sim step
        plt_time = fig.add_subplot(gs[0, :])

        plt_time.plot(
            [all_results[i][0] for i in range(len(all_results))],
            label="mean",
            color="b",
        )
        # plt_time.plot([all_results[i][1]
        # for i in range(len(all_results))], label='std')
        plt_time.plot(
            [all_results[i][2] for i in range(len(all_results))], label="min", color="y"
        )
        plt_time.fill_between(
            list(range(len(all_results))),
            y1=[all_results[i][0] for i in range(len(all_results))],
            y2=[all_results[i][0] + all_results[i][1] for i in range(len(all_results))],
            label="mean - \u03C3",
            alpha=0.5,
        )
        plt_time.axhline(
            -args.digits,
            ls="--",
            lw=2,
            c="m",
            label=f"limit: {args.digits}",
        )
        plt_time.grid(which="both", axis="y")
        plt_time.set_xlim(0, len(all_results) - 1)
        plt_time.set_ylim(1, -18)
        plt_time.set_xlabel("Simulation Step")
        plt_time.set_ylabel("Matching Digits")
        # calculate ticks for fixed number of ticks
        tickstep = math.ceil(len(all_results) / 50)
        xticks = list(range(0, len(all_results) - 1, tickstep))
        xticks.append(len(all_results) - 1)
        xticklabels = list(range(1, len(all_results), tickstep))
        xticklabels.append(len(all_results))
        yticks = plt_time.get_yticks()
        yticklabels = [f"{-x if x < 0.0 else 0}" for x in yticks]
        plt_time.set_yticklabels(yticklabels)
        plt_time.set_xticks(xticks)
        plt_time.set_xticklabels(xticklabels)
        handles, labels = plt.gca().get_legend_handles_labels()
        order = [0, 3, 1, 2]
        plt_time.legend(
            [handles[i] for i in order], [labels[i] for i in order], loc="lower right"
        )
        plt_time.set_title("Matching Digits for each Simulation Step")

        # plot histogram of diff values
        plt_hist = fig.add_subplot(gs[1, 0])

        _, _, patches = plt_hist.hist(
            range(len(hist[0])),
            bins=range(0, len(hist[1])),
            weights=hist[0],
            label=f"# of data points:\n{hist[0].sum()}",
        )
        plt_hist.axvline(19 - args.digits, ls="--", lw=2, c="m")
        plt_hist.set_xlim(len(hist[0]), 0)
        plt_hist.set_xticks(range(len(hist[1])))
        # set xticklabels
        xticklabels = [
            f"{-1 * math.log10(x) if x !=1.0 else 0:.0f}" for x in hist[1][1:]
        ]
        xticklabels.insert(0, "all")
        xticklabels[-1] = "none"
        plt_hist.set_xticklabels(xticklabels)
        plt_hist.set_xlabel("Matching Digits")
        plt_hist.set_title("Histogram of Matching Digits")
        # attach a label above each bar, displaying its height
        for rect in patches:
            height = rect.get_height()
            plt_hist.annotate(
                f"{int(height):d}",
                xy=(rect.get_x() + rect.get_width() / 2, height),
                xytext=(0, 3),
                textcoords="offset points",
                ha="center",
                va="bottom",
            )
        plt_hist.legend()

        if scatter is not None:
            # scatter plot diff values and data magnitudes
            plt_scat = fig.add_subplot(gs[1, 1])

            # set up colorbar
            cmap = cm.ScalarMappable(cmap="rainbow")
            cbar = plt.colorbar(
                mappable=cmap,
                orientation="horizontal",
                aspect=40,
                ax=plt_scat,
                fraction=0.045,
            )

            # split up scatter array and colorize
            num_points = scatter.shape[1]
            step = int(num_points / len(all_results))
            for val in range(0, num_points, step):
                plt_scat.scatter(
                    scatter[0][val : val + step],
                    scatter[1][val : val + step],
                    marker="2",
                    color=cmap.to_rgba(
                        val / (num_points - step + 1), alpha=0.7, norm=False
                    ),
                )

            # calculate ticks and labels for colorbar
            c_ticks = [x * step / (num_points - step + 1) for x in xticks[::3]]
            c_ticks.append(xticks[-1] * step / (num_points - step + 1))
            cbar.set_ticks(c_ticks)
            cbar.set_ticklabels(
                [f"{x * (len(all_results) - 1) + 1:.0f}" for x in c_ticks]
            )
            cbar.set_label("Simulation Step")

            plt_scat.loglog()
            plt_scat.axhline(float(f"1e-{args.digits}"), ls="--", lw=2, c="m")
            plt_scat.set_ylim(1e1, 1e-18)
            max_mag = max(scatter[0])
            min_mag = min(x if x != 0.0 else max_mag for x in scatter[0]) / 10
            plt_scat.set_xlim(min_mag)
            # calculate tick positions for 11 ticks
            mag_diff = int(math.log10(max_mag) - math.log10(min_mag))
            tickstep = math.ceil(mag_diff / 10)
            xticklabels = [
                math.pow(10, x)
                for x in range(
                    int(math.log10(min_mag)), int(math.log10(max_mag)), tickstep
                )
            ]
            xticklabels.append(math.pow(10, int(math.log10(max_mag)) + tickstep // 2))
            plt_scat.set_xticks(xticklabels)
            plt_scat.set_xticklabels(xticklabels)
            plt_scat.xaxis.set_major_formatter(ticker.FormatStrFormatter("%.0e"))
            yticks = plt_scat.get_yticks()
            yticklabels = [
                f"{-1 * math.log10(x) if x != 1.0 else 0:.0f}" for x in yticks
            ]
            plt_scat.set_yticklabels(yticklabels)
            plt_scat.set_xlabel("Magnitudes of Data Values")
            plt_scat.set_ylabel("Matching Digits")
            plt_scat.set_title("Matching Digits vs Data Value Magnitudes")

        if isinstance(args.graphs, str):
            fig.savefig(args.graphs)
            print(f"saving plot to file {args.graphs}\n\n")
        else:
            plt.show()

except OSError as e:
    sys.exit(f'\n\ncannot write output to the supplied file:\t"{e.filename}"\n{e}\n\n')


class TestDataConsistency(unittest.TestCase):
    """Unittests on difference values of data for an iteration step.

    The differences of the supplied data are calculated using the
    `evaluation.py` module and statistically evaluated and tested
    for relevance.

    Requires the variables `files_dict`, `all_results`, `hist`,
    `scatter`,`args.fastmath`, `args.index`, `args.hide`, `args.digits`
    from the global namespace.
    """

    @classmethod
    def setUpClass(cls):
        """Generate all data to test.

        Calculate the statistics on the differences of the supplied data
        using the function `diff_statistics()` from the module
        `evaluation.py` and store them in the class attributes
        `all_results`, `hist` and `scatter`.
        """

        # flush the output to clear leftovers if called after
        # finished simulations which print a lot
        sys.stdout.flush()

        # get the results from the evaluation
        cls.all_results, cls.hist, cls.scatter = all_results, hist, scatter

        try:
            cls.results = cls.all_results[args.index]
        except IndexError:
            sys.exit(
                "\nError: the specified simulation step exceeds the "
                "number of steps available in the data file\n"
            )

    def test_min_accuracy(self):
        """Check if minimum is above digits limit."""

        # using results[2], the min
        self.assertTrue(
            args.digits < -self.results[2],
            msg=f"min above {args.digits} digits",
        )

    def test_mean_accuracy(self):
        """Check if mean is above digits limit."""

        # using results[0], the mean
        self.assertTrue(
            args.digits < -self.results[0],
            msg=f"mean above {args.digits} digits",
        )


# launch the custom test runner
if not args.skiptests:
    TestRunner.main()
