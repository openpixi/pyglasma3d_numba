# Python Package [test_consistency][folder-link]


This [test_consistency][folder-link] package provides tools to easily compare data files with  
numerical data. Its main purpose is to assist with the development of [Pyglasma3d_Numba][root-link].  
The [User Guide][this-wiki-link] is provided via the [repository Wiki][wiki-link]. It 
contains detailed intructions  
and background information.


### Requirements

This package requires:  
- Python version 3.8.1 or higher  

Additionally the following external modules are required:  

| Package                       | Version &#8805; |
| ----------------------------- | :-------------: |
| [matplotlib][matplotlib-link] |      3.1.2      |
| [numpy][numpy-link]           |     1.18.1      |


## Quick-Start Guide

### [`start.sh`][start-link] Script:
The [`start.sh`][start-link] script provides a set of preconfigured test setups to choose  
from for the [`consistency_tests.py`][tests-link] script. Upon execution, first the selected  
[simulation setup][sst-link] will be run and then the data it generated will be tested.  
Preparing the reference data file in advance is necessary for proper execution.  
Please consult the section [Bash Script `start.sh`][start-wiki-link] of the [repository Wiki][wiki-link] for details.

Use the [`start.sh`][start-link] script with the bash shell:  
```bash
$ bash start.sh
```
Or by calling it directly from the command line:
```bash
$ ./start.sh
```
Then follow the prompts on the screen.


### [`consistency_tests.py`][tests-link] Script

The [`consistency_tests.py`][tests-link] script provides a feature rich testing and analysis  
tool for comparing the numerical data of two files. It is built as a command line  
tool and offers customization options. Within the [test_consistency][folder-link] package  
this script is also used by the [`start.sh`][start-link] bash script.

The [`consistency_tests.py`][tests-link] script can be called directly, as long as all modules of  
the [test_consistency][folder-link] package are importable from the current working directory. Use   
the `-f` / `--files` parameter to specify the two data files to compare:
```
$ ./consistency_tests.py -f path/to/file1 path/to/file2
```
```
$ python3 consistency_tests.py -f path/to/file1 path/to/file2
```
```
$ python3 -m consistency_tests -f path/to/file1 path/to/file2
```
Use the `-h` / `--help` flag for all available parameters or see the section [Python  
Script `consistency_tests.py`][tests-wiki-link] of the [repository Wiki][wiki-link] for details.


## Output

The scripts provided with this package log output to the execution shell and  
generate graphs. Please see the section [Understanding the Test Results][res-wiki-link] of  
the [repository Wiki][wiki-link] for detailed descriptions.


[//]: # (references: )  
[folder-link]: test_consistency/
[root-link]: ./
[start-link]: test_consistency/start.sh
[tests-link]: test_consistency/consistency_tests.py
[sst-link]: examples/
[wiki-link]: https://openpixi.gitlab.io/pyglasma3d_numba/
[this-wiki-link]: https://openpixi.gitlab.io/pyglasma3d_numba/docs/pyglasma3d-numba/test-consistency/
[start-wiki-link]: https://openpixi.gitlab.io/pyglasma3d_numba/docs/pyglasma3d-numba/test-consistency/test-consistency-start/
[tests-wiki-link]: https://openpixi.gitlab.io/pyglasma3d_numba/docs/pyglasma3d-numba/test-consistency/test-consistency-tests/
[res-wiki-link]: https://openpixi.gitlab.io/pyglasma3d_numba/docs/pyglasma3d-numba/test-consistency/#understanding-the-test-results
[matplotlib-link]: https://pypi.org/project/matplotlib/
[numpy-link]: https://pypi.org/project/numpy/
