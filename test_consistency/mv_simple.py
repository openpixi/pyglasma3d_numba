"""A script for Au-Au collisions in the MV-model.

The main input parameters are:
the collision energy sqrt{s},
the infrared regulator m and
the box dimensions LT, LL

This is a simplified version of the mv.py script from the cython
version (https://gitlab.com/monolithu/pyglasma3d). Its purpose is to
provide a reference setup for the cython version to use for the
consistency tests.

Changes to the original version:
- Grid size is reduced to: 128, 32, 32
- Max iterations is reduced to: 1 * nx // 3
- A fixed random seed
- Reformating
"""

from __future__ import print_function

import time

import numpy as np

# path for Cython version is not resolveable
# pylint: disable=import-error
import pyglasma3d.cy.interpolate as interpolate
import pyglasma3d.cy.mv as mv
from pyglasma3d.core import Simulation


# data will end up in `./output/T_<run>.dat`
run = "mv_trial_simple"

# grid size (make sure that ny == nz)
nx, ny, nz = 128, 32, 32

# transverse and longitudinal box widths [fm]
LT = 6.0
LL = 6.0

# collision energy [MeV]
sqrts = 200.0 * 1000.0

# infrared and ultraviolet regulator [MeV]
m = 200.0
uv = 10.0 * 1000.0

# ratio between dt and aL [int, multiple of 2]
steps = 4

# option for debug
debug = True

# The rest of the parameters are computed automatically.

# constants
hbarc = 197.3270  # hbarc [MeV*fm]
R_Au = 7.27331  # Gold nuclear radius [fm]

# determine lattice spacings and energy units
aT_fm = LT / ny
E0 = hbarc / aT_fm
aT = 1.0
aL_fm = LL / nx
aL = aL_fm / aT_fm
a = [aL, aT, aT]
dt = aL / steps

# determine initial condition parameters
gamma = sqrts / 2000.0
Qs = np.sqrt((sqrts / 1000.0) ** 0.25) * 1000.0
alphas = 12.5664 / (18.0 * np.log(Qs / 217.0))
g = np.sqrt(12.5664 * alphas)
mu = Qs / (g * g * 0.75) / E0
uvt = uv / E0
ir = m / E0
sigma = R_Au / (2.0 * gamma) / aL_fm * aL
sigma_c = sigma / aL

# number of evolution steps (max_iters) and output file path
max_iters = 1 * nx / 3
file_path = "./output/T_" + run + ".dat"

# Initialization

# set random seed for run to run comparability
np.random.seed(10)

# initialize simulation object
dims = [nx, ny, nz]
s = Simulation(dims=dims, a=a, dt=dt, g=g, iter_dims=[1, nx - 1, 0, ny, 0, nz])
s.debug = debug

t = time.time()
s.log("Initializing left nucleus.")
v = mv.initialize_mv(
    s, x0=nx * 0.25 * aL, mu=mu, sigma=sigma, mass=ir, uvt=uvt, orientation=+1
)
interpolate.initialize_charge_planes(s, +1, 0, nx / 2)
s.log("Initialized left nucleus.", round(time.time() - t, 3))

t = time.time()
s.log("Initializing right nucleus.")
v = mv.initialize_mv(
    s, x0=nx * 0.75 * aL, mu=mu, sigma=sigma, mass=ir, uvt=uvt, orientation=-1
)
interpolate.initialize_charge_planes(s, -1, nx / 2, nx)
s.log("Initialized right nucleus.", round(time.time() - t, 3))

s.init()

# Simulation loop

t = time.time()
for it in range(max_iters):
    # this for loop moves the nuclei exactly one grid cell
    for step in range(steps):
        t = time.time()
        s.evolve()
        print(s.t, "Complete cycle.", round(time.time() - t, 3))

    t = time.time()
    s.write_energy_momentum(max_iters, file_path)
    print(s.t, "Writing energy-momentum tensor to file.", round(time.time() - t, 3))
