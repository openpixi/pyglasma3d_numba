---
bookHidden: true
---

# Pyglasma3d_Numba Wiki

**Full documentation and help is located [here](https://openpixi.gitlab.io/pyglasma3d_numba/).**

---

## 3+1D Glasma Simulations
As part of the [OpenPixi simulators](http://www.openpixi.org/), the Pyglasma3d_Numba code allows to calculate the  
formation and evolution of the Glasma in 3+1D. The Glasma is an early stage in heavy ion  
collisions, where hadrons are melted into quarks and gluons. This stage is modeled by  
the color glass condensate, an effective theory based on classical Yang-Mills field theory  
coupled to external color currents. The colliding nuclei are described using a modified  
version of the McLerran-Venugopalan model that takes the finite thickness of the color  
charge sheets into account. Numerically solving the resulting equations of motion relies  
on real-time lattice gauge theory and the colored particle-in-cell method. Pyglasma3d_Numba  
is a port of [pyglasma3d](https://gitlab.com/monolithu/pyglasma3d) to Numba and features GPU acceleration based on 
CUDA.


## Quick-start guide

1. Install dependencies:
   - for [conda][conda-ref] environments use:  
     ```
     $ conda env create -f conda-environment.yml
     $ conda activate pyglasma3d_numba
     ```
   - for [pip][pip-ref] Python venvs use:  
     ```
     $ python3 -m venv .venv
     $ source .venv/bin/activate
     $ pip install -r pip-requirements.txt
     ```
   - GPU acceleration on NVIDIA GPUs additionally requires the [proprietary driver](https://www.nvidia.com/en-us/drivers/unix/)  
     <br>

2. Launch a setup:
   - From the root directory of the repository launch the setup module:  
     ```
     $ python3 -m examples.mv [options]
     ```
   - To list all available command line arguments use:  
     ```
     $ python3 -m examples.mv --help
     ```


---
**Full documentation and help is located [here](https://openpixi.gitlab.io/pyglasma3d_numba/).**
