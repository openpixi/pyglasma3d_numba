---
menu:
  before:
    name: News
    weight: 2
title: News
---

<!-- #TODO:
    The "News" section will contain:
    - new releases (a collection of release notes)
      with more information than on the simple list of release notes on gitlab
    - scientific news, like papers and important results connected with the project

    - currently it contains the sample from hugo
      use the headers of the sample files to compile a "common" header
      that will be used across all posts
-->
