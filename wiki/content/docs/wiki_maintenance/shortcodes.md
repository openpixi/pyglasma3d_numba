---
title: "Shortcodes Reference"
weight: 20
---

# Links

## Absolute Links

Absolute links start from the "root" path of the wiki: the "content" directory.  
All links starting with a slash "/" will be relative to that directory.

The simple markdown syntax as shown below should work fine:  
Example: [Hints](/docs/wiki_maintenance/shortcodes#hints)
```
[Hints](/docs/wiki_maintenance/shortcodes#hints)
```

However that feature is marked "experimental". If there are problems, fall  
back to the Hugo shortcode syntax:  
Example: [Hints]({{< relref "/docs/wiki_maintenance/shortcodes#hints" >}})
```
[Hints]({{</* relref "/docs/wiki_maintenance/shortcodes#hints" */>}})
```

For links just to the page without targeting a specific section:  
Default Markdown Example: [Shortcodes](/docs/wiki_maintenance/shortcodes)
```
[Shortcodes](/docs/wiki_maintenance/shortcodes)
```
Hugo shortcode Example: [Shortcodes]({{< relref "/docs/wiki_maintenance/shortcodes" >}})
```
[Shortcodes]({{</* relref "/docs/wiki_maintenance/shortcodes" */>}})
```

## Relative Links on the same Page

The simple markdown syntax as shown below should work fine:  
Example: [Hints](./shortcodes#hints)
```
[Hints](./shortcodes#hints)
```

However that feature is marked "experimental". If there are problems, fall  
back to the Hugo shortcode syntax:  
Example: [Hints]({{< relref "./shortcodes#hints" >}})
```
[Hints]({{</* relref "./shortcodes#hints" */>}})
```

Omitting the page specification after the dot "." will reference the "home page"  
(`_index.md`) file of the current directory.

Default Markdown Example: [Basic Setup](.#basic-setup)
```
[Basic Setup](.#basic-setup)
```
Hugo shortcode Example: [Basic Setup]({{< relref ".#basic-setup" >}})
```
[Basic Setup]({{</* relref ".#basic-setup" */>}})
```

### Manually set Heading Anchors

If there are multiple headings with the same name on one page, the automatically  
created anchors are not uniquely referenceable with custom links. In that case  
manually set a heading anchor:

```
My Heading {#my-heading-anchor}
```


# Hints

Hint shortcode can be used as hint/alerts/notification block.  
There are 3 colors to choose: `info`, `warning` and `danger`.

```tpl
{{</* hint [info|warning|danger] */>}}
**Markdown content**  
Lorem markdownum insigne. Olympo signis Delphis! Retexi Nereius nova develat
stringit, frustra Saturnius uteroque inter! Oculis non ritibus Telethusa
{{</* /hint */>}}
```

## Example

{{< hint info >}}
**Markdown content**  
Lorem markdownum insigne. Olympo signis Delphis! Retexi Nereius nova develat
stringit, frustra Saturnius uteroque inter! Oculis non ritibus Telethusa
{{< /hint >}}

{{< hint warning >}}
**Markdown content**  
Lorem markdownum insigne. Olympo signis Delphis! Retexi Nereius nova develat
stringit, frustra Saturnius uteroque inter! Oculis non ritibus Telethusa
{{< /hint >}}

{{< hint danger >}}
**Markdown content**  
Lorem markdownum insigne. Olympo signis Delphis! Retexi Nereius nova develat
stringit, frustra Saturnius uteroque inter! Oculis non ritibus Telethusa
{{< /hint >}}

# KaTeX

KaTeX shortcode let you render math typesetting in markdown document. See [KaTeX](https://katex.org/)

## Example
{{< columns >}}

```latex
{{</* katex [display] [class="text-center"]  */>}}
x = \begin{cases}
   a &\text{if } b \\
   c &\text{if } d
\end{cases}
{{</* /katex */>}}
```

<--->

{{< katex >}}
x = \begin{cases}
   a &\text{if } b \\
   c &\text{if } d
\end{cases}
{{< /katex >}}

{{< /columns >}}

## Display Mode Example

Here is some inline example: {{< katex >}}\pi(x){{< /katex >}}, rendered in the same line. And below is `display` example, having `display: block`
{{< katex display >}}
x = \begin{cases}
   a &\text{if } b \\
   c &\text{if } d
\end{cases}
{{< /katex >}}
Text continues here.
