---
title: Bash Script `start_telemetry_run.sh`
---

# Bash Script `start_telemetry_run.sh`

## Contents

- [User Guide](./telemetry-start#user-guide)
  - [Execution](./telemetry-start#execution)
    - [`HOME` Environment Variable](./telemetry-start#home-environment-variable)
    - [Command Line Arguments](./telemetry-start#command-line-arguments)
  - [Logging Tools](./telemetry-start#logging-tools)
    - [`/proc/meminfo`](./telemetry-start#procmeminfo)
    - [`mpstat(1)`](./telemetry-start#mpstat(1))
    - [`nvidia-smi`](./telemetry-start#nvidia-smi)
  - [Generated Logfiles](./telemetry-start#generated-logfiles)
  - [`/proc/meminfo` File](./telemetry-start#procmeminfo-file)

* * * *

## User Guide

The [`start_telemetry_run.sh`][file-link] script provides a full telemetry suite for 
analyzing  
[simulation setup][sst-link] runs. The suite uses third party logging tools and system information  
files (see ['Logging Tools'](./telemetry-start#logging-tools)) to produce 6 [logfiles](./telemetry-start#generated-logfiles). Those [files](./telemetry-start#generated-logfiles) are then passed on to the  
[`telemetry_plots`][plot-link] module to generate 6 [plots][plots-link] (see ['Python Module `telemetry_plots`'][plot-link]).

The [logged data](./telemetry-start#generated-logfiles) contains:
- CPU utilization for each core and average over all cores
- System memory usage
- GPU utilization
- GPU memory utilization and usage

In addition to system telemetry data, 5 [logfiles](./telemetry-start#generated-logfiles) containing information about the  
simulation run are saved. These include:
- Messages to `stdout` (the simulation log)
- Configuration parameters for the [simulation setup][sst-link]
- Starttime, endtime and duration measurements

This script is hardcoded to use the [`mv.py`][ssmvg-link] setup. This can be changed by  
modifying [line 164][file-link-l164] and changing 'mv' to any other available [simulation 
setup][sst-link].

The [`start_telemetry_run.sh`][file-link] uses the custom symbolic link `python3_mv_gpu` to  
the python interpreter to easily identify the processes that belong to the active simulation  
run. This name will carry over to all processes on the host (CPU) and (GPU) device.  
The symlink will link to the first `python3` found in the current `PATH` and will be located  
in the same folder as the link target.  
This symlink will be created every time this script is executed, but it will not be deleted  
when this script finishes. If the user executing this script does not have the privileges  
to write to the location of `python3`, the script will fail with the following message:
```
Failed to create custom symlink for python.
```
It is not recommended to use this script as the super user, as this might create the symlink  
in a system wide accessible location. Instead create and use a virtual environment for the  
current user, so that the created symlink is only visible when using that virtual environment.

### Execution

Use this script with the bash shell:
```
$ bash start_telemetry_run.sh
```
Or by calling it directly from the command line:
```
$ ./start_telemetry_run.sh
```
For the latter case the bash shell set in the current environment is used. It is  
determined by using the `env` command:
```
/usr/bin/env bash
```

This script can be executed from any working directory. It is not required to change  
into the [`telemetry`][folder-link] folder.

The [`start_telemetry_run.sh`][file-link] script will print status information to the execution  
shell. A sample output for a successful run is provided below. The basic functionality can  
be split into three parts:  
1. All folders and [logfiles](./telemetry-start#generated-logfiles) are prepared and all [logging tools](./telemetry-start#logging-tools) are started
2. The simulation run is executed ([`mv.py`][ssmvg-link] setup)
3. Graphs for visualizing the telemetry data are plotted ([`telemetry_plots`][plot-link] module)

The first part will have finished successfully when the third status message appears. The  
second part will have finished successfully when the sixth status message appears. And the  
third part will have finished together with the whole script when the last status message  
appears.

```
$ ./start_telemetry_run.sh

setting up telemetry files

all loggers running

starting simulation run

simulation run finished

quitting telemetry log

plotting graphs

all plots done

script finished
```

The [logging tools](./telemetry-start#logging-tools) are controlled by this script as background processes. If any of the  
[tools](./telemetry-start#logging-tools) fails during setup, all [loggers](./telemetry-start#logging-tools) will be stopped and this script will terminate with  
exit code 1. The [logfiles](./telemetry-start#generated-logfiles) for that run will be rendered unusable and have to be sorted  
out by the user manually. The same holds true if the execution of the [simulation setup][sst-link]  
returns a non-zero exit code.

All [loggers](./telemetry-start#logging-tools) are configured to run on a single CPU core by using the [`taskset(1)`][taskset-link]  
command. The default core index is set to 1 on [line 75][file-link-l75]. In theory, confining background  
tasks to "weaker" cores frees up "stronger" cores for compute heavy tasks. The core with  
index 0 will typically be the "strongest" core. For the best results, the default setting should  
be adjusted to the target hardware.

#### `HOME` Environment Variable

The [`start_telemetry_run.sh`][file-link] script will use the path set in the `HOME` environment  
variable as 'root' to source the [simulation code][proj-link] files as well as to save the [logfiles](./telemetry-start#generated-logfiles). Upon  
execution, first the path set in `HOME` will be checked, if it contains the folder  
`pyglasma3d_numba`. This folder should be a clone of the [project git repository][proj-link]. If no   
such folder is found, the script will exit with the following error message:
```
ERROR
code resource location '${HOME}/pyglasma3d_numba' does not exits
```
where `${HOME}` will get expanded with the value it had during runtime.

The [logfiles](./telemetry-start#generated-logfiles) will be saved in a subfolder of the folder `HOME/pyglasma3d_numba_stats/`,  
with the current timestamp as its name. The `pyglasma3d_numba_stats/` folder is not  
required to exist when launching the script. It will be created if necessary.

```
HOME/
├── pyglasma3d_numba/
│     ├─ ...
└── pyglasma3d_numba_stats/
       ├─ ...
```

On a standard linux installation of any distribution the `HOME` environment variable is  
pre-set to the home folder of the current user. However, this variable can be changed for  
the current execution of this script by either exporting a new value in the current shell  
before launching this script:
```
$ export HOME=new/custom/path
```
Or by directly setting the value at launch (recommended):
```
$ HOME=new/custom/path ./start_telemetry_run.sh
```

#### Command Line Arguments

The [`start_telemetry_run.sh`][file-link] script accepts arbitrary command line arguments. Every  
argument supplied on execution will be logged in the [logfile](./telemetry-start#generated-logfiles) `CMD_ARGS_$now.dat` and  
passed on to the [simulation setup][sst-link] ([`mv.py`][ssmvg-link]). Handling of invalid arguments will be taken  
care of by the [simulation setup][sst-link] after all [logfiles](./telemetry-start#generated-logfiles) have been initialized. If invalid arguments  
lead to a non-zero exit code of the [simulation setup][sst-link], this will again lead to unusable [logfiles](./telemetry-start#generated-logfiles)  
for the current run, which have to be sorted out manually.


### Logging Tools

The following logging tools are used to generate 6 [logfiles](./telemetry-start#generated-logfiles) with system telemetry data:

#### `/proc/meminfo`

System memory usage information is collected by periodically polling the [`meminfo`][meminfo-link] file that  
is located on the [`/proc`][meminfo-link] (process information) pseudo-filesystem. The command used is:
```
$ bash -c "while sleep 1; do \
(date +%Y/%m/%d\ %T && sed -n -e 1,2p -e 4,5p -e 21p -e 23p /proc/meminfo) | \
tr '\n' '\t'; echo; done >> $HOME/pyglasma3d_numba_stats/$now/sys_mem_$now.dat"
```
This will trigger an infinite while loop that loops once every second. Each time the  
lines 1, 2, 4, 5, 21 and 23 are read from [`meminfo`][meminfo-link] and are written to the [logfile](./telemetry-start#generated-logfiles)  
`sys_mem_$now.dat` with a timestamp attached and all newlines replaced with tabs.  
The resulting file follows this scheme:
```
YYYY/MM/DD HH:MM:SS   "MemTotal:" MEM_tot "kB"   "MemFree:" MEM_free "kB" "Buffers:" Buff "kB" Cached:" cache "kB"   "Shmem:" shmem "kB" "SReclaimable:" src "kB"
```

For the above command to work, the [`meminfo`][meminfo-link] file has to have a certain format. For  
reference a compatible file is provided at the [end of this page](./telemetry-start#procmeminfo-file). The content of the  
[`meminfo`][meminfo-link] file changes based on the version of the linux kernel and kernel compilation  
flags set. The [reference file](./telemetry-start#procmeminfo-file) is based on kernel version 4.19.12 with no extra flags set, that  
would change the [`meminfo`][meminfo-link] file. For a list of all related compile flags please see the  
`meminfo` section of the manpage for [`proc(5)`][meminfo-link].


#### `mpstat(1)`

Utilization information for the CPU is collected by using the [`mpstat(1)`][mpstat-link] utility. The  
command used is:
```
$ S_TIME_FORMAT='ISO' mpstat -u -P ALL 1 >> \
$HOME/pyglasma3d_numba_stats/$now/sys_util_$now.dat
```
This will trigger reporting of CPU utilization in 1 second intervals for all cores as well  
as an average over all cores. The data will be collected in the `sys_util_$now.dat`  
[logfile](./telemetry-start#generated-logfiles).  
Setting the environment variable `S_TIME_FORMAT` to 'ISO' ensures the logged timestamps  
have the same format as timestamps from other logfiles.

The resulting [logfile](./telemetry-start#generated-logfiles) will have on its first line a summary of the basic system properties.  
Following that will be blocks of utilization data separated by a blank line. Each block will  
have a number of lines:  
A header line, a summary line and one line for each core.
```
Linux 4.19.0-0.bpo.1-amd64 (server) 	2020-02-05 	_x86_64_	(12 CPU)

HH:MM:SS XM   CPU_id   %usr   %nice   %sys   %iowait   %irq   %soft   %steal   %guest   %gnice   %idle
HH:MM:SS XM   ALL   %usr   %nice   %sys   %iowait   %irq   %soft   %steal   %guest   %gnice   %idle
HH:MM:SS XM   CPU_0   %usr   %nice   %sys   %iowait   %irq   %soft   %steal   %guest   %gnice   %idle
...

HH:MM:SS XM   CPU_id   %usr   %nice   %sys   %iowait   %irq   %soft   %steal   %guest   %gnice   %idle
...
```

The [`start_telemetry_run.sh`][file-link] script is verified to work with [`mpstat(1)`][mpstat-link] version 11.4.3  
(from [`sysstat`][sysstat-link] version 11.4.3).


#### `nvidia-smi`

Telemetry for the GPU is collected using the [`nvidia-smi`][smi-link] tool that is bundled with the  
nvidia driver. The [`start_telemetry_run.sh`][file-link] script is verified to work with version  
440.40 of the nvidia driver and [`nvidia-smi`][smi-link].

Four different [logfiles](./telemetry-start#generated-logfiles) are generated with different calls to [`nvidia-smi`][smi-link]:

- `gpu_long_$now.dat`:  
  This [logfile](./telemetry-start#generated-logfiles) contains utilization data for the GPU and GPU memory 
  in 500ms intervalls   
  and is generated by using the command:
  ```
  nvidia-smi \
  --query-gpu=timestamp,utilization.gpu,utilization.memory,memory.used,memory.total \
  --format=csv,noheader,nounits -lms 500 \
  -f $HOME/pyglasma3d_numba_stats/$now/gpu_long_$now.dat
  ```

- `gpu_$now.dat`:  
  This [logfile](./telemetry-start#generated-logfiles) contains data for GPU utilization in 166ms intervalls and is generated  
  with the command:
  ```
  nvidia-smi stats -d gpuUtil \
  -f $HOME/pyglasma3d_numba_stats/$now/gpu_$now.dat &
  ```

- `gpu_mem_long_$now.dat`:  
  This [logfile](./telemetry-start#generated-logfiles) contains the GPU memory used for each compute processe running  
  on the GPU in 500ms intervalls and is the output of the command:
  ```
  nvidia-smi --query-compute-apps=timestamp,process_name,used_memory \
  --format=csv,noheader,nounits -lms 500 \
  -f $HOME/pyglasma3d_numba_stats/$now/gpu_mem_long_$now.dat &
  ```

- `gpu_mem_$now.dat`:  
  This [logfile](./telemetry-start#generated-logfiles) contains GPU memory utilization in 166ms intervalls and is generated  
  with the command:
  ```
  nvidia-smi stats -d memUtil \
  -f $HOME/pyglasma3d_numba_stats/$now/gpu_mem_$now.dat &
  ```


### Generated Logfiles

The resulting file tree with all 11 logfiles is listed below. The placeholder `$now` will  
get expanded with the timestamp recorded at launch of the [`start_telemetry_run.sh`][file-link]  
script.

`$HOME/pyglasma3d_numba_stats/$now/`  
&nbsp;&nbsp;&nbsp;&nbsp; ├── [`CMD_ARGS_$now.dat`](./_index#cmd_args_xxxxxxxxxxxxxxxxxxxdat)  
&nbsp;&nbsp;&nbsp;&nbsp; ├── [`DURATION_$now.dat`](./_index#duration_xxxxxxxxxxxxxxxxxxxdat)  
&nbsp;&nbsp;&nbsp;&nbsp; ├── [`ENDTIME_$now.dat`](./_index#endtime_xxxxxxxxxxxxxxxxxxxdat)  
&nbsp;&nbsp;&nbsp;&nbsp; ├── [`gpu_$now.dat`](./_index#gpu_xxxxxxxxxxxxxxxxxxxdat)  
&nbsp;&nbsp;&nbsp;&nbsp; ├── [`gpu_long_$now.dat`](./_index#gpu_long_xxxxxxxxxxxxxxxxxxxdat)  
&nbsp;&nbsp;&nbsp;&nbsp; ├── [`gpu_mem_$now.dat`](./_index#gpu_mem_xxxxxxxxxxxxxxxxxxxdat)  
&nbsp;&nbsp;&nbsp;&nbsp; ├── [`gpu_mem_long_$now.dat`](./_index#gpu_mem_long_xxxxxxxxxxxxxxxxxxxdat)  
&nbsp;&nbsp;&nbsp;&nbsp; ├── [`SIMLOG_$now.dat`](./_index#simlog_xxxxxxxxxxxxxxxxxxxdat)  
&nbsp;&nbsp;&nbsp;&nbsp; ├── [`STARTTIME_$now.dat`](./_index#starttime_xxxxxxxxxxxxxxxxxxxdat)  
&nbsp;&nbsp;&nbsp;&nbsp; ├── [`sys_mem_$now.dat`](./_index#sys_mem_xxxxxxxxxxxxxxxxxxxdat)  
&nbsp;&nbsp;&nbsp;&nbsp; └── [`sys_util_$now.dat`](./_index#sys_util_xxxxxxxxxxxxxxxxxxxdat)  



#### `/proc/meminfo` File
The first 23 lines of a compatible [`meminfo`][meminfo-link] file. Any lines after line 23 are not  
used by the [`start_telemetry_run.py`][file-link] script.

```
MemTotal:       32881424 kB
MemFree:         7840556 kB
MemAvailable:   30484848 kB
Buffers:         3876228 kB
Cached:         15336968 kB
SwapCached:          204 kB
Active:         13376416 kB
Inactive:        7127328 kB
Active(anon):     577496 kB
Inactive(anon):   793756 kB
Active(file):   12798920 kB
Inactive(file):  6333572 kB
Unevictable:        9316 kB
Mlocked:            9316 kB
SwapTotal:      29296636 kB
SwapFree:       29279740 kB
Dirty:                64 kB
Writeback:             0 kB
AnonPages:       1295804 kB
Mapped:           515088 kB
Shmem:             77156 kB
Slab:            4189056 kB
SReclaimable:    3962132 kB
...
...
```


[//]: # (references: )  
[file-link]: #TODO:_linkt_to_this_file_in_repo
[file-link-l164]: #TODO:_linkt_to_this_file_in_repo#L164
[file-link-l75]: #TODO:_linkt_to_this_file_in_repo#L75
[plot-link]: ./telemetry-plots
[plots-link]: ./telemetry-plots#generated-plots
[folder-link]: TODO:_link_to_package_root "Link to Package Folder in Project Repository"
[sst-link]: TODO:_link_to_sim_setups
[ssmvg-link]: TODO:_link_to_sim_setup_mv
[proj-link]: TODO:_linkt_to_pygl3d_proj
[taskset-link]: http://man7.org/linux/man-pages/man1/taskset.1.html "Link to manpage for 'taskset(1)'"
[meminfo-link]: http://man7.org/linux/man-pages/man5/proc.5.html "Link to manpage for 'proc(5)'"
[mpstat-link]: http://man7.org/linux/man-pages/man1/mpstat.1.html "Link to manpage for 'mpstat(1)'"
[sysstat-link]: http://sebastien.godard.pagesperso-orange.fr/features.html "Link to homepage of 'sysstat'"
[smi-link]: https://developer.nvidia.com/nvidia-system-management-interface "Link to homepage of 'nvidia-smi'"
