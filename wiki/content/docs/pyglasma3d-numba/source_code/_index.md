---
title: Source Code
bookToC: false
weight: 11
---
<style>
/* body {overflow: hidden;} */
.markdown {height: 95%;}
.book-page {height: 100vh;}
</style>
<script type="text/javascript">
window.onload = function()
{
  var site_url = new URL(window.location.href);
  var target_url = site_url.searchParams.get("iframetarget");
  var subtarget_hash = site_url.hash;
  if (target_url == null){
    target_url = "pyglasma3d_numba_source/index.html";
  } else {
    target_url = target_url + subtarget_hash;
  }
  document.getElementById("iframe").setAttribute("src", target_url);
}
</script>
<iframe style="width:125%; height:115vh; transform:scale(0.8); transform-origin: 0 0;"
frameborder="0", id="iframe" src=""></iframe>
<!-- code source last modified: Sep 24th, 2020 -->
