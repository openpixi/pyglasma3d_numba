---
title: Python Package `test_consistency`
bookCollapseSection: true
---

# Python Package `test_consistency`

## Contents

- [Quick-Start Guide](./_index#quick-start-guide)
  - [`start.sh` Script](./_index#startsh-script)
  - [`consistency_tests.py` Script](./_index#consistency_testspy-script)
  - [`evaluation` Module](./_index#evaluation-module)
- [User Guide](./_index#user-guide)
  - [Requirements](./_index#requirements)
  - [Execution Methods](./_index#execution-methods)
  - [Data File Format](./_index#data-file-format)
  - [Preparing Comparison Data](./_index#preparing-comparison-data)
- [Understanding the Test Results](./_index#understanding-the-test-results)
  - [Unit Test Setups](./_index#unit-test-setups)
  - [Test Outputs](./_index#test-outputs)
  - [Sample Plot](./_index#sample-plot)
  - [Sample Console Log](./_index#sample-console-log)

### Additional Documentation
- [`start.sh` Script][start-link]
- [`consistency_tests.py` Script][tests-link]
- [`evaluation` Module][eval-link]

* * * *

## Quick-Start Guide

This [test_consistency][folder-link] package provides tools to easily compare data files with  
numerical data. Its main purpose is to assist with the development of [pyglasma3d_numba][root-link].  
The different ways to use this package are:


#### `start.sh` Script
The [`start.sh`][start-link] script provides a set of preconfigured test setups to choose  
from for the [`consistency_tests.py`][tests-link] script.

Use the [`start.sh`][start-link] script with the bash shell:  
```bash
$ bash start.sh
```
Or by calling it directly from the command line:
```bash
$ ./start.sh
```
Then follow the prompts on the screen.


#### `consistency_tests.py` Script

The [`consistency_tests.py`][tests-link] script provides a feature rich testing and analysis  
tool for comparing the numerical data of two files. It is built as a command line  
tool and offers customization options. Within the [test_consistency][folder-link] package  
this script is also used by the [`start.sh`][start-link] bash script.

The [`consistency_tests.py`][tests-link] script can be called directly. Use the  
`-f` / `--files` parameter to specify the two data files to compare:
```
$ ./consistency_tests.py -f path/to/file1 path/to/file2
```
```
$ python3 consistency_tests.py -f path/to/file1 path/to/file2
```
Use the `-h` / `--help` flag or see the section ['Parameters'][tests-params-link] for [`consistency_tests.py`][tests-link]  
for explanations of all available parameters.


#### `evaluation` Module

The calculations for the comparison of the data files are handled by the [`evaluation`][eval-link]  
module. It generates error values for all compared values from the data files. This  
module can be imported and used in custom code.


## User Guide

The [test_consistency][folder-link] package is located inside the root folder of the [pyglasma3d_numba][root-link]  
project and consists of the following files:
```
pyglasma3d_numba/
└─ test_consistency/
    ├─ __init__.py              [internal file]
    ├─ consistency_tests.py     [script for usage]
    ├─ evaluation.py            [custom module]
    ├─ mv_simple.py             [setup script for pyglasma3d (cython)]
    ├─ README.md                [readme]
    ├─ start.sh                 [script for usage]
    ├─ TestRunner.py            [custom module]
    └─ original/                [folder for data files]
       └─ ...                  
```


### Requirements

The requirements for this package are covered by the requirements files located in the  
project root:
```
pyglasma3d_numba/
 ├─ ...
 ├─ pip-requirements.txt
 ├─ conda-environment.yml
 └─ ...
```
See the section ['Dependencies'][deps-link] for more information on how to use these files.


### Execution Methods

Two scripts:
- [`start.sh`][start-link]
- [`consistency_tests.py`][tests-link]

are provided with the [test_consistency][folder-link] package. They fit different usecases:

The [`start.sh`][start-link] script provides a set of preconfigured test setups for the  
[`consistency_tests.py`][tests-link] script to choose from. First the selected [simulation  
setup][sst-link] will be run and then the output will be compared to a [preconfigured file][start-files-link].  
This method is designed to assist with the development of the [pyglasma3d_numba][root-link]  
project. It allows quick testing for bugs that do not break the code, but produce  
wrong results when being compared to the [cython version][cython-link] of the pyglasma3d  
codebase. For detailed execution instructions please see the ['Execution'][start-exec-link] section  
for [`start.sh`][start-link].

The [`consistency_tests.py`][tests-link] script provides a feature rich testing and analysis  
tool for comparing the numerical data of two files. In contrast to [`start.sh`][start-link],  
[`consistency_tests.py`][tests-link] provides a number of [parameters][tests-params-link] to configure a custom  
test setup. For detailed execution instructions please see the ['Execution'][tests-exec-link] section for  
[`consistency_tests.py`][tests-link].

Both scripts produce the same kind of output (log to console and graphs). Please  
refer to the ['Understanding the Test Results'](./_index#understanding-the-test-results) section for explanations on the  
meanings of the output.


### Data File Format

The data files with the numerical data to compare have to follow a strict format  
which is required for the [`evaluation`][eval-link] module to work. They should be text  
files. On the first two lines there has to be one integer each. The product of those  
two integers plus 2 (val1*val2 + 2) has to match the number of lines the file has.  
On each subsequent line have to be a fixed number of whitespace separated values.  
For more detail please see the ['Parameters'][eval-iter-params-link] section for the [`evaluation`][eval-link] module.

The data files produced by the [pyglasma3d_numba][root-link] simulation code meet these  
criteria after being converted to text files using the [`data_io` module][dio-link].
Some of the  
provided [simulation setups][sst-link] convert their binary output to text files as a last step.  

An example for a compatible data file is:
```
3
2
8.118377781300748965e-34 1.516547440201633402e-32 2.539145522631136880e-31 1.630555665954511669e-32 -2.783912529521835096e-32
7.006971246215732269e-34 1.674598777474349129e-32 2.740350899963080185e-31 1.139540826052872642e-32 -6.276361421468398732e-33
6.946098194118973359e-34 1.697061908076236292e-32 2.757402828885618003e-31 1.059846084545592635e-32 -1.045003201325233200e-33
6.946102784942463474e-34 1.697245103263265276e-32 2.758471643340202761e-31 1.059046310965086305e-32 -3.131123374594051781e-34
6.946102784942463474e-34 1.697245103263265276e-32 2.758471643340202761e-31 1.059046310965086305e-32 -3.043964740698971201e-34
6.946102784942463474e-34 1.697245103263265276e-32 2.758471643340202761e-31 1.059046310965086305e-32 -3.043964740698971201e-34
```

### Preparing Comparison Data

The [test_consistency][folder-link] package was designed to compare data files generated by  
the original [cython version][cython-link] of 'pyglasma3d' with the data files generated by   
[pyglasma3d_numba][root-link]. The following steps provide a guide for how to get comparison  
data for the [`mv_simple.py` setup][ssmvs-link] from the [cython version][cython-link]. The names and locations  
of the files are chosen correctly, so that they are compatible with the requirements  
of the [`start.sh`][start-link] script.

The [cython version][cython-link] requires modules as dependencies that may conflict with the  
[dependencies][deps-link] of [pyglasma3d_numba][root-link]. Please consider creating an extra virtual  
environment for using the [cython version][cython-link] with the following modules:
- Python 2.7.9
- NumPy 1.11.0
- Cython 0.24.1
- gcc 5.4.0 or equivalent C compiler

Note that the [cython version][cython-link] requires Python version 2.7.  
Compatibility with the newest versions of the modules listed above was tested,  
but no warranty or support will be provided when using newer versions. For up  
to date version numbers please visit the [home page of the cython version][cython-link].

**Steps:**

1. Clone the git repository of the [cython version][cython-link] found on gitlab.com:  
   Via ssh:
   ```
   $ git clone git@gitlab.com:monolithu/pyglasma3d.git
   ```
   Or via https:
   ```
   $ git clone https://gitlab.com/monolithu/pyglasma3d.git
   ```
   Enter your credentials if prompted.

2. Change into the directory `pyglasma3d/`:  
    ```bash
    $ cd pyglasma3d
    ```

3. Compile the custom simulation modules with:
   ```bash
   $ python2 setup.py build_ext --inplace
   ```
   Note that this step requires Python 2.7.X.  
   This step is a manual workaround for compiling the modules. Using the  
   `setup.sh` script instead, will use whatever Python version is linked to the  
   `python` command in the current environment, which might not be 2.7.X.

4. On successful compilation the simulation can be run. Copy the `mv_simple.py`  
   setup from the root folder of the [test_consistency][folder-link] package into the `examples/`  
   folder of the [cython version][cython-link]:  
   (substitute '<path/to>' with the correct path)
   ```bash
   $ cp <path/to>/pyglasma3d_numba/test_consistency/mv_simple.py pyglasma3d/examples/
   ```


5. Run the simulation:
   ```bash
   $ python2 -m pyglasma3d.examples.mv_simple
   ```

   After the simulation run completed a new folder `pyglasma3d/output/` will appear  
   that contains the binary file:
    - `T_mv_trial_simple.dat`  
    
   ```
   pyglasma3d/
    ├─ ...
    ├─ output/
    │   └─ T_mv_trial_simple.dat
    ├─ ...
   ```

6. This file needs to be converted to a text file. First copy the file into the `original/`  
   folder inside [test_consistency][folder-link] package:  
   (substitute '<path/to>' with the correct path)
   ```bash
   $ cp pyglasma3d/output/T_mv_trial_simple.dat <path/to>/pyglasma3d_numba/test_consistency/original/
   ```

7. Change into the root directory of the [pyglasma3d_numba][root-link] project and start an  
   interactive Python interpreter:  
   (substitute '<path/to>' with the correct path)
   ```
   $ cd <path/to>/pyglasma3d_numba
   $ python
   ```  
   Then import this module from `pyglasma3d_numba_source/`
   ```python
   import pyglasma3d_numba_source.data_io as io
   ```
   It provides helper functions to easily convert the file.

   Then execute this line of code to convert the binary file into a text file:
   ```python
   io.write("./test_consistency/original/T_mv_trial_simple_cython_textfile.dat", **io.read("./test_consistency/original/T_mv_trial_simple.dat"))
   ```
   This will create a text file in the `pyglasma3d_numba/test_consistency/original`  
   folder:
   - `T_mv_trial_simple_cython_textfile.dat`  
    
   ```
   pyglasma3d_numba/
   └─ test_consistency/
       ├─ ...
       ├─ original/
       │  ├─ T_mv_trial_simple.dat
       │  └─ T_mv_trial_simple_cython_textfile.dat
       ├─ ...
   ```
   The [`start.sh`][start-link] script is configured to source this file as the first file containing  
   the reference data when it calls the [`consistency_tests.py`][tests-link] script internally.


## Understanding the Test Results

In the following the term 'tests' will not only include the [unit tests][tests-unit-link] conducted  
by the [`consistency_tests.py`][tests-link] script, but also every evaluation done on the  
comparison data itself.  
The 'number of matching digits' when comparing two numbers will be counted  
starting from the coefficient of the highest power of 10 until the first mismatched  
coefficient (please see the [`diff_statistics()`][eval-ds-link] section for the [`evaluation`][eval-link]  
module) when viewing the number in standard base 10 representation.


### Unit Test Setups

These [unit tests][tests-unit-link] are only meant to be a fast way to check for major errors. A <span style="color:green">'pass'</span>  
for these tests does not equate to a 'perfect match' of the data, nor does it give any  
information about the quality of the match. Furthermore, a truly perfect matching  
set of data will not pass the unit tests, as in this case the evaluation does not produce  
any results. Assessments should only be based on the plots and logs discussed in the  
[next section](./_index#test-outputs).

These [unit tests][tests-unit-link] probe the minimum and average number of matching digits against  
the chosen value. The tests will only be performed on one 'chunk' of the comparison  
data. For [pyglasma3d_numba][root-link] a 'chunk' corresponds to one simulation step. The  
amount of data included in one 'chunk' (or simulation step) is given by the integer  
value on the first line of each of the original data files. The default simulation step  
is configured to be the last step. A custom step can be set with the [`-s` parameter][tests-params-link].

The value for the limit of the matching digits should be chosen very carefully.  
It has to be matched with the precision of the numerical data types used and  
adjusted according to any mathematically imprecise operations used in the  
code. The defaults set are 15 digits for standard 'double precision' and 10  
digits with the [`--fastmath` flag][tests-params-link] set, to account for accuracy loss based on  
faster evaluation of standard mathematical functions (eg. sin(), log()). The  
limit can be set to a custom value with the [`-p` parameter][tests-params-link].

For reference, the 'IEEE-754' double precision standard defines 15 - 17 digits  
as the number of significant digits for standard mathematical operations.  
The 'IEEE-754' standard is the most used for double precision (see  
https://en.wikipedia.org/wiki/Double-precision_floating-point_format).  

In the special case of Python, the precision used on the target machine is  
saved in the named tuple [`sys.float_info`](https://docs.python.org/3/library/sys.html?highlight=float_info#sys.float_info) and typically will be 15. Officially,  
Python adheres to the ['C99' standard](http://www.open-std.org/jtc1/sc22/wg14/www/docs/n1256.pdf), where at §5.2.4.2.2 p.25f the 'DBL_DIG'  
value is given as 10. However, on p.27f example 2 describes a floating point  
representation that is also compliant with the 'IEEE-754' double precision standard  
and requires 'DBL_DIG' to be 15.  
It is also important to check any modules using a C backend. Numpy, for example,  
provides a precision value of 15 digits for its `numpy.float64` (the default) floating  
point type (as can be checked with `numpy.finfo(np.float64).precision`).  
So it matches the standard Python `float` type.  

Another special case to consider for the [pyglasma3d_numba][root-link] project is the  
precision used by [Numba](http://numba.pydata.org/). There is no specific value listed in the [Numba  
documentation](http://numba.pydata.org/numba-doc/latest/reference/fpsemantics.html), but insted compliance with both, the 'IEEE-754' and 'C99'  
standards is assured. These standards get 'relaxed' when using the [`fastmath`](https://numba.pydata.org/numba-doc/dev/user/performance-tips.html#fastmath)  
option during just-in-time compilation.

Based on this information, the default values for the [unit tests][tests-unit-link] are set to  
15 digits for standard calculations and 10 digits for `fastmath` enabled  
calculations.


### Test Outputs

At the end of this page are examples for the test outputs consisting of plots and data  
logged to the console. They are separated based on output that is only produced  
when using [`start.sh`][start-link] and output that is produced for both [execution methods](./_index#execution-methods)  
when the [`-v flag`][tests-params-link] is set. If the chosen option for [`start.sh`][start-link] runs a [simulation setup][sst-link]  
before the [`consistency_tests.py`][tests-link] script, the test output will be at the end of the  
full console log.


**[`start.sh`][start-link] Script**

The only output unique to the [`start.sh`][start-link] script are three timing values:
- 'real'
- 'user'
- 'sys'  

which are the output of the bash built-in command `time`. They show the elapsed  
real time ('real'), user cpu time ('user') and system cpu time ('sys') for  
the chosen simulation setup, including the amount of time it took the Python  
interpreter to initialize.  
These values provide an approximation of the total runtime, and thus cannot  
be treated as 'profiling data'. However, averaging multiple runs and comparing  
different setups is a valid procedure.


**[`consistency_tests.py`][tests-link] Script**

The output of the [`consistency_tests.py`][tests-link] script starts with a line of '#' which is  
followed by a log of the data files compared. If the [`-v` flag][tests-params-link] is set (like in the example),  
the next part of the output until the next line of '#' will show the numerical values that  
will be used for the plots.  

The first table lists the mean, min and std values (the minus sign can be ignored) for  
the number of matching digits for each 'chunk' (simulation step). The averaging process  
does not include the amount of perfect matches (see the section on the  
[`diff_statistics()` function][eval-ds-link]).

After that the bin edges and frequency data for the historgam are logged.

The last part of the output will show the 'Unit Test Report'. It will log the success  
or failure of the configured [unit tests][tests-unit-link], with some information on the cause for the  
latter case. The 'Unit Test Report' is always present, except when [`--skiptests`][tests-params-link] is set.


**Plots**

The plots are the most important results of the evaluation and will show how well  
the compared data matches together. By default the generated image will contain  
three graphs:
- Matching Digits for each Simulation Step
- Histogram of Matching Digits
- Matching Digits vs Data Value Magnitudes  

For all available customization options please see the ['Structure'][tests-unit-link] section for  
[`consistency_tests.py`][tests-link].

The first plot is the visualization of the table of data mentioned before. For each  
'chunk' (simulation step) the mean and minimum number of matching digits are  
plotted as blue and yellow lines. The blue shaded are in between these lines marks  
the standard deviation. The value for the chosen precision ('limit') is denoted by a  
dashed magenta line. Ideally this plot should show the limit line below all other  
values for all simulation steps.  
In the [provided example](./_index#sample-plot) are large deviations for the first part of the simulation  
run. Those differences vanish towards the end of the run, where the target limit  
is reached by the mean number of matching digits, but not by the minimun number.  
This result is confirmed by the [sample unit tests](./_index#sample-console-log), which passed for the mean 
value  
but not for the minimum value.

For further judgement of the error, the other plots should be used. The histogram shows  
the frequency distribution of the number of matching digits. It shows that the majority  
of the values does lie above the limit (dashed magenta line). However the distribution is  
skewed towards large errors.

Lastly, when examining the third plot, it becomes clear, that the compared data deviates  
for small magnitudes of the original data. Large magnitudes are near the limit. Even for  
small magnitudes parts of the data are around the limit, while others do not match at all.  
Again, it is visible that the values reach the target line for later simulation steps. Data  
points for perfect matches are not included in this scatterplot.

Taking all those aspects into account, it is possible to assess the the results. Although  
the first plot would suggest a rather bad match, the other plots do show, that the  
majority of the difference values meet the set limit. That means, that the first 20  
simulation steps are a mix of good and bad matches, the latter of which disappear  
for later simulation steps. For further evaluation the background of the original data  
(the simulation) is required.  

This example should provide a guide for how to interpret the results of the 'consistency  
tests'. The last decision on wether or not the compared data can be considered 'equal'  
will always depend on the particular example and its background. A rather good match  
could be not good enough for one case, whereas a rather bad match could still be  
sufficient for another case.


### Sample Plot
![sample plot](sample-plot.png)


### Sample Console Log

[`start.sh`][start-link]
```
...
...
10.5 Complete cycle. 0.013
10.5 Writing energy-momentum tensor to file. 0.071


converting binary output file to text file...

real    0m13.761s
user    0m13.978s
sys     0m0.543s

calling consistency_test.py with the following arguments:
...
...
```

* * * *

[`consistency_tests.py`][tests-link]
```
$ ./consistency_tests.py -f original/source1.dat ../output/source2.dat -v -p 12


 ########################################################################################################### 

statistics on the differences of the input data for files:

source1.dat      and      source2.dat


iter step   mean                             min                              std
01          -5.23533736287300577e+00          0.00000000000000000e+00          5.38257750896663012e+00
02          -6.20544027466667725e+00          0.00000000000000000e+00          5.22958497439693648e+00
03          -7.05313890002745225e+00          0.00000000000000000e+00          5.10254075417518926e+00
04          -7.75058442026928773e+00          0.00000000000000000e+00          5.04575536257561286e+00
05          -8.39803490540145958e+00          0.00000000000000000e+00          4.89198453040639514e+00
06          -9.08641484784138775e+00          0.00000000000000000e+00          4.72217861796043614e+00
07          -9.36215848561086972e+00          0.00000000000000000e+00          4.51196371698584908e+00
08          -9.57132891892933735e+00          0.00000000000000000e+00          4.33717129103758214e+00
09          -9.73064036376788088e+00          0.00000000000000000e+00          4.09628771185579676e+00
10          -9.93795399932990620e+00          0.00000000000000000e+00          3.84711062385458202e+00
11          -1.01105726661008681e+01          0.00000000000000000e+00          3.53883562580500266e+00
12          -1.02706189829114471e+01          0.00000000000000000e+00          3.27712178423152301e+00
13          -1.04999074232313578e+01          0.00000000000000000e+00          3.01772488010317241e+00
14          -1.07669860938315143e+01          -6.12681992293747202e-01          2.68355263456251381e+00
15          -1.10298796391376435e+01          -1.11286315047552598e+00          2.37443698414202942e+00
16          -1.12727014547276045e+01          -2.48122731928997142e+00          2.11808781562478377e+00
17          -1.14736681119995492e+01          -4.15482944733859050e+00          1.81044372056548619e+00
18          -1.16752755757275377e+01          -4.87551750225039893e+00          1.63155847106522733e+00
19          -1.18226822408683336e+01          -5.61103352734079852e+00          1.46067337091114524e+00
20          -1.19295180709738826e+01          -6.84528934099750952e+00          1.34168643798515586e+00
21          -1.20235319104534888e+01          -7.64542834576540997e+00          1.18752009461779018e+00
22          -1.21364655488793183e+01          -8.65365849694569178e+00          1.08259752419031452e+00
23          -1.22422418976673804e+01          -8.87925789415600164e+00          1.05050074465212662e+00
24          -1.22829347792917716e+01          -9.35842316833423205e+00          9.49068307968019331e-01
25          -1.23497993578520386e+01          -9.50464827332545781e+00          8.92204630619525818e-01
26          -1.23941435722743343e+01          -9.63745595494490459e+00          8.41145804464971558e-01
27          -1.24499966097483448e+01          -9.62313988672886644e+00          7.62828273044895422e-01
28          -1.24953066830543662e+01          -1.00729861541494419e+01          7.00460595180073531e-01
29          -1.25243849290176854e+01          -1.06877620556009525e+01          6.47574830956791025e-01
30          -1.25519400465270383e+01          -1.06924910943754110e+01          6.42957247318995839e-01
31          -1.25534575365301357e+01          -1.08267449826513271e+01          6.11629847655739467e-01
32          -1.25383614315753675e+01          -1.07412380596436350e+01          6.07077851429742021e-01
33          -1.25496530597545757e+01          -1.06624938536782494e+01          6.02596603718830504e-01
34          -1.25664435484445072e+01          -1.00423012722170295e+01          6.08744071672543630e-01
35          -1.25373970993243464e+01          -1.04187791050813896e+01          6.05506140872575482e-01
36          -1.24925757140182281e+01          -1.06791074530514205e+01          5.82919119554282772e-01
37          -1.24824554379645019e+01          -1.07447293173062199e+01          6.01353267224167709e-01
38          -1.24637180117153719e+01          -1.06851771660098898e+01          5.86495240865939849e-01
39          -1.24760145090732273e+01          -1.03646794204214867e+01          6.21298836886916228e-01
40          -1.24504166278713040e+01          -1.04496837268954916e+01          5.72039268820039859e-01
41          -1.24273343886279335e+01          -1.05929541430278515e+01          5.68104518137865777e-01
42          -1.24380266194687295e+01          -1.02131950079146812e+01          5.91415428782090635e-01

historgram data:
bin edges: 0e+00    1e-18    1e-17    1e-16    1e-15    1e-14    1e-13    1e-12    1e-11    1e-10    1e-09    1e-08    1e-07    1e-06    1e-05    1e-04    1e-03    1e-02    1e-01    1e+00    1e+01    
frequency:      747      0        0        146      664      3603     10859    3979     2256     1520     398      174      145      156      114      117      178      469      736      199

 ########################################################################################################### 
```
```
              Unit Test Report
Status: 
    Pass: 1
    Failure: 1

Description:
Summary: 
    +------------------------------------------------------------------------------------+-------+------+------+-------+
    |                                Test group/Test case                                | Count | Pass | Fail | Error |
    +------------------------------------------------------------------------------------+-------+------+------+-------+
    | TestDataConsistency: Unittests on difference values of data for an iteration step. | 2     | 1    | 1    | 0     |
    | Total                                                                              | 2     | 1    | 1    | 0     |
    +------------------------------------------------------------------------------------+-------+------+------+-------+
    TestDataConsistency
        +------------------------------------------------------------+--------------------------------------------------------------------------------+------------+
        |                         Test name                          |                                     Stack                                      |   Status   |
        +------------------------------------------------------------+--------------------------------------------------------------------------------+------------+
        | test_mean_accuracy: Check if mean is above digits limit.   |                                                                                | pass       |
        | test_min_accuracy: Check if minimum is above digits limit. | Traceback (most recent call last):                                             | fail       |
        |                                                            |   File "test_consistency/consistency_tests.py", line 419, in test_min_accuracy |            |
        |                                                            |     self.assertTrue(                                                           |            |
        |                                                            | AssertionError: False is not true : min above 12 digits                        |            |
        +------------------------------------------------------------+--------------------------------------------------------------------------------+------------+
```


[//]: # (references: )  
[folder-link]: TODO:_link_to_package_root "Link to Package Folder in Project Repository"
[root-link]: TODO:_link_to_project_root "Link to Project Repository"
[start-link]: ./test-consistency-start
[start-exec-link]: ./test-consistency-start#execution
[start-files-link]: ./test-consistency-start#comparison-data-and-default-arguments
[tests-link]: ./test-consistency-tests
[tests-params-link]: ./test-consistency-tests#parameters
[tests-exec-link]: ./test-consistency-tests#execution
[tests-unit-link]: ./test-consistency-tests#structure
[eval-link]: ./test-consistency-evaluation
[eval-iter-params-link]: ./test-consistency-evaluation#read-iter-params
[eval-ds-link]: ./test-consistency-evaluation#function-diff_statistics
[sst-link]: TODO:_link_to_sim_setups
[ssmvs-link]: TODO:_link_to_sim_setup_mv_simple
[matplotlib-link]: https://pypi.org/project/matplotlib/
[numpy-link]: https://pypi.org/project/numpy/
[cython-link]: https://gitlab.com/monolithu/pyglasma3d
[dio-link]: #TODO:_link_to_data_io_module
[deps-link]: #TODO:_link_to_project_dependencies
[unitt-link]: https://docs.python.org/3/library/unittest.html?highlight=unittest#module-unittest "Link to Python Docs"
[deps-link]: #TODO:_link_to_root_dependency_section
