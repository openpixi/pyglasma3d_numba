---
title: Python Script `consistency_tests.py`
---

# Python Script `consistency_tests.py


## Contents

- [Overview](./test-consistency-tests#overview)
- [User Guide](./test-consistency-tests#user-guide)
  - [Structure](./test-consistency-tests#structure)
  - [Execution](./test-consistency-tests#execution)
  - [Data File Format](./test-consistency-tests#data-file-format)
  - [Parameters](./test-consistency-tests#parameters)
  - [Output](./test-consistency-tests#output)
- [Example](./test-consistency-tests#example)


## Overview

The [`consistency_tests.py`][file-link] script provides a feature rich testing and analysis  
tool for comparing the numerical data of two files. It is built as a command line  
tool and offers customization options. Within the ['test_consistency'][home-link] package  
this script is also used by the [`start.sh`][start-link] bash script.

The [`consistency_tests.py`][file-link] script can either be used indirectly via the  
[`start.sh`][start-link] script:
```
$ ./start.sh
```
Or it can be called directly. Use the `-f` / `--files` parameter to specify the two data  
files to compare:
```
$ ./consistency_tests.py -f path/to/file1 path/to/file2
```
```
$ python3 consistency_tests.py -f path/to/file1 path/to/file2
```
Use the `-h` / `--help` flag or see the section ['Parameters'](./test-consistency-tests#parameters) for explanations of  
all available parameters.


## User Guide

This script is designed as an executable script. As such it is not importable as  
a module. When trying to import this script the [`ImportError`][imperr-link] exception will  
be raised with the following error message:
```
WARNING!
This `test_consistency.py` script cannot be imported or called as a secondary module.
```


### Structure
This script accomplishes three tasks:  

#### 1. Comparing the Data

The function [`diff_statistics()`][diff-link] from the [`evaluation`][eval-link] module is  
used to calculate the differences of the numerical data supplied.

#### 2. Statistical Evaluation of the comparison Data

The function [`diff_statistics()`][diff-link] additionally provides statistical data  
on the difference values, which are then presented to the user in an easily  
interpretable form. Graphs and numerical values can be controlled by  
[command line parameters](./test-consistency-tests#parameters). For help on how to interpret the results please  
see the section ['Understanding the Test Results'][res-link]. A sample output is provided  
in the ['Output'](./test-consistency-tests#output) section below.

By default, three graphs are drawn:
1. **'Matching Digits for each Simulation Step'**  
   Showing mean+&#963; and min values.
2. **'Histogram of Matching Digits'**  
   Showing frequency values accumulated over all simulation steps.
3. **'Matching Digits vs. Data Value Magnitudes'**  
   Showing values color coded for all simulation steps.

The third plot will load a data point for each data value present in the  
data files, on the basis of the return data `scatter` from the [`diff_statistics()`][diff-link]  
function. For large data files it might not be possible to load all data values  
into memory. Hence, the flag `--hide` is provided which disables the third plot  
and also passes the `skip_scatter` parameter to [`diff_statistics()`][diff-link].  

The standard behavior is to show a pop-up window with the graphs. The script  
pauses as long as this window is open. For available manipulation options  
please see the [matplotlib documentation][mpl-link].  
To change this the option `-g` / `--graphs` can be set to a valid file path where  
the graphs will be saved automatically and the pop-up window will be suppressed.  
This option can also be set to `False`, which will disable all plotting functionality.

If the flag `-v` / `--verbose` is set, the data for the `results` and `(hist, bins)`  
return values of [`diff_statistics()`][diff-link] are printed to the terminal. This  
output can additionally be written to a file by setting the `-o` / `--output`  
option to a valid file path.


#### 3. Unit Tests on statistical Evaluation Data

A [unit test][unitt-link] suite is set up to give quick feedback about the comparison.  
The unittests are only performed for a single simulation step on the `results`  
return value of [`diff_statistics()`][diff-link]. As default the last step is chosen.  
This can be changed by setting the `-s` / `--step` option to the number of the  
desired simulation step (negative values will count backwards from the last step).

`results` contains information about the average and minimal number of  
matching digits of the data compared. The unittests compare these numbers  
with predefined values and fail if the numbers for the matching digits are too low.

In particular the default test case probes for 15 matching digits. If the flag  
`--fastmath` is set, this limit is reduced to 10 digits. It is also possible to set a  
custom limit with the `-p` / `--precision` option. However, either `--fastmath`  
or `-p` / `--precision` are allowed, not both. For more background on the  
number of matching digits please see the section ['Understanding the Test Results'][res-link].

The results of the unittests are printed to the console. All tests can be skipped  
by setting the flag `--skiptests`.


### Execution

#### Indirectly via `start.sh`

```
$ ./start.sh
```

The [`start.sh`][start-link] script uses this script with the following options:  
```
-f test_consistency/original/T_mv_trial_simple_cython_textfile.dat output/T_mv_trial_simple_textfile.dat -v [--fastmath]
```
- `-f`:  
  The first file is located in the subfolder `original/`.  
  The second path points to a file that is created by the simulation setup [mv_simple.py][mvs-link].
- `-v`:  
  The verbose flag is set to print output to the console.
- `--fastmath`:  
  Depending on the test setup chosen when running [`start.sh`][start-link], the `--fastmath` flag  
  is either added or not.

For help about the [`start.sh`][start-link] script please visit the section 
['Bash Script `start.sh`'][start-link].

#### Direct Usage

This script can be called like any other Python script, as long as it is the  
main module. For the first option, the specified Python interpreter  
will execute the script.
```
$ python consistency_tests.py -f path/to/file1 path/to/file2
```
It is also possible to execute the script directly, without specifying an interpreter:
```
$ ./consistency_tests.py -f path/to/file1 path/to/file2
```
In this case the interpreter set for the current environment will be used,  
as returned by this command:
```
/usr/bin/env python
```

### Data File Format

The paths to the data files to compare specified by the required `-f / --files`  
flag need to be valid paths to those files. The paths can either be absolute, or  
relative. For the latter case, each path has to be reachable from the current  
working directory.

**File Format**

The supplied data files have to obey a certain format as required by the  
[`evaluation`][eval-params-link] module. First of all, they must be text files. On the first two  
lines there has to be one integer each. The product of those two integers  
plus 2 (val1*val2 + 2) has to match the number of total lines in the file. On each  
subsequent line have to be the same number of  whitespace separated values that  
are compatible with the [float64 format of Numpy][npf-link].

The data files produced by the [pyglasma3d_numba][root-link] simulation code meet these  
criteria after being converted to text files using the [`data_io` module][dio-link].
Some of the  
provided [simulation setups][simset-link] convert their binary output to a text file as a last step.  

For more information on the format requirements please see the sections about  
the [`evaluation`][eval-link] module or [`data_io`][dio-link] module.


### Parameters

For all available parameters use `-h`/ `--help`:

```
$ ./consistency_tests.py -h
usage: consistency_tests.py [-h] -f path/to/file path/to/file [-v] [--hide] [-o path/to/file]
                            [-g path/to/file or True/False] [--skiptests] [-s step]
                            [-p {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18} | --fastmath]

A python script to plot and test difference data.

This python script is meant to be executed from the command line, either
directly `./consistency_tests.py` or via `python consistency_tests.py`.
For available command line options use the '-h' or '--help' flags.

The command line argument '-f' / '--files' is required and takes two
paths to the data files to analyze as string arguments.

The differences are calculated using the `evaluation.py` module.

optional arguments:
  -h, --help            show this help message and exit.
                         
  -f path/to/file path/to/file, --files path/to/file path/to/file
                        specify two files to compare
                        this argument is required
                         
  -v, --verbose         print evaluation results to terminal
                        default: not set
                         
  --hide                set this flag to skip the third plot
                        this is useful if there is not enough system memory to save all the data to compare
                        default: not set
                         
  -o path/to/file, --output path/to/file
                        specify a file to write evaluation data to
                        default: None
                         
  -g path/to/file or True/False, --graphs path/to/file or True/False
                        prepare 3 (or 2 with option --hide) plots of data
                        if set to True, plots are shown interactively
                        if set to a string representing a path, plots are saved to the given path
                        default: True
                         
  --skiptests           skip all unittests
                        default: not set
                         
  -s step, --step step  specify the iteration step on which to perform the unittests
                        negative values count backwards
                        default: -1, the last iteration step
                         
  -p {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18}, --precision {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18}
                        set exponent "p" used for calculating precision for unittests: 1e-"p"
                        this option conflicts with --fastmath
                        default: 15
                         
  --fastmath            change accuracy in unittests to 1e-10
                        this option conflicts with -p / --precision
                        default: not set
```

### Output

The [example](./test-consistency-tests#sample-console-log) below shows how a typical output looks like. After a line  
of '#' the names of the two files to compare are repeated. If the `-v` /  
`--verbose` flag is set (like in the example), the next part of the output  
until the next line of '#' will show the values of the `results` and  
`(hist, bins)` return values of [`diff_statistics()`][diff-link].  

At this point of the execution, either a pop-up window with the [plots](./test-consistency-tests#sample-plot)  
will show up or the script will continue, depending on how the `-g` / `--graphs`  
parameter was used. If a window with the plots pops up, the script will pause,  
until that window is closed. For available manipulation options please see the  
[matplotlib documentation][mpl-link].

The last part of the output will be the 'Unit Test Report'. It will log the success  
or failure of the configured [unit tests](./test-consistency-tests#3-unittests-on-statistical-evaluation-data).

For help on how to interpret these results please see the section  
['Understanding the Test Results'][res-link].

## Example

### Sample Plot
![sample plot](../sample-plot.png)


### Sample Console Log
```
$ ./consistency_tests.py -f original/source1.dat ../output/source2.dat -v -p 12


 ########################################################################################################### 

statistics on the differences of the input data for files:

source1.dat      and      source2.dat


iter step   mean                             min                              std
01          -5.23533736287300577e+00          0.00000000000000000e+00          5.38257750896663012e+00
02          -6.20544027466667725e+00          0.00000000000000000e+00          5.22958497439693648e+00
03          -7.05313890002745225e+00          0.00000000000000000e+00          5.10254075417518926e+00
04          -7.75058442026928773e+00          0.00000000000000000e+00          5.04575536257561286e+00
05          -8.39803490540145958e+00          0.00000000000000000e+00          4.89198453040639514e+00
06          -9.08641484784138775e+00          0.00000000000000000e+00          4.72217861796043614e+00
07          -9.36215848561086972e+00          0.00000000000000000e+00          4.51196371698584908e+00
08          -9.57132891892933735e+00          0.00000000000000000e+00          4.33717129103758214e+00
09          -9.73064036376788088e+00          0.00000000000000000e+00          4.09628771185579676e+00
10          -9.93795399932990620e+00          0.00000000000000000e+00          3.84711062385458202e+00
11          -1.01105726661008681e+01          0.00000000000000000e+00          3.53883562580500266e+00
12          -1.02706189829114471e+01          0.00000000000000000e+00          3.27712178423152301e+00
13          -1.04999074232313578e+01          0.00000000000000000e+00          3.01772488010317241e+00
14          -1.07669860938315143e+01          -6.12681992293747202e-01          2.68355263456251381e+00
15          -1.10298796391376435e+01          -1.11286315047552598e+00          2.37443698414202942e+00
16          -1.12727014547276045e+01          -2.48122731928997142e+00          2.11808781562478377e+00
17          -1.14736681119995492e+01          -4.15482944733859050e+00          1.81044372056548619e+00
18          -1.16752755757275377e+01          -4.87551750225039893e+00          1.63155847106522733e+00
19          -1.18226822408683336e+01          -5.61103352734079852e+00          1.46067337091114524e+00
20          -1.19295180709738826e+01          -6.84528934099750952e+00          1.34168643798515586e+00
21          -1.20235319104534888e+01          -7.64542834576540997e+00          1.18752009461779018e+00
22          -1.21364655488793183e+01          -8.65365849694569178e+00          1.08259752419031452e+00
23          -1.22422418976673804e+01          -8.87925789415600164e+00          1.05050074465212662e+00
24          -1.22829347792917716e+01          -9.35842316833423205e+00          9.49068307968019331e-01
25          -1.23497993578520386e+01          -9.50464827332545781e+00          8.92204630619525818e-01
26          -1.23941435722743343e+01          -9.63745595494490459e+00          8.41145804464971558e-01
27          -1.24499966097483448e+01          -9.62313988672886644e+00          7.62828273044895422e-01
28          -1.24953066830543662e+01          -1.00729861541494419e+01          7.00460595180073531e-01
29          -1.25243849290176854e+01          -1.06877620556009525e+01          6.47574830956791025e-01
30          -1.25519400465270383e+01          -1.06924910943754110e+01          6.42957247318995839e-01
31          -1.25534575365301357e+01          -1.08267449826513271e+01          6.11629847655739467e-01
32          -1.25383614315753675e+01          -1.07412380596436350e+01          6.07077851429742021e-01
33          -1.25496530597545757e+01          -1.06624938536782494e+01          6.02596603718830504e-01
34          -1.25664435484445072e+01          -1.00423012722170295e+01          6.08744071672543630e-01
35          -1.25373970993243464e+01          -1.04187791050813896e+01          6.05506140872575482e-01
36          -1.24925757140182281e+01          -1.06791074530514205e+01          5.82919119554282772e-01
37          -1.24824554379645019e+01          -1.07447293173062199e+01          6.01353267224167709e-01
38          -1.24637180117153719e+01          -1.06851771660098898e+01          5.86495240865939849e-01
39          -1.24760145090732273e+01          -1.03646794204214867e+01          6.21298836886916228e-01
40          -1.24504166278713040e+01          -1.04496837268954916e+01          5.72039268820039859e-01
41          -1.24273343886279335e+01          -1.05929541430278515e+01          5.68104518137865777e-01
42          -1.24380266194687295e+01          -1.02131950079146812e+01          5.91415428782090635e-01

historgram data:
bin edges: 0e+00    1e-18    1e-17    1e-16    1e-15    1e-14    1e-13    1e-12    1e-11    1e-10    1e-09    1e-08    1e-07    1e-06    1e-05    1e-04    1e-03    1e-02    1e-01    1e+00    1e+01    
frequency:      747      0        0        146      664      3603     10859    3979     2256     1520     398      174      145      156      114      117      178      469      736      199

 ########################################################################################################### 
```
```
              Unit Test Report
Status: 
    Pass: 1
    Failure: 1

Description:
Summary: 
    +------------------------------------------------------------------------------------+-------+------+------+-------+
    |                                Test group/Test case                                | Count | Pass | Fail | Error |
    +------------------------------------------------------------------------------------+-------+------+------+-------+
    | TestDataConsistency: Unittests on difference values of data for an iteration step. | 2     | 1    | 1    | 0     |
    | Total                                                                              | 2     | 1    | 1    | 0     |
    +------------------------------------------------------------------------------------+-------+------+------+-------+
    TestDataConsistency
        +------------------------------------------------------------+--------------------------------------------------------------------------------+------------+
        |                         Test name                          |                                     Stack                                      |   Status   |
        +------------------------------------------------------------+--------------------------------------------------------------------------------+------------+
        | test_mean_accuracy: Check if mean is above digits limit.   |                                                                                | pass       |
        | test_min_accuracy: Check if minimum is above digits limit. | Traceback (most recent call last):                                             | fail       |
        |                                                            |   File "test_consistency/consistency_tests.py", line 419, in test_min_accuracy |            |
        |                                                            |     self.assertTrue(                                                           |            |
        |                                                            | AssertionError: False is not true : min above 12 digits                        |            |
        +------------------------------------------------------------+--------------------------------------------------------------------------------+------------+
```


[//]: # (references: )  
[home-link]: ./_index
[start-link]: ./test-consistency-start
[eval-link]: ./test-consistency-evaluation
[eval-params-link]: ./test-consistency-evaluation#read-iter-params
[diff-link]: ./test-consistency-evaluation#function-diff_statistics
[res-link]: ./_index#understanding-the-test-results
[mpl-link]: https://matplotlib.org/users/navigation_toolbar.html "Link to matplotlib Wiki"
[unitt-link]: https://docs.python.org/3/library/unittest.html?highlight=unittest#module-unittest "Link do Python Docs"
[imperr-link]: https://docs.python.org/3/library/exceptions.html#ImportError "Link to Python Docs"
[moderr-link]: https://docs.python.org/3/library/exceptions.html#ModuleNotFoundError "Link to Python Docs"
[mvs-link]: #TODO:_add_link_to_sim_setup_simple
[ppath-link]: https://docs.python.org/3/library/sys.html#sys.path "Link to Python Docs"
[npf-link]: https://docs.scipy.org/doc/numpy/user/basics.types.html "Link to Numpy Docs: Data types"
[dio-link]: #TODO:link_to_data_io_module
[simset-link]: #TODO:link_to_sim_setups
[root-link]: #TODO:_link_to_project_home "Link to Project Repository"
[file-link]: #TODO:_link_to_this_file_in_repo "Link to File in Project Repository"
