---
title: Bash Script `start.sh`
---

# Bash Script `start.sh`

## Contents

- [Overview](./test-consistency-start#overview)
- [User Guide](./test-consistency-start#user-guide)
  - [Comparison Data and 'default' Arguments][def-link]
  - [Execution](./test-consistency-start#execution)
  - [Output](./test-consistency-start#output)
- [Test Setups](./test-consistency-start#test-setups)


* * * *


## Overview

The [`start.sh`][file-link] script provides a set of preconfigured [test setups](./test-consistency-start#test-setups) to choose  
from for the [`consistency_tests.py`][tests-link] script. Upon execution, a numbered  
list of options to choose from will be displayed.

Use this script with the bash shell:  
```bash
$ bash start.sh
```
Or by calling it directly from the command line:
```bash
$ ./start.sh
```
Then follow the prompts on the screen.


## User Guide

This script is designed to be integrated with the the [pyglasma3d_numba][root-link] project.  
As such it is located inside the [test_consistency][home-link] package folder in the root  
directory of that project:
```
pyglasma3d_numba/
└─ test_consistency
    ├─ ...
    ├─ original
    │  └── T_mv_trial_simple_cython_textfile.dat
    ├─ ...
    ├─ start.sh
    └─ ...
```
{{< hint warning >}}
WARNING:  
To ensure proper execution this location must not be changed.
{{< /hint >}}


### Comparison Data and 'default' Arguments

The data files to compare, that are passed to the [`consistency_tests.py`][tests-link] script,  
are defined on line 40 of the [`start.sh`][file-link] script after the [`-f` flag][tests-params-link]. This line sets the  
default arguments for the [`consistency_tests.py`][tests-link] script.
```bash
40: args="-f test_consistency/original/T_mv_trial_simple_cython_textfile.dat output/T_mv_trial_simple_textfile.dat -v"
```
The files are specified with paths relative to the project root `pyglasma3d_numba/`.  

The first path:  
`test_consistency/original/T_mv_trial_simple_cython_textfile.dat`  
is the path for the [reference data][ref-data-link]. A convenient location for this file is provided  
with the `original/` folder inside this [test_consistency][home-link] package.  

The second path:  
`output/T_mv_trial_simple_textfile.dat`  
refers to the default output folder of [pyglasma3d_numba][root-link] [simulation runs][sst-link],  
that is located in the root directory of the [pyglasma3d_numba][root-link] project.

The [`-v` flag][tests-params-link] toggles the 'verbose' mode for the [`consistency_tests.py`][tests-link] script.


### Execution

This script can be executed from any working directory. Setting the correct  
working directory will be handled automatically by the script itself.

When executing this script directly (as shown [before](./test-consistency-start#overview)) the bash shell set in the  
current environment is used. It is determined by using the `env` command:
```
/usr/bin/env bash
```

Launching the script will lead to the following prompt, where all the available  
[test setups](./test-consistency-start#test-setups) are listed. Selecting a number and confirming with `[enter]` will  
launch the selected setup. The execution can also be aborted at the current  
state by selecting `'q'`.

```
.../pyglasma3d_numba $ test_consistency/start.sh

please input the number of the setup to run...

1 => test pyglasma3d_numba mv_simple setup with fastmath = True
2 => test pyglasma3d_numba mv_simple setup with fastmath = False
3 => test pyglasma3d_numba mv_simple setup in cuda mode with fastmath = True
4 => test pyglasma3d_numba mv_simple setup in cuda mode with fastmath = False
5 => time pyglasma3d_numba mv_debug setup in cuda mode with fastmath = True
6 => test existing output against original/[...].dat with fastmath = True
7 => test existing output against original/[...].dat with fastmath = False
q => ABORT
```

`1` to `5` will at first run the chosen [simulation setup][sst-link]. Then the data produced  
by that simulation run will be tested using the [`consistency_tests.py`][tests-link]  
Python script with the `--verbose` flag set, except for `5` where no tests are  
run. `6` and `7` use the existing output from  
`original/T_mv_trial_simple_textfile.dat`  
and only perform the tests.


### Output

```
...
...
10.5 Complete cycle. 0.013
10.5 Writing energy-momentum tensor to file. 0.071


converting binary output file to text file...

real    0m13.761s
user    0m13.978s
sys     0m0.543s

calling consistency_test.py with the following arguments:
...
...
```

All previous output including the line ```converting binary output ...``` is  
part of the [simulation setups'][sst-link] output. The rest of the output will be generated  
by this script. For options without implicit execution of simulation setups, the  
output only contains the last line from the example above.

First there are three timing values:
- 'real'
- 'user'
- 'sys'  

which are the output of the bash built-in command `time`. They show the elapsed  
real time ('real'), user cpu time ('user') and system cpu time ('sys') for  
the chosen simulation setup, including the amount of time it took the Python  
interpreter to initialize.

After the line ```calling consistency_tests.py ...``` follows the output of  
the [`consistency_tests.py`][tests-link] script. For reference on that output, which  
includes graphs and console logs, please see the section [Output][testsOut-link] of the  
[`consistency_tests.py`][testsOut-link] script documentation. For help on how to interpret  
these results please see the section ['Understanding the Test Results'][results-link].


## Test Setups

There following setups are preconfigured:

#### 1 => test pyglasma3d_numba mv_simple setup with fastmath = True

This option will use the [`mv_simple.py`][mvs-link] simulation setup. It will be executed in the  
default Numba mode with parallelization on the CPU and the option for 'fastmath'  
enabled. See the section on the [`mv_simple.py`][mvs-link] setup for more details.  
To match the 'fastmath' option of the simulation run, the [default arguments][def-link] for the  
[`consistency_tests.py`][tests-link] script are expanded with the [`--fastmath` flag][tests-params-link].


#### 2 => test pyglasma3d_numba mv_simple setup with fastmath = False

This option will use the [`mv_simple.py`][mvs-link] simulation setup. It will be executed in the  
default Numba mode with parallelization on the CPU and the option for 'fastmath'  
disabled. See the section on the [`mv_simple.py`][mvs-link] setup for more details.


#### 3 => test pyglasma3d_numba mv_simple setup in cuda mode with fastmath = True

This option will use the [`mv_simple.py`][mvs-link] simulation setup. It will be executed in the  
advanced CUDA mode with parallelization on the CPU and GPU and the option for  
'fastmath' enabled. See the section on the [`mv_simple.py`][mvs-link] setup for more details.  
To match the 'fastmath' option of the simulation run, the [default arguments][def-link] for the  
[`consistency_tests.py`][tests-link] script are expanded with the [`--fastmath` flag][tests-params-link].


#### 4 => test pyglasma3d_numba mv_simple setup in cuda mode with fastmath = False

This option will use the [`mv_simple.py`][mvs-link] simulation setup. It will be executed in the  
advanced CUDA mode with parallelization on the CPU and GPU and the option for  
'fastmath' disabled. See the section on the [`mv_simple.py`][mvs-link] setup for more details.


#### 5 => time pyglasma3d_numba mv_debug setup in cuda mode with fastmath = True

This option will use the [`mv_debug.py`][mvg-link] simulation setup. It will be executed in the  
advanced CUDA mode with parallelization on the CPU and GPU and the option for  
'fastmath' enabled. The purpose of this option is to provide basic timing numbers  
for the [`mv_debug.py`][mvg-link] setup. There will be no tests conducted on the produced data.  
Thus the output of this option will lack the normally provided [results](./test-consistency-start#output). Only the  
timing numbers will be available in addition to the standard simulation log output.  
As [`mv_debug.py`][mvg-link] is a very resource intensive setup, the number of iteration steps will  
be set to 7 in order to produce results in a reasonable amount of time. See the  
section on the [`mv_debug.py`][mvg-link] setup for more details.

#### 6 => test existing output against original/[...].dat with fastmath = True
#### 7 => test existing output against original/[...].dat with fastmath = False

These options will use existing output from previous runs located at:  
`original/T_mv_trial_simple_textfile.dat`.  
The [`--fastmath` flag][tests-params-link] will be used when calling the [`consistency_tests.py`][tests-link]  
script for option `6`.


[//]: # (references: )  
[def-link]: #comparison-data-and-default-arguments
[tests-link]: ./test-consistency-tests
[testsOut-link]: ./test-consistency-tests#output
[tests-params-link]: ./test-consistency-tests#parameters
[root-link]: #TODO:_link_to_project_home "Link to Project Repository"
[file-link]: #TODO:_link_to_this_file_in_repo "Link to File in Project Repository"
[home-link]: ./_index
[results-link]: ./_index#understanding-the-test-results
[mvs-link]: #TODO:_add_link_to_sim_setup_simple
[mvg-link]: #TODO:_add_link_to_sim_setup_mv_debug
[sst-link]: #TODO:_add_link_to_sim_setups
[ref-data-link]: ./_index#preparing-comparison-data
