---
title: Python Module `evaluation`
---

# Python Module `evaluation`

## Contents

- [Overview](./test-consistency-evaluation#overview)
- [Function `diff_statistics()`](./test-consistency-evaluation#function-diff_statistics)
  - [Parameters](./test-consistency-evaluation#diff-stat-params)
  - [Return Values](./test-consistency-evaluation#diff-stat-return)


* * * *


## Overview

The [`evaluation`][file-link] module is meant to be used in conjunction with the  
[`consistency_tests.py`](./test-consistency-tests) script. It calculates the differences of numerical  
data supplied by data files and performs statistical analysis on the differences.  

The supplied data files have to obey a certain layout. On the first two lines  
there has to be one integer each. The product of those two integers plus 2  
(val1*val2 + 2) has to match the number of total lines in the file. On each  
subsequent line there have to be five whitespace separated values that are  
compatible with the [float64 format of Numpy][npf-link]. The data files produced by the  
[pyglasma3d_numba][root-link] simulation code meet these criteria after being converted  
to text files using the [`data_io`][dio-link] module. An example for a compatible text file  
is provided in section [`Data File Format`][file-link].

## Function `diff_statistics()`

The function `diff_statistics()` calculates the element wise differences  
of supplied numerical data files and normalizes those difference values to  
extract the number of matching digits. This number tells how many digits  
of two numbers are identical when starting from the coefficient of the highest  
power of 10 when viewing the number in standard base 10 representation. For  
example:
{{< katex display >}}
\begin{aligned}
		 & 1.111111111111111 \\
		 & 1.111111425270376 \\
		 & \rArr 7
	\end{aligned}
{{< /katex >}}
As obvious, the number of matching ditigs in this example is 7.

The difference values are calculated following this simple method:  

1. Two 1D Numpy arrays with identical shapes that represent each data set  
   are required. They are sourced from the arguments supplied.  
3. The per element difference of the Numpy arrays is calculated.
4. The calculated difference values are normalized with respect to the  
   magnitudes of the first data file's values. If the first data file contains 0,  
   the other file's data values' magnitudes are used for that specific  
   difference value. If both are 0, the difference is 0 as well.

For example:  
The compared values match up to 7 digits. The 8th digits show a difference of 3.  
{{< katex display >}}
\begin{aligned}
\mathrm{val1:} & \qquad 1.111111111111111\times10^3 \\
\mathrm{val2:} & \qquad 1.111111425270376\times10^3 \\
\mathrm{diff:} & \qquad 0.000000314159265\times10^3 \\
\text{normalized difference:} & \qquad 3.14159265\times10^{-7}
\end{aligned}
{{< /katex >}}

The required 1D Numpy arrays are built by calling the `read_textfile_iter()`  
generator function with the specified files. This function is imported from the  
`data_io.py` module located in the `pyglasma3d_numba_source` directory.  
As this function represents an iterator over the provided file, the calculations  
mentioned above take place in a loop over this iterator.

### Parameters {#diff-stat-params}  

There is one required parameter `files_dict` and one optional parameter  
`skip_scatter`.

`files_dict`  

The `files_dict` is required to be a Python dictionary of the following format:  
```python
files_dict = {
    "source1": "path1",
    "source2": "path2"
    }
```
The dictionary has to have two elements with the hard coded key names  
"source1" and "source2". Each of these elements is a string representing  
the path of one of the data files to source. The file paths have to be  
absolute or relative to the current working directory. Any [`KeyErrors`][kerr-link]  
raised because of supplying a wrong `files_dict` will be handled. 

`skip_scatter`  

The parameter `skip_scatter` is an optional boolean flag, that is set to  
`False` per default. If set to `True`, the return value `scatter` (see next  
section) will be set to `None` and all associated calculations will be skipped.  
Use this if dealing with very large files with lots of data values whose  
accumulated size would surpass the available system memory. It will  
prevent storing all difference and data values in memory. Instead only  
the difference and data values for each iteration step over the  
`read_textfile_iter()` generator 
function as well as the other return  
values will be stored in memory.

### Return Values {#diff-stat-return}  

The `diff_statistics()` function returns a tuple of three values:  
``` python
results, (hist, bins), scatter
```  
The first, `results`, is a Numpy array with shape (N, 3), where N is the number  
of iterations done over the input files by using `read_textfile_iter()`. 
For  
each iteration three values are stored in this array with the following order:  
```
(mean, std, min)
```
Here, `mean` is the average number of matching digits for each iteration,  
`std` is the standard deviation for each iteration and `min` is the minimum  
for each iteration. Note that the averaging process does not include perfect  
matches where the number of matching digits is undefined.

The second value `(hist, bins)` is a tuple itself. For each iteration step  
the frequency of each number of matching digits for a histogram with bins  
`bins` is calculated. The bin edges are:  
```
[none, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, all]
```
If there are difference values which could not be assigned to one of these  
bins, a warning is issued, but the evaluation continues.  
The frequency values for each iteration step are accumulated in the `hist`  
Numpy array. This array has the shape ```(len(bins)-1, )``` and carries the  
[Numpy integer][npf-link] data type.

The `scatter` value is controlled by the parameter `skip_scatter` (see  
section above). If `skip_scatter` is set to `False` (default), scatter will be a  
2D Numpy array with shape (2, n) where n is the total number of all difference  
values. This number is equal to the number of iterations times the size of  
the array returned from `read_textfile_iter()`.  
The first dimension of `scatter` contains the n data values from the first  
source as specified in the `files_dict` parameter. The second dimension  
contains the n difference values calculated over the course of all iterations.  
The 2*n values represent (val, diff) pairs and are the values of a scatter plot.  
For example:
```python
scatter = [[10, 11, 12, 13, 14, 15, 16, 17, 18, 19],
           [ 0,  1,  2,  3,  4,  5,  6,  7,  8,  9]])
pairs = [(v, d) for v, d in zip(scatter[0], scatter[1])]
# [(10, 0), (11, 1), (12, 2), (13, 3), (14, 4), (15, 5), (16, 6), (17, 7), (18, 8), (19, 9)]
```  
If `skip_scatter` is set to `True`, `scatter` will be `None` and none of the  
calculations just mentioned are performed.


[//]: # (references: )  
[file-link]: ./_index#data-file-format
[dio-link]: #TODO:link_to_data_io_module
[simset-link]: #TODO:link_to_sim_setups
[root-link]: #TODO:_link_to_project_home "Link to Project Repository"
[file-link]: #TODO:_link_to_this_file_in_repo "Link to File in Project Repository"
[genf-link]: https://wiki.python.org/moin/Generators "Link to Python Wiki: Generators"
[open-link]: https://docs.python.org/3/library/functions.html#open "Link to Python Docs: open()"
[syse-link]: https://docs.python.org/3/library/sys.html#sys.exit "Link to Python Docs: sys"
[kerr-link]: https://docs.python.org/3/library/exceptions.html#KeyError "Link to Python Docs"
[oserr-link]: https://docs.python.org/3/library/exceptions.html#OSError "Link to Python Docs"
[valerr-link]: https://docs.python.org/3/library/exceptions.html#ValueError "Link to Python Docs"
[npf-link]: https://docs.scipy.org/doc/numpy/user/basics.types.html "Link to Numpy Docs: Data types"
[npltxt-link]: https://docs.scipy.org/doc/numpy/reference/generated/numpy.loadtxt.html "Link to Numpy Docs: loadtxt()"
[npr-link]: https://docs.scipy.org/doc/numpy/reference/generated/numpy.ravel.html "Link to Numpy Docs: ravel()"
