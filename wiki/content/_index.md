---
title: Welcome
type: docs
---

# Welcome to the Wiki of "pyglasma3d_numba"!

## About this Project

This project is a port of the [pyglasma3d][pyglasma3d-cy] project to Python 3. It uses the [Numba][numba-link] open source JIT compiler to parallelize and accelerate the code and also offers [Numba][numba-link] based GPU acceleration with [CUDA][cuda-link] (so it is limited to compatible Nvidia GPUs).  
The currently present version is still in development and does lack lots of the features from the original [pyglasma3d][pyglasma3d-cy].


## Roadmap

  - v0.6  
    full documentation of what is implemented  
    final documentation, properly done with GitLab Pages + minimal READMEs (short, links to wiki)

  - v0.X  
    will have the interpolate charges loops fixed  
    and optimal init? with unified jit and minimal copying  
    fix for em tensor parallelization

  - v1.0  
    final optimizations,  
    perfect code

  - v1.X / vX.X  
    adding more features from the original pyglasma3d code  
    porting necessary modules to python


## Pyglasma3d-Numba Project

## Help for the consistency tests


[//]: # (references: )  
[pyglasma3d-cy]: https://gitlab.com/monolithu/pyglasma3d "Link to original Cython version"
[numba-link]: numba.pydata.org "Link to Numba homepage"
[cuda-link]: https://developer.nvidia.com/cuda-zone "Link to CUDA homepage"
