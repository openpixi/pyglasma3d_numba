#! /usr/bin/env bash

# exit script on error
set -e

# change current working dir to script location
cd -P -- "$(dirname -- "${BASH_SOURCE[0]}")"

# build pdoc and start hugo local server
pdoc --html --force --template-dir=themes/pdoc/ -o content/docs/pyglasma3d-numba/source_code ../pyglasma3d_numba_source/
hugo server &

cd ..
# watch for changes in codefiles and run pdoc when necessary
TIME=$(stat -c %Y pyglasma3d_numba_source/*)

while true; do
    T_TIME=$(stat -c %Y pyglasma3d_numba_source/*)

   if [[ "$T_TIME" != "$TIME" ]]; then
       pdoc --html --force --template-dir=wiki/themes/pdoc/ -o wiki/content/docs/pyglasma3d-numba/source_code pyglasma3d_numba_source/
       TIME=$T_TIME
   fi
   sleep 5
done
