"""A script for Au-Au collisions in the MV-model.

The main input parameters are the collision energy sqrt{s},
the infrared regulator m
and the box dimensions (LT, LL).

This setup is based on the mv.py script and is used for development and
debugging. The simulation terminates after 7 iterations.
"""

import os
import time
import numpy as np

from .common_args import parser, output_arg


# inject setup name into help strings
parser.prog = parser.prog.replace("[setup]", os.path.basename(__file__).split(".")[0])
output_arg.help = output_arg.help.replace(
    "[setup]", os.path.basename(__file__).split(".")[0]
)
# parse cl args
args = parser.parse_args()

# evaluate environment variables to control numba
os.environ["MY_NUMBA_TARGET"] = args.device
os.environ["FASTMATH"] = str(args.fastmath)
os.environ["SYSINFO"] = str(int(args.sysinfo))

# Simulation Setup

# simulation specific imports have to occur after command line args
# have been evaluated (for numba target etc.)
# disable flake8 and pylint errors
# pylint: disable=wrong-import-position,wrong-import-order

import pyglasma3d_numba_source.interpolate as interpolate
import pyglasma3d_numba_source.mv as mv
from pyglasma3d_numba_source.core import Simulation

# import pyglasma3d_numba_source.data_io as data_io

# FIXME: remove profiler controls
if args.device == "cuda":
    from pyglasma3d_numba_source.core import cuda

# FIXME: remove the debug overwrite
args.debug = True

# grid size (make sure that ny == nz): args.nx, args.nt, args.nt
# nx, ny, nz = 2048, 64, 64   # 3.87 GB
# nx, ny, nz = 2048, 32, 32   # 0.97 GB
# nx, ny, nz = 512, 128, 128   # 3.87 GB
# nx, ny, nz = 128, 128, 128   # 0.97 GB
# nx, ny, nz = 64, 256, 256   # 1.92 GB
# nx, ny, nz = 64, 512, 512   # >8 GB

# constants
hbarc = 197.3270  # hbarc [MeV*fm]
RAu = 7.27331  # Gold nuclear radius [fm]

# determine lattice spacings and energy units
aT_fm = args.LT / args.nt
E0 = hbarc / aT_fm
aT = 1.0
aL_fm = args.LL / args.nx
aL = aL_fm / aT_fm
a = [aL, aT, aT]
dt = aL / args.steps

# determine initial condition parameters
gamma = args.sqrts / 2000.0
Qs = np.sqrt((args.sqrts / 1000.0) ** 0.25) * 1000.0
alphas = 12.5664 / (18.0 * np.log(Qs / 217.0))
g = np.sqrt(12.5664 * alphas)
mu = Qs / (g * g * 0.75) / E0
uvt = args.uv / E0
ir = args.m / E0
sigma = RAu / (2.0 * gamma) / aL_fm * aL
sigma_c = sigma / aL

# number of evolution steps (max_iters) and output file path
max_iters = 2 * args.nx // 3
file_path = "./output/" + args.run + "_mv_debug.dat"

# Initialization

# set random seed for run to run comparability
np.random.seed(10)

# initialize simulation object
dims = [args.nx, args.nt, args.nt]
s = Simulation(
    dims=dims,
    a=a,
    dt=dt,
    g=g,
    iter_dims=[1, args.nx - 1, 0, args.nt, 0, args.nt],
    debug=args.debug,
)

t = time.time()
print(f"[t={s.t}]: Initializing left nucleus.")
v = mv.initialize_mv(
    s, x0=args.nx * 0.25 * aL, mu=mu, sigma=sigma, mass=ir, uvt=uvt, orientation=+1
)
interpolate.initialize_charge_planes(s, +1, 0, args.nx // 2)
print("Initialized left nucleus in:", round(time.time() - t, 3))

t = time.time()
print(f"[t={s.t}]: Initializing right nucleus.")
v = mv.initialize_mv(
    s, x0=args.nx * 0.75 * aL, mu=mu, sigma=sigma, mass=ir, uvt=uvt, orientation=-1
)
interpolate.initialize_charge_planes(s, -1, args.nx // 2, args.nx)
print("Initialized right nucleus in:", round(time.time() - t, 3))

# FIXME: remove profiler controls
if args.device == "cuda":
    cuda.profile_start()

s.init()

# Simulation loop

t = time.time()
for it in range(max_iters):
    s.log("\n" + s.mem_info() + "\n")
    tc = time.time()

    # this for loop moves the nuclei exactly one grid cell
    for step in range(args.steps):
        t = time.time()
        s.evolve()
        s.log(s.t, "Complete cycle in:", round(time.time() - t, 3))

    t = time.time()
    s.log(
        "\nGauss constraint squared, avg over simulation box: "
        f"{s.gauss_constraint_squared():.2e}"
    )
    s.log("Writing energy-momentum tensor to file.\n")
    s.write_energy_momentum(max_iters, file_path)
    print(
        f"[t={s.t}]: Iteration step",
        it + 1,
        "/",
        max_iters,
        "complete in:",
        round(time.time() - tc, 3),
    )

    # FIXME: remove limit to 7 iterations
    if it > 5:
        break

# FIXME: remove profiler controls
if args.device == "cuda":
    cuda.profile_stop()


# convert binary output file to readable txt

# print('\n\nconverting binary output file to text file...')
# data_io.write('./output/' + args.run + '_mv_debug_textfile.dat',
#   **data_io.read(file_path)
# )
