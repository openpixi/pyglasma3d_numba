"""A module for parsing simulation setup CL arguments with argparse.

Use the `--help` flag with any setup for all available options.
"""

import argparse
import os


# helper function to check steps arg
def check_steps(arg):
    """Checker for steps argument."""
    try:
        arg = int(arg)
        if arg % 2 == 0 and arg >= 2:
            return arg
        raise argparse.ArgumentTypeError(f"{arg} is not a multiple of 2")
    except ValueError as e:
        raise argparse.ArgumentTypeError(
            f"Invalid check_steps value 'steps={arg}'!"
        ) from e


# set up argparse
parser = argparse.ArgumentParser(
    description="A script for Au-Au collisions in the MV-model.",
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    prog="python3 -m examples.[setup]",
)

# simulation control
output_arg = parser.add_argument(
    "-o",
    "--output",
    type=str,
    default="T",
    metavar="<name>",
    dest="run",
    help='specify the <name> for the output file in "/output/<name>_[setup].dat"',
)
parser.add_argument(
    "-l", "--nl", type=int, default=128, dest="nx", help="longitudinal grid size"
)
parser.add_argument(
    "-t", "--nt", type=int, default=128, dest="nt", help="transverse grid size"
)
parser.add_argument(
    "-L",
    "--LL",
    type=float,
    default=6.0,
    dest="LL",
    help="longitudinal simulation box size in [fm]",
)
parser.add_argument(
    "-T",
    "--LT",
    type=float,
    default=6.0,
    dest="LT",
    help="transverse simulation box size in [fm]",
)
parser.add_argument(
    "-E",
    "--energy",
    type=float,
    default=200000.0,
    dest="sqrts",
    help="collision energy sqrt(s_NN) [MeV]",
    metavar="energy",
)
parser.add_argument("-m", type=float, default=200.0, help="infrared regulator [MeV]")
parser.add_argument(
    "-u",
    "--uv",
    type=float,
    default=10000.0,
    dest="uv",
    help="ultraviolet regulator [MeV]",
)
parser.add_argument(
    "-s",
    "--steps",
    type=check_steps,
    default=4,
    dest="steps",
    help="ratio between dt and aL in multiples of 2, has to be integer",
)


# device control
parser.add_argument(
    "-d",
    "--device",
    type=str,
    default=os.environ.get("MY_NUMBA_TARGET", "cuda"),
    dest="device",
    choices=["cuda", "numba", "python"],
    help="set the target compute device; "
    'this will set the environment variable "MY_NUMBA_TARGET"',
)
parser.add_argument(
    "--fastmath",
    type=int,
    default=os.environ.get("FASTMATH", 1),
    dest="fastmath",
    choices=[0, 1],
    help="configure use of fastmath; "
    'this will set the environment variable "FASTMATH"',
)


# debugging options
parser.add_argument(
    "--sysinfo",
    action="store_true",
    default=bool(int(os.environ.get("SYSINFO", 0))),
    dest="sysinfo",
    help="toggle verbose system information dump on startup; "
    'this will set the environment variable "SYSINFO" to 1',
)
parser.add_argument(
    "--debug", action="store_true", help="set debug mode for verbose output"
)
