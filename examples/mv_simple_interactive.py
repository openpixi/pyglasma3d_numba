"""A script for Au-Au collisions in the MV-model.

The main input parameters are the collision energy sqrt{s},
the infrared regulator m and
the box dimensions (LT, LL).

This is a derived version of the mv.py script.
There is no output file generated.
Instead there is a live plot for each iteration step.
"""

import os
import time
import numpy as np
import matplotlib.pyplot as plt

from .common_args import parser, output_arg


# inject setup name into help strings
parser.prog = parser.prog.replace("[setup]", os.path.basename(__file__).split(".")[0])
output_arg.help = output_arg.help.replace(
    "[setup]", os.path.basename(__file__).split(".")[0]
)
# parse cl args
args = parser.parse_args()

# evaluate environment variables to control numba
os.environ["MY_NUMBA_TARGET"] = args.device
os.environ["FASTMATH"] = str(args.fastmath)
os.environ["SYSINFO"] = str(int(args.sysinfo))

# Simulation Setup

# simulation specific imports have to occur after command line args
# have been evaluated (for numba target etc.)
# disable flake8 and pylint errors
# pylint: disable=wrong-import-position,wrong-import-order

import pyglasma3d_numba_source.interpolate as interpolate
import pyglasma3d_numba_source.mv as mv
from pyglasma3d_numba_source.core import Simulation

# import pyglasma3d_numba_source.data_io as data_io

# constants
hbarc = 197.3270  # hbarc [MeV*fm]
RAu = 7.27331  # Gold nuclear radius [fm]

# determine lattice spacings and energy units
aT_fm = args.LT / args.nt
E0 = hbarc / aT_fm
aT = 1.0
aL_fm = args.LL / args.nx
aL = aL_fm / aT_fm
a = [aL, aT, aT]
dt = aL / args.steps

# determine initial condition parameters
gamma = args.sqrts / 2000.0
Qs = np.sqrt((args.sqrts / 1000.0) ** 0.25) * 1000.0
alphas = 12.5664 / (18.0 * np.log(Qs / 217.0))
g = np.sqrt(12.5664 * alphas)
mu = Qs / (g * g * 0.75) / E0
uvt = args.uv / E0
ir = args.m / E0
sigma = RAu / (2.0 * gamma) / aL_fm * aL
sigma_c = sigma / aL

# number of evolution steps (max_iters) and output file path
max_iters = 2 * args.nx // 3
# file_path = "./output/" + args.run + "_mv_interactive.dat"

# Initialization

# set random seed for run to run comparability
# np.random.seed(10)

# initialize simulation object
dims = [args.nx, args.nt, args.nt]
s = Simulation(
    dims=dims,
    a=a,
    dt=dt,
    g=g,
    iter_dims=[1, args.nx - 1, 0, args.nt, 0, args.nt],
    debug=args.debug,
)

t = time.time()
print("Initializing left nucleus.")
v = mv.initialize_mv(
    s, x0=args.nx * 0.25 * aL, mu=mu, sigma=sigma, mass=ir, uvt=uvt, orientation=+1
)
interpolate.initialize_charge_planes(s, +1, 0, args.nx // 2)
print("Initialized left nucleus.", round(time.time() - t, 3))

t = time.time()
print("Initializing right nucleus.")
v = mv.initialize_mv(
    s, x0=args.nx * 0.75 * aL, mu=mu, sigma=sigma, mass=ir, uvt=uvt, orientation=-1
)
interpolate.initialize_charge_planes(s, -1, args.nx // 2, args.nx)
print("Initialized right nucleus.", round(time.time() - t, 3))

s.init()

# Simulation loop

plt.ion()
t = time.time()
for it in range(max_iters):
    # this for loop moves the nuclei exactly one grid cell
    for step in range(args.steps):
        t = time.time()
        s.evolve()
        print(s.t, "Complete cycle.", round(time.time() - t, 3))

    # data for plotting
    EL, BL, ET, BT, SL = s.energy_momentum_tensor()
    plt.clf()
    plt.axis([0, dims[0], 0, 1])
    plt.autoscale(True, axis="y")
    xaxis = np.arange(1, dims[0] - 1)
    plt.plot(xaxis, EL, c="red")
    plt.plot(xaxis, BL, c="green")
    plt.plot(xaxis, ET, c="blue")
    plt.plot(xaxis, BT, c="cyan")
    plt.plot(xaxis, np.abs(SL) / 2, c="black")
    plt.pause(0.000000001)


# convert binary output file to readable txt

# print('\n\nconverting binary output file to text file...')
# data_io.write('./output/' + args.run + '_mv_interactive_textfile.dat',
#   **data_io.read(file_path)
# )
