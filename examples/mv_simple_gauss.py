"""A setup derived from the mv.py script.

The changes are:
It does not print an output file.
It only prints the information relevant for the gauss constraint and
gauss violation to the terminal.

To get a file from the output use:
`python3 -m examples.mv_simple_gauss > output/mv_simple_gauss_txt.dat`
"""

import os
import time
import numpy as np

from .common_args import parser, output_arg


# inject setup name into help strings
parser.prog = parser.prog.replace("[setup]", os.path.basename(__file__).split(".")[0])
output_arg.help = output_arg.help.replace(
    "[setup]", os.path.basename(__file__).split(".")[0]
)
# parse cl args
args = parser.parse_args()

# evaluate environment variables to control numba
os.environ["MY_NUMBA_TARGET"] = args.device
os.environ["FASTMATH"] = str(args.fastmath)
os.environ["SYSINFO"] = str(int(args.sysinfo))

# Simulation Setup

# simulation specific imports have to occur after command line args
# have been evaluated (for numba target etc.)
# disable flake8 and pylint errors
# pylint: disable=wrong-import-position,wrong-import-order

import pyglasma3d_numba_source.interpolate as interpolate
import pyglasma3d_numba_source.mv as mv
from pyglasma3d_numba_source.core import Simulation

# import pyglasma3d_numba_source.data_io as data_io

# constants
hbarc = 197.3270  # hbarc [MeV*fm]
RAu = 7.27331  # Gold nuclear radius [fm]

# determine lattice spacings and energy units
aT_fm = args.LT / args.nt
E0 = hbarc / aT_fm
aT = 1.0
aL_fm = args.LL / args.nx
aL = aL_fm / aT_fm
a = [aL, aT, aT]
dt = aL / args.steps

# determine initial condition parameters
gamma = args.sqrts / 2000.0
Qs = np.sqrt((args.sqrts / 1000.0) ** 0.25) * 1000.0
alphas = 12.5664 / (18.0 * np.log(Qs / 217.0))
g = np.sqrt(12.5664 * alphas)
mu = Qs / (g * g * 0.75) / E0
uvt = args.uv / E0
ir = args.m / E0
sigma = RAu / (2.0 * gamma) / aL_fm * aL
sigma_c = sigma / aL

# number of evolution steps (max_iters) and output file path
max_iters = 2 * args.nx // 3
file_path = "./output/" + args.run + "_mv_simple_gauss.dat"

# Initialization

# set random seed for run to run comparability
# np.random.seed(10)

# initialize simulation object
dims = [args.nx, args.nt, args.nt]
s = Simulation(
    dims=dims,
    a=a,
    dt=dt,
    g=g,
    iter_dims=[1, args.nx - 1, 0, args.nt, 0, args.nt],
    debug=args.debug,
)

t = time.time()
print("Initializing left nucleus.")
v = mv.initialize_mv(
    s, x0=args.nx * 0.25 * aL, mu=mu, sigma=sigma, mass=ir, uvt=uvt, orientation=+1
)
interpolate.initialize_charge_planes(s, +1, 0, args.nx // 2)
# s.log("Initialized left nucleus.", round(time.time()-t, 3))

t = time.time()
print("Initializing right nucleus.")
v = mv.initialize_mv(
    s, x0=args.nx * 0.75 * aL, mu=mu, sigma=sigma, mass=ir, uvt=uvt, orientation=-1
)
interpolate.initialize_charge_planes(s, -1, args.nx // 2, args.nx)
# s.log("Initialized right nucleus.", round(time.time()-t, 3))

s.init()


# Simulation loop

t = time.time()
for it in range(max_iters):
    # this for loop moves the nuclei exactly one grid cell
    for step in range(args.steps):
        s.evolve()

    print(s.t)
    # t = time.time()
    print("Gauss constraint squared", s.gauss_constraint_squared())
    # print("Gauss violation", gauss.rel_gauss_violation(s))
    # print("timedelta: ", round(time.time()-t, 3))

# convert binary output file to readable txt

# data_io.write('./output/' + args.run +
# '_mv_simple_gauss_textfile.dat', **data_io.read(file_path)
# )
