"""Test setup to check simulation code.

Test are conducted against numerical and methodological errors.

The `scipy` package is required for the tests to work properly.
The custom algebra code is tested against the `scipy.linalg` methods.

Execute this script from within the root dir './pyglasma3d_numba' of
this project via:
`python -m unittests.tests`

Or use the unittest runner of your IDE. Don't forget to set the working
directory to the project root in that case.
"""

# utilities
import unittest

import numpy as np
from scipy.linalg import expm

# simulation code
from pyglasma3d_numba_source import gauge, gauss, lattice, parallel
from pyglasma3d_numba_source import solver_leapfrog as leapfrog
from pyglasma3d_numba_source.core import Simulation

# fancy unit test report to cl
from . import TestRunner

# TBD: restructure unit tests, get more coverage
# and add docstrings to all functions
# disable flake8 and pylint errors for missing docstrings
# pylint: disable=missing-function-docstring


class TestSimulation(unittest.TestCase):
    """Set of tests against numerical and methodological errors."""

    @classmethod
    def setUpClass(cls):
        """Set up test simulation object."""

        cls.nx, cls.ny, cls.nz = 8, 12, 7
        cls.dims = [cls.nx, cls.ny, cls.nz]
        cls.iter_dims = [0, cls.nx, 0, cls.ny, 0, cls.nz]
        cls.spacings = [0.1, 1.0, 1.0]
        cls.gc = 2.0
        cls.s = Simulation(
            cls.dims, cls.iter_dims, cls.spacings, cls.spacings[0] / 4.0, cls.gc
        )
        cls.s.init()

        cls.pauli = np.array([[[0, 1], [1, 0]], [[0, -1j], [1j, 0]], [[1, 0], [0, -1]]])
        cls.RNG = np.random.default_rng()

    # Grid function tests (indices, shifting, ...)

    def test_index(self):
        """Test lattice.get_index() and lattice.get_point()."""

        indices = np.array(range(self.nx * self.ny * self.nz)).reshape(
            (self.nx, self.ny, self.nz)
        )
        indices_lattice = np.zeros((self.nx, self.ny, self.nz), dtype=np.int64)
        for ix in range(self.nx):
            for iy in range(self.ny):
                for iz in range(self.nz):
                    indices_lattice[ix, iy, iz] = lattice.get_index(
                        ix, iy, iz, self.s.dims
                    )

        self.assertTrue(np.array_equal(indices, indices_lattice))

        # test some random points back and forth
        for _ in range(100):
            pos = tuple(self.RNG.integers(0, self.s.dims[d]) for d in range(3))
            x = lattice.get_index(*pos, self.s.dims)
            posl = lattice.get_point(x, self.s.dims)

            self.assertTrue(np.array_equal(pos, posl))

    def test_shift(self):
        """Test lattice.shift() at boundaries and random positions."""

        index = lattice.get_index(0, 0, 0, self.s.dims)
        self.assertEqual(index, 0)

        # boundary tests
        self.assertEqual(lattice.shift(index, 0, 1, self.s.dims), self.ny * self.nz)
        self.assertEqual(lattice.shift(index, 1, 1, self.s.dims), self.nz)
        self.assertEqual(lattice.shift(index, 2, 1, self.s.dims), 1)

        self.assertEqual(
            lattice.shift(index, 0, -1, self.s.dims), (self.nx - 1) * self.ny * self.nz
        )
        self.assertEqual(
            lattice.shift(index, 1, -1, self.s.dims), (self.ny - 1) * self.nz
        )
        self.assertEqual(lattice.shift(index, 2, -1, self.s.dims), (self.nz - 1))

        # random positions within box
        for _ in range(100):
            pos = (
                self.RNG.integers(1, self.nx - 1),
                self.RNG.integers(1, self.ny - 1),
                self.RNG.integers(1, self.nz - 1),
            )
            index = lattice.get_index(*pos, self.s.dims)

            direction = self.RNG.integers(0, 3)
            shifted_index = lattice.shift(index, direction, 1, self.s.dims)
            shifted_pos = lattice.get_point(shifted_index, self.s.dims)
            self.assertEqual(shifted_pos[direction], pos[direction] + 1)

            shifted_index = lattice.shift(index, direction, -1, self.s.dims)
            shifted_pos = lattice.get_point(shifted_index, self.s.dims)
            self.assertEqual(shifted_pos[direction], pos[direction] - 1)

    def test_shift2(self):
        """Test lattice.shift2() at boundaries and random positions."""

        index = lattice.get_index(0, 0, 0, self.s.dims)
        self.assertEqual(index, 0)

        # boundary tests
        self.assertEqual(
            lattice.shift2(index, 0, 1, self.s.dims, self.s.acc), self.ny * self.nz
        )
        self.assertEqual(lattice.shift2(index, 1, 1, self.s.dims, self.s.acc), self.nz)
        self.assertEqual(lattice.shift2(index, 2, 1, self.s.dims, self.s.acc), 1)

        self.assertEqual(
            lattice.shift2(index, 0, -1, self.s.dims, self.s.acc),
            (self.nx - 1) * self.ny * self.nz,
        )
        self.assertEqual(
            lattice.shift2(index, 1, -1, self.s.dims, self.s.acc),
            (self.ny - 1) * self.nz,
        )
        self.assertEqual(
            lattice.shift2(index, 2, -1, self.s.dims, self.s.acc), (self.nz - 1)
        )

        # random positions within box
        for _ in range(100):
            pos = (
                self.RNG.integers(1, self.nx - 1),
                self.RNG.integers(1, self.ny - 1),
                self.RNG.integers(1, self.nz - 1),
            )
            index = lattice.get_index(*pos, self.s.dims)

            direction = self.RNG.integers(0, 3)
            shifted_index = lattice.shift2(index, direction, 1, self.s.dims, self.s.acc)
            shifted_pos = lattice.get_point(shifted_index, self.s.dims)
            self.assertEqual(shifted_pos[direction], pos[direction] + 1)

            shifted_index = lattice.shift2(
                index, direction, -1, self.s.dims, self.s.acc
            )
            shifted_pos = lattice.get_point(shifted_index, self.s.dims)
            self.assertEqual(shifted_pos[direction], pos[direction] - 1)

    # SU(2) group and algebra unit tests:
    # checking against numpy matrix operations

    # some useful functions for converting the su2 parametrization
    # into matrices and back

    def su2_random_algebra(self):
        return 3.0 * (self.RNG.random(3) - 0.5)

    def su2_random_group(self):
        a = self.su2_random_algebra()
        ma = expm(self.su2_algebra(a) * 1.0j)
        comp = self.comps_group(ma)
        return a, ma, comp

    def su2_algebra(self, a):
        result = np.zeros((2, 2), dtype=np.complex)
        for i in range(3):
            result += 0.5 * a[i] * self.pauli[i]
        return result

    def su2_group(self, u):
        result = np.zeros((2, 2), dtype=np.complex)
        result += u[0] * np.identity(2)
        for i in range(3):
            result += u[i + 1] * self.pauli[i]
        return result

    def comps_algebra(self, ma):
        result = np.zeros(3)
        result[:] = np.real(np.trace(ma.dot(self.pauli[:])))
        return result

    def comps_group(self, mu):
        result = np.zeros(4)
        result[0] = np.real(0.5 * np.trace(mu))
        for i in range(3):
            result[i + 1] = np.imag(0.5 * np.trace(mu.dot(self.pauli[i])))
        return result

    def proj(self, mu):
        result = np.zeros(3)
        for i in range(3):
            result[i] = np.imag(np.trace(mu.dot(self.pauli[i])))
        return result

    def test_su2_multiplication(self):
        """Test SU(2) group algebra against numpy matrix product."""

        # multiplication of 2 random su(2) matrices
        _, a_m, a_c = self.su2_random_group()
        _, b_m, b_c = self.su2_random_group()
        r1 = self.comps_group(a_m.dot(b_m))
        r2 = self.comps_group(a_m.conj().T.dot(b_m))
        r3 = self.comps_group(a_m.dot(b_m.conj().T))
        r4 = self.comps_group(a_m.conj().T.dot(b_m.conj().T))

        lr1 = lattice.mul2(a_c, b_c, 1, 1)
        lr1_fast = lattice.mul2_fast(a_c, b_c)
        lr2 = lattice.mul2(a_c, b_c, -1, 1)
        lr3 = lattice.mul2(a_c, b_c, 1, -1)
        lr4 = lattice.mul2(a_c, b_c, -1, -1)

        self.assertTrue(np.allclose(r1, lr1))
        self.assertTrue(np.allclose(r1, lr1_fast))
        self.assertTrue(np.allclose(r2, lr2))
        self.assertTrue(np.allclose(r3, lr3))
        self.assertTrue(np.allclose(r4, lr4))

        # multiplication of 4 random matrices
        _, c_m, c_c = self.su2_random_group()
        _, d_m, d_c = self.su2_random_group()

        r1 = self.comps_group(a_m.dot(b_m).dot(c_m.conj().T).dot(d_m.conj().T))
        r2 = self.comps_group(a_m.dot(b_m.conj().T).dot(c_m.conj().T).dot(d_m))
        lr1 = lattice.mul4(a_c, b_c, c_c, d_c, 1, 1, -1, -1)
        lr2 = lattice.mul4(a_c, b_c, c_c, d_c, 1, -1, -1, 1)

        self.assertTrue(np.allclose(r1, lr1))
        self.assertTrue(np.allclose(r2, lr2))

    def test_su2_mexp_proj(self):
        """Test SU(2) group algebra against numpy mexp and proj."""

        a_a, a_m, _ = self.su2_random_group()
        r_exp = self.comps_group(a_m)
        lr_exp = lattice.mexp(a_a, 1.0)

        r_proj = self.proj(a_m)
        lr_proj = lattice.proj(self.comps_group(a_m))

        self.assertTrue(np.allclose(r_exp, lr_exp))
        self.assertTrue(np.allclose(r_proj, lr_proj))

        # special case: zero field exponentiated
        lr_exp = lattice.mexp(a_a, 0.0)
        self.assertTrue(np.allclose(lr_exp, [1.0, 0.0, 0.0, 0.0]))

    # Plaquette functions

    def pos_plaquette(self, i_m, i_c, d, start_pos):
        """Positive plaquette."""

        a_m, b_m, c_m, d_m = i_m
        a_c, b_c, c_c, d_c = i_c
        di, di_v, dj, dj_v = d

        plaq = self.comps_group(a_m.dot(b_m).dot(c_m.conj().T).dot(d_m.conj().T))

        pos0 = start_pos
        pos1 = start_pos + di_v
        pos2 = start_pos + dj_v
        pos3 = start_pos

        x0 = lattice.get_index(*pos0, self.s.dims)
        x1 = lattice.get_index(*pos1, self.s.dims)
        x2 = lattice.get_index(*pos2, self.s.dims)
        x3 = lattice.get_index(*pos3, self.s.dims)

        gi0 = int(4 * (3 * x0 + di))
        gi1 = int(4 * (3 * x1 + dj))
        gi2 = int(4 * (3 * x2 + di))
        gi3 = int(4 * (3 * x3 + dj))

        # reset simulation object
        self.s = Simulation(
            self.dims, self.iter_dims, self.spacings, self.spacings[0] / 4.0, self.gc
        )
        self.s.init()

        # if using CUDA, copy live data to host
        # next steps operate explicitly on host arrays
        if self.s.use_device == "cuda":
            self.s.copy_to_host()

        self.s.u0_h[gi0 : gi0 + 4 : 1] = a_c
        self.s.u0_h[gi1 : gi1 + 4 : 1] = b_c
        self.s.u0_h[gi2 : gi2 + 4 : 1] = c_c
        self.s.u0_h[gi3 : gi3 + 4 : 1] = d_c

        lplaq = lattice.plaq_pos(self.s.u0_h, x0, di, dj, self.s.dims, self.s.acc)

        return np.allclose(plaq, lplaq)

    def neg_plaquette(self, i_m, i_c, d, start_pos):
        """Negative plaquette."""

        a_m, b_m, c_m, d_m = i_m
        a_c, b_c, c_c, d_c = i_c
        di, di_v, dj, dj_v = d

        plaq = self.comps_group(a_m.dot(b_m.conj().T).dot(c_m.conj().T).dot(d_m))

        pos0 = start_pos
        pos1 = start_pos + di_v - dj_v
        pos2 = start_pos - dj_v
        pos3 = start_pos - dj_v

        x0 = lattice.get_index(*pos0, self.s.dims)
        x1 = lattice.get_index(*pos1, self.s.dims)
        x2 = lattice.get_index(*pos2, self.s.dims)
        x3 = lattice.get_index(*pos3, self.s.dims)

        gi0 = int(4 * (3 * x0 + di))
        gi1 = int(4 * (3 * x1 + dj))
        gi2 = int(4 * (3 * x2 + di))
        gi3 = int(4 * (3 * x3 + dj))

        # reset simulation object
        self.s = Simulation(
            self.dims, self.iter_dims, self.spacings, self.spacings[0] / 4.0, self.gc
        )
        self.s.init()

        # if using CUDA, copy live data to host
        # next steps operate explicitly on host arrays
        if self.s.use_device == "cuda":
            self.s.copy_to_host()

        self.s.u0_h[gi0 : gi0 + 4 : 1] = a_c
        self.s.u0_h[gi1 : gi1 + 4 : 1] = b_c
        self.s.u0_h[gi2 : gi2 + 4 : 1] = c_c
        self.s.u0_h[gi3 : gi3 + 4 : 1] = d_c

        lplaq = lattice.plaq_neg(self.s.u0_h, x0, di, dj, self.s.dims, self.s.acc)

        return np.allclose(plaq, lplaq)

    def test_plaquettes(self):
        """Test plaquette calculation against numpy methods."""

        # test a few random positions and directions
        for _ in range(10):
            # setup a plaquette to calculate
            randoms = np.arange(3)
            self.RNG.shuffle(randoms)
            di = randoms[0]
            dj = randoms[1]

            di_v = np.eye(1, 3, di)[0]
            dj_v = np.eye(1, 3, dj)[0]

            _, a_m, a_c = self.su2_random_group()
            _, b_m, b_c = self.su2_random_group()
            _, c_m, c_c = self.su2_random_group()
            _, d_m, d_c = self.su2_random_group()

            start_pos = np.fromiter(
                (self.RNG.integers(0, self.dims[i]) for i in range(3)),
                int,
                count=3,
            )

            # 'positive' plaquette
            self.assertTrue(
                self.pos_plaquette(
                    (a_m, b_m, c_m, d_m),
                    (a_c, b_c, c_c, d_c),
                    (di, di_v, dj, dj_v),
                    start_pos,
                )
            )

            # 'negative' plaquette
            self.assertTrue(
                self.neg_plaquette(
                    (a_m, b_m, c_m, d_m),
                    (a_c, b_c, c_c, d_c),
                    (di, di_v, dj, dj_v),
                    start_pos,
                )
            )

    # Gauge link update tests

    def test_gauge_link_update(self):
        """Test leapfrog.evolve_u() for correct gauge link update."""

        # reset simulation object
        self.s = Simulation(
            self.dims, self.iter_dims, self.spacings, self.spacings[0] / 4.0, self.gc
        )
        self.s.init()

        # set random electric field
        rand_e = (self.RNG.random(3 * 3 * self.s.N) - 0.5) * 0.5
        parallel.set_field_to_field_kernel(0, rand_e.size, self.s.e, rand_e, 1, 0)

        # compute what gauge links should look like
        dt = self.s.dt
        u_r = np.empty(4 * 3 * self.s.N, dtype=np.float64)
        for i in range(3 * self.s.N):
            r = expm(-1.0j * dt * self.su2_algebra(self.s.e[3 * i : 3 * (i + 1)]))
            u_r[4 * i : 4 * (i + 1)] = self.comps_group(r)

        # evolve gauge links
        leapfrog.evolve_u(self.s)

        self.assertTrue(np.allclose(u_r, self.s.u1))

    def test_inv_gauge_link_update(self):
        """Test leapfrog.evolve_u_inv() for inv gauge link update."""

        # reset simulation object
        self.s = Simulation(
            self.dims, self.iter_dims, self.spacings, self.spacings[0] / 4.0, self.gc
        )
        self.s.init()

        # set random electric field
        rand_e = (self.RNG.random(3 * 3 * self.s.N) - 0.5) * 0.1
        parallel.set_field_to_field_kernel(0, rand_e.size, self.s.e, rand_e, 1, 0)

        # evolve
        leapfrog.evolve_u(self.s)
        e = np.copy(self.s.e)

        # do inverse gauge link update
        # `evolve_u_inv()` uses force_dev='numba' for its kernels
        # this requires manual copy to host if use_device == 'cuda'
        if self.s.use_device == "cuda":
            self.s.copy_to_host()
            # change live pointers to host arrays
            self.s.u0 = self.s.u0_h
            self.s.u1 = self.s.u1_h
            self.s.e = self.s.e_h

        leapfrog.evolve_u_inv(self.s)

        # compare electric field
        self.assertTrue(np.allclose(e, self.s.e))

    # Gauss constraint test

    def prep_e(self):
        r"""Set up specific electric field.

        E^a_{x,i} = A_i B^a (n_j x_j)
        (\partial_i E^a_{x,i})^2 = (A_i n_i)^2 (B^a)^2.

        Works on the host data for e field: e_h
        """
        amplitude = 1.0
        n = self.RNG.uniform(-0.5, 0.5, 3)
        n /= np.linalg.norm(n)
        a = self.RNG.uniform(-amplitude, amplitude, 3)
        b = np.array(self.RNG.uniform(-amplitude, amplitude, 3), np.float64)
        unit = np.zeros(3, dtype=np.float64)
        unit[:] = self.s.g * self.s.a

        for ix in range(self.s.dims[0]):
            for iy in range(self.s.dims[1]):
                for iz in range(self.s.dims[2]):
                    pos = (ix, iy, iz)
                    x = lattice.get_index(*pos, self.s.dims)
                    pos2 = lattice.get_point(x, self.s.dims)
                    self.assertTrue(np.array_equal(pos, pos2))

                    dot = (
                        self.s.a[0] * pos[0] * n[0]
                        + self.s.a[1] * pos[1] * n[1]
                        + self.s.a[2] * pos[2] * n[2]
                    )
                    for d in range(3):
                        field_index = 3 * (3 * x + d)
                        self.s.e_h[field_index : field_index + 3] = (
                            a[d] * b[0:3] * dot * unit[d]
                        )

        return a, b, n

    def test_gauss_constraint(self):
        """Test gauss module against analytic results."""

        # reset simulation object
        self.s = Simulation(
            self.dims, self.iter_dims, self.spacings, self.spacings[0] / 4.0, self.gc
        )
        self.s.init()

        for _ in range(10):
            # reset fields
            self.s.reset()

            # if using CUDA, copy live data to host
            # next steps operate partially explicitly on host arrays
            if self.s.use_device == "cuda":
                self.s.copy_to_host()

            # set up e field
            # this changes host data
            a, b, n = self.prep_e()

            # if using CUDA, update live arrays on device
            if self.s.use_device == "cuda":
                self.s.copy_to_device()

            # check against analytical result
            gauss_analytic = np.dot(a, n) ** 2 * np.dot(b, b)

            for _ in range(100):
                pos = tuple(self.RNG.integers(1, self.s.dims[d] - 1) for d in range(3))
                x = lattice.get_index(*pos, self.s.dims)
                gauss_lattice = gauss.gauss_sq(self.s, x)

                self.assertTrue(np.isclose(gauss_analytic, gauss_lattice))

            # apply random gauge
            g = np.zeros(4 * self.nx * self.ny * self.nz, dtype=np.float64)
            for x in range(self.nx * self.ny * self.nz):
                _, _, rg = self.su2_random_group()
                g[4 * x : 4 * (x + 1)] = rg

            # this changes live data
            gauge.apply_gauge(self.s, g)

            # if using CUDA, copy live data to host
            # as gauss_sq works on host arrays
            if self.s.use_device == "cuda":
                self.s.copy_to_host()

            # check against analytical result
            for _ in range(100):
                pos = tuple(self.RNG.integers(1, self.s.dims[d] - 1) for d in range(3))
                x = lattice.get_index(*pos, self.s.dims)
                gauss_lattice = gauss.gauss_sq(self.s, x)

                self.assertTrue(np.isclose(gauss_analytic, gauss_lattice))

            # evolve for some time
            # this changes live data
            for _ in range(100):
                self.s.evolve()

            # if using CUDA, copy live data to host
            # as gauss_sq works on host arrays
            if self.s.use_device == "cuda":
                self.s.copy_to_host()

            # check against analytical result
            for _ in range(100):
                pos = tuple(self.RNG.integers(1, self.s.dims[d] - 1) for d in range(3))
                x = lattice.get_index(*pos, self.s.dims)
                gauss_lattice = gauss.gauss_sq(self.s, x)

                self.assertTrue(np.isclose(gauss_analytic, gauss_lattice))


class TestParallel(unittest.TestCase):
    """Test parallel routines for array manipulation."""

    @classmethod
    def setUpClass(cls):
        """Set up test arrays."""

        cls.arr = np.arange(964)
        cls.ref = np.array(cls.arr)
        cls.a = np.array(cls.arr)

    def reset_arr(self):
        """Reset arrays."""
        self.ref[:] = self.arr
        self.a[:] = self.arr

    def test_set_field_zero(self):
        """Test parallel.set_field_zero_kernel."""

        self.reset_arr()

        self.ref[191:659] = 0
        parallel.set_field_zero_kernel(191, 659, self.a)
        self.assertTrue(np.array_equal(self.ref, self.a))

        self.ref[:100] = 0
        parallel.set_field_zero_kernel(0, 100, self.a)
        self.assertTrue(np.array_equal(self.ref, self.a))

        self.ref[700:] = 0
        parallel.set_field_zero_kernel(700, self.a.size, self.a)
        self.assertTrue(np.array_equal(self.ref, self.a))

        self.reset_arr()
        self.ref[:] = 0
        parallel.set_field_zero_kernel(0, self.a.size, self.a)
        self.assertTrue(np.array_equal(self.ref, self.a))

    def test_set_field_to_constant(self):
        """Test parallel.set_field_to_constant_kernel."""

        # start, stop and range are multiples of step
        self.reset_arr()
        self.ref[:360:4] = -1
        parallel.set_field_to_constant_kernel(
            0, int(np.ceil(360 / 4)), self.a, -1, 4, 0
        )
        self.ref[600::4] = -1
        parallel.set_field_to_constant_kernel(
            0, int(np.ceil((self.a.size - 600) / 4)), self.a, -1, 4, 600
        )
        self.assertTrue(np.array_equal(self.ref, self.a))

        # either start or stop are multiples of step
        self.reset_arr()
        self.ref[:361:4] = -1
        parallel.set_field_to_constant_kernel(
            0, int(np.ceil(361 / 4)), self.a, -1, 4, 0
        )
        self.ref[601::4] = -1
        parallel.set_field_to_constant_kernel(
            0, int(np.ceil((self.a.size - 601) / 4)), self.a, -1, 4, 601
        )
        self.assertTrue(np.array_equal(self.ref, self.a))

        self.reset_arr()
        self.ref[1:360:4] = -1
        parallel.set_field_to_constant_kernel(
            0, int(np.ceil((360 - 1) / 4)), self.a, -1, 4, 1
        )
        self.ref[600:761:4] = -1
        parallel.set_field_to_constant_kernel(
            0, int(np.ceil((761 - 600) / 4)), self.a, -1, 4, 600
        )
        # nothing is multiple of step
        self.ref[801:963:4] = -1
        parallel.set_field_to_constant_kernel(
            0, int(np.ceil((963 - 801) / 4)), self.a, -1, 4, 801
        )
        self.assertTrue(np.array_equal(self.ref, self.a))

        self.reset_arr()
        self.ref[::4] = -1
        parallel.set_field_to_constant_kernel(
            0, int(np.ceil(self.a.size / 4)), self.a, -1, 4, 0
        )
        self.assertTrue(np.array_equal(self.ref, self.a))

    def test_set_field_to_field(self):
        """Test parallel.set_field_to_field_kernel."""

        self.reset_arr()
        source = np.full(int(np.ceil((963 - 801) / 4)), -1)
        self.ref[801:963:4] = source
        parallel.set_field_to_field_kernel(0, source.size, self.a, source, 4, 801)
        self.assertTrue(np.array_equal(self.ref, self.a))

        self.reset_arr()
        source = np.full(int(np.ceil(self.ref.size / 4)), -1)
        self.ref[::4] = source
        parallel.set_field_to_field_kernel(0, source.size, self.a, source, 4, 0)
        self.assertTrue(np.array_equal(self.ref, self.a))


# start the tests when called as the main module
if __name__ == "__main__":
    TestRunner.main()
