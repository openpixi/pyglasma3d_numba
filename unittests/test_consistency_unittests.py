"""Test setup to verify the `test_consistency` package.

The unit tests in this file verify the functionality of the
`test_consistency.evaluation` module by comparing the results to the
expected results that are based on the heavily controlled test data.
The tests also cover exceptions raised from poorly formatted data files.

Additionally the plotting functionality of the
`test_consistency/consistency_tests.py` script is validated for a series
of test data.
"""

import gc
import os
import subprocess
import tempfile
import unittest

import numpy as np
from numpy.random import default_rng
from test_consistency import evaluation

# fancy unit test report to cl
from . import TestRunner


class TestEvaluation(unittest.TestCase):
    """Test `test_consistency.evaluation` module routines."""

    @classmethod
    def setUpClass(cls):
        """Set up reference data set."""

        cls.values = 2240
        cls.chunks = (7, 32)
        cls.columns = (5, 10)
        cls.chulumns = [(ch, co) for ch in cls.chunks for co in cls.columns]

        # generate reference data
        rng = default_rng()
        # random floats in interval [-9, 9)
        cls.ref_base = rng.random(cls.values)
        cls.ref_base = 18 * cls.ref_base - 9
        # random integers in interval [-30, 6]
        cls.ref_expo = rng.integers(
            low=-30, high=6, size=cls.values, dtype=np.int8, endpoint=True
        )
        cls.ref_data = cls.ref_base * np.float_power(10.0, cls.ref_expo)

    def generate_test_data(self):
        """Generate base, expo pair to shift reference data.

        Args:
            values: Dimensions of the np array to return as base.
        """
        # create new random number generator for each run
        rng = default_rng()

        # add a random sign
        sign = np.ones(self.values)
        sign[: sign.size // 2 + 1] = -1
        rng.shuffle(sign)

        # fix base to 1.11111111...
        base = 10 / 9 * sign
        # normal sampled values mu, sigma
        expo = rng.integers(low=-10, high=3)

        return base, expo

    def generate_temporary_file(self, data, chunk, column):
        """Generate tmp file with data arranged in columns.

        The output format is compatible with
        `pyglasma3d_numba_source.data_io` and contains the header lines.
        """
        file_ref = tempfile.TemporaryFile("w+")

        data = np.reshape(data, (self.values // column, column))
        np.savetxt(
            file_ref,
            data,
            delimiter=" ",
            header=f"{chunk}\n{(self.values // column)//chunk}",
            comments="",
        )

        file_ref.seek(0)
        return file_ref

    @staticmethod
    def count_histogram(expo, hist):
        """Return bin edges for histogram based on `expo`."""

        # calculate correct bin edges based on expo
        if expo > 1:
            bin_edge = hist.size
        elif expo < -18:
            bin_edge = 1
        else:
            bin_edge = hist.size + expo + 1
        # count values
        matching = np.sum(hist[:bin_edge])
        mismatching = np.sum(hist[bin_edge:])
        total = np.sum(hist)

        return matching, mismatching, total

    def test_evaluation(self):
        """Test evaluation module based on histogram data.

        Test for expo in [-20, 2] with different combinations of columns
        and lines per chunk (both =/= 1). Pass if 98% of values are at
        the expected value for matching digits. This is a fault tolerant
        test that works around the outliers produced by numpy math.
        """
        for expo in range(-20, 3):
            for chunk, col in self.chulumns:

                ref_file = self.generate_temporary_file(self.ref_data, chunk, col)

                base, _ = self.generate_test_data()
                test_file = self.generate_temporary_file(
                    self.ref_data + base * np.float_power(10.0, self.ref_expo + expo),
                    chunk,
                    col,
                )

                _, (hist, _), _ = evaluation.diff_statistics(
                    {
                        "source1": ref_file.fileno(),
                        "source2": test_file.fileno(),
                    }
                )

                matching, mismatching, total = self.count_histogram(expo, hist)
                self.assertGreaterEqual(
                    matching / total,
                    0.98,
                    f"{expo=}\t{chunk=}\t{col=}\n"
                    f"(fail) #: {mismatching}\t"
                    f"(fail) %: {mismatching/total}",
                )

                # this is necessary to clean up the tmp files
                del ref_file, test_file
                gc.collect()

    def test_evaluation_1_chunk_n_cols(self):
        """Test evaluation module based on histogram and mean data.

        All values are evaluated as one chunk with different number of
        columns. A random exponent out of [-10,2] is selected for each
        configuration.
        """
        for col in (1,) + self.columns:
            chunk = self.values // col

            ref_file = self.generate_temporary_file(self.ref_data, chunk, col)

            base, expo = self.generate_test_data()
            test_file = self.generate_temporary_file(
                self.ref_data + base * np.float_power(10.0, self.ref_expo + expo),
                chunk,
                col,
            )

            stats, (hist, _), _ = evaluation.diff_statistics(
                {
                    "source1": ref_file.fileno(),
                    "source2": test_file.fileno(),
                }
            )

            matching, mismatching, total = self.count_histogram(expo, hist)
            self.assertGreaterEqual(
                matching / total,
                0.98,
                f"{expo=}\t{chunk=}\t{col=}\n"
                f"(fail) #: {mismatching}\t"
                f"(fail) %: {mismatching/total}",
            )

            self.assertTrue(
                np.allclose(
                    np.round(stats[:, 0]), expo if expo < 1 else 0, rtol=0, atol=1
                ),
                "Fail for testing the evaluation mean:\n"
                f"{expo=}\t{chunk=}\t{col=}\n"
                f"(fail) #: {mismatching}\t"
                f"(fail) %: {mismatching/total}",
            )

            # this is necessary to clean up the tmp files
            del ref_file, test_file
            gc.collect()

    def test_evaluation_1_chunk_1_col(self):
        """Test evaluation module based on histogram data.

        The data is arranged as 1 column where each line is a separate
        chunk. A random exponent out of [-10,2] is selected.
        """
        chunk, col = 1, 1

        ref_file = self.generate_temporary_file(self.ref_data, chunk, col)

        base, expo = self.generate_test_data()
        test_file = self.generate_temporary_file(
            self.ref_data + base * np.float_power(10.0, self.ref_expo + expo),
            chunk,
            col,
        )

        _, (hist, _), _ = evaluation.diff_statistics(
            {
                "source1": ref_file.fileno(),
                "source2": test_file.fileno(),
            }
        )

        matching, mismatching, total = self.count_histogram(expo, hist)
        self.assertGreaterEqual(
            matching / total,
            0.98,
            f"{expo=}\t{chunk=}\t{col=}\n"
            f"(fail) #: {mismatching}\t"
            f"(fail) %: {mismatching/total}",
        )

        # this is necessary to clean up the tmp files
        del ref_file, test_file
        gc.collect()

    def test_zero_data_values(self):
        """Test evaluation module for data with zeros.

        Two tests for expo in [-10,2] with 1 column and 1 chunk. First
        only random 0.0 in ref data are tested. Then the 0.0 values
        are in the same locations for ref and test data, and the test
        checks if the number of perfect matches (0.0 == 0.0) is equal
        to the number of 0.0 substituted.
        The % for a match are reduced to 90, because the results are
        very unstable for the chosen shift.
        """

        # scatter ref_data with zeros
        rng = default_rng()
        n_zeros = rng.integers(low=self.values // 3, high=2 * self.values // 3)
        replace_mask = np.full_like(self.ref_data, False, dtype=np.bool)
        replace_mask[:n_zeros] = True
        rng.shuffle(replace_mask)
        new_ref_data = np.array(self.ref_data)
        new_ref_data[replace_mask] = 0.0

        for expo in range(-10, 3):
            # lower boundary far enough, so that perfect matches only
            # result from both ref and test data = 0.0
            ref_file = self.generate_temporary_file(new_ref_data, self.values, 1)

            base, _ = self.generate_test_data()
            test_data = new_ref_data + base * np.float_power(10.0, self.ref_expo + expo)
            test_file = self.generate_temporary_file(
                test_data,
                self.values,
                1,
            )

            _, (hist, _), _ = evaluation.diff_statistics(
                {
                    "source1": ref_file.fileno(),
                    "source2": test_file.fileno(),
                }
            )

            matching, mismatching, total = self.count_histogram(expo, hist)
            self.assertGreaterEqual(
                matching / total,
                0.90,
                "Fail for testing zeros in the reference data.\n"
                f"{expo=}\n(fail) #: {mismatching}\t(fail) %: {mismatching/total}",
            )

            # this is necessary to clean up the tmp files
            del test_file, ref_file
            gc.collect()

            ref_file = self.generate_temporary_file(new_ref_data, self.values, 1)
            # scatter test data with zeros at same pos as ref data
            test_data[replace_mask] = 0.0
            test_file = self.generate_temporary_file(
                test_data,
                self.values,
                1,
            )

            _, (hist, _), _ = evaluation.diff_statistics(
                {
                    "source1": ref_file.fileno(),
                    "source2": test_file.fileno(),
                }
            )

            # test number of perfect matches: ref=0.0, test=0.0
            self.assertEqual(
                np.sum(replace_mask),
                hist[0],
                "Fail for testing zeros in both reference and test data.\n"
                f"{expo=}\ncorrect: {np.sum(replace_mask)}\n(fail): {hist[0]}",
            )

            # this is necessary to clean up the tmp files
            del test_file, ref_file
            gc.collect()

    def test_files_dict_keys(self):
        """Test for incompatible `files_dict` parameter."""
        self.assertRaises(
            KeyError,
            evaluation.diff_statistics,
            {
                "hello": 42,
                "world": 42,
            },
        )


class TestFileFormat(unittest.TestCase):
    """Test poorly formatted and incompatible data files.

    Test if the exceptions in `test_consistency.evaluation` and
    `pyglasma3d_numba_source.data_io` trigger properly and illegal files
    do not lead to bad evaluation data.
    """

    @classmethod
    def setUpClass(cls):
        """Perpare random data to work with."""

        # 560 = 4*4*5*7
        cls.values = 560

        rng = default_rng()
        # random floats in interval [-9, 9)
        cls.data = rng.random(cls.values)
        cls.data = 18 * cls.data - 9

    def generate_files(self, cols, chunk):
        """Return two tmp files with data layout from arguments."""

        data = np.reshape(self.data, (self.values // cols, cols))
        file_ref1 = tempfile.TemporaryFile("w+")
        file_ref2 = tempfile.TemporaryFile("w+")
        np.savetxt(
            file_ref1,
            data,
            delimiter=" ",
            header=f"{chunk}\n{(self.values // cols)//chunk+1}",
            comments="",
        )
        np.savetxt(
            file_ref2,
            data,
            delimiter=" ",
            header=f"{chunk}\n{(self.values // cols)//chunk+1}",
            comments="",
        )
        file_ref1.seek(0)
        file_ref2.seek(0)

        return file_ref1, file_ref2

    def test_bad_lines_per_chunk(self):
        """Test for bad `NL` with `MT`x`NT` =/= #values."""

        # test for bad NL
        # this should leave the last chunk with 2 rows
        # if MT > 4*5*7 / 3
        chunk = 3
        cols = 4
        f1, f2 = self.generate_files(cols, chunk)
        self.assertRaises(
            ValueError,
            evaluation.diff_statistics,
            {
                "source1": f1.fileno(),
                "source2": f2.fileno(),
            },
        )
        del f1, f2
        gc.collect()

    def test_bad_chunk_repetition(self):
        """Test for bad `MT`, but good `NT` and col layout."""

        # test for bad MT
        chunk = 4
        cols = 7
        f1, f2 = self.generate_files(cols, chunk)
        self.assertRaises(
            ValueError,
            evaluation.diff_statistics,
            {
                "source1": f1.fileno(),
                "source2": f2.fileno(),
            },
        )
        del f1, f2
        gc.collect()

    def test_varied_cols_per_chunk(self):
        """Test for change in columns per chunk but correct `MT`."""

        chunks = (1, 5, 7)
        # 4+5+7 = 16; 560 / 16 = 35
        cols = (4, 5, 7)
        # test varying number of cols per chunk
        for chunk in chunks:
            file_ref1 = tempfile.TemporaryFile("w+")
            file_ref2 = tempfile.TemporaryFile("w+")
            # make sure MT is large enough
            file_ref1.write(f"{chunk}\n{self.values // np.min(cols) // chunk}\n")
            file_ref2.write(f"{chunk}\n{self.values // np.min(cols) // chunk}\n")

            # write the file with varying cols
            for t in range(self.values // np.sum(cols) // chunk):
                cstep = 0
                for c in cols:
                    idx = t * np.sum(cols) * chunk + cstep * chunk
                    data = np.reshape(self.data[idx : idx + c * chunk], (chunk, c))
                    np.savetxt(file_ref1, data, delimiter=" ")
                    np.savetxt(file_ref2, data, delimiter=" ")
                    cstep += c
            file_ref1.seek(0)
            file_ref2.seek(0)
            self.assertRaises(
                ValueError,
                evaluation.diff_statistics,
                {
                    "source1": file_ref1.fileno(),
                    "source2": file_ref2.fileno(),
                },
            )
            del file_ref1, file_ref2
            gc.collect()

    def test_header(self):
        """Test if badly formatted header values are caught."""

        headers = (
            "hello world\n42\n",
            "1 2\n1\n",
            "1 h\n1\n",
            "42\nhello world\n",
            "1\n1 2\n",
            "1\n1 h\n",
        )
        for h in headers:
            file_ref1 = tempfile.TemporaryFile("w+")
            file_ref2 = tempfile.TemporaryFile("w+")

            file_ref1.write(h)
            file_ref2.write(h)

            file_ref1.seek(0)
            file_ref2.seek(0)

            self.assertRaises(
                ValueError,
                evaluation.diff_statistics,
                {
                    "source1": file_ref1.fileno(),
                    "source2": file_ref2.fileno(),
                },
            )

            del file_ref1, file_ref2
            gc.collect()


class TestPlotting(unittest.TestCase):
    """Test `test_consistency.consistency_tests` plotting routines."""

    @classmethod
    def setUpClass(cls):
        """Prepare temporary files with input data and for output."""

        # use named tmp files to be able to pass them on os level
        # output files
        cls.data_file = tempfile.NamedTemporaryFile("w+", delete=False)
        cls.data_file.close()
        cls.plot_file = tempfile.NamedTemporaryFile("w+b", delete=False, suffix=".png")
        cls.plot_file.close()

        cls.values = 560
        cls.chunks = (1, 4, 7)
        cls.columns = (1, 4, 5)
        cls.chulumns = [(ch, co) for ch in cls.chunks for co in cls.columns]

        # generate reference data
        rng = default_rng()
        # random floats in interval [-9, 9)
        cls.ref_base = rng.random(cls.values)
        cls.ref_base = 18 * cls.ref_base - 9
        # random integers in interval [-30, 6]
        cls.ref_expo = rng.integers(
            low=-30, high=6, size=cls.values, dtype=np.int8, endpoint=True
        )
        cls.ref_data = cls.ref_base * np.float_power(10.0, cls.ref_expo)

        # generate test data
        # add a random sign
        sign = np.ones(cls.values)
        sign[: sign.size // 2 + 1] = -1
        rng.shuffle(sign)

        # fix base to 1.11111111...
        base = 10 / 9 * sign
        # normal sampled values mu, sigma
        expo = rng.integers(low=-10, high=-5)
        cls.test_data = cls.ref_data + base * np.float_power(10.0, cls.ref_expo + expo)

        # prepare data files
        cls.ref_data_file = tempfile.NamedTemporaryFile("w+", delete=False)
        cls.ref_data_file.close()
        cls.test_data_file = tempfile.NamedTemporaryFile("w+", delete=False)
        cls.test_data_file.close()

    @classmethod
    def tearDownClass(cls):
        os.remove(cls.data_file.name)
        os.remove(cls.plot_file.name)
        os.remove(cls.ref_data_file.name)
        os.remove(cls.test_data_file.name)

    def save_to_file(self, file_path, data, chunk, column):
        """Fill file_path with data arranged in columns.

        The output format is compatible with
        `pyglasma3d_numba_source.data_io` and contains the header lines.
        """
        with open(file_path, "w") as f:
            data = np.reshape(data, (self.values // column, column))
            np.savetxt(
                f,
                data,
                delimiter=" ",
                header=f"{chunk}\n{(self.values // column)//chunk}",
                comments="",
            )

    def test_data_output(self):
        """Test if consistency_test.py works for sample data files."""

        for chunk, col in self.chulumns:
            self.save_to_file(self.ref_data_file.name, self.ref_data, chunk, col)
            self.save_to_file(self.test_data_file.name, self.test_data, chunk, col)
            # implicitly checks for the exit code
            subprocess.run(
                [
                    "coverage",
                    "run",
                    "test_consistency/consistency_tests.py",
                    "--files",
                    self.ref_data_file.name,
                    self.test_data_file.name,
                    "--output",
                    self.data_file.name,
                    "--graphs",
                    self.plot_file.name,
                    "--precision",
                    "1",
                    "--verbose",
                ],
                check=True,
                stdout=subprocess.DEVNULL,
            )


if __name__ == "__main__":
    TestRunner.main()
