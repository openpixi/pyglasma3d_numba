# Python Package [telemetry][folder-link]

The [`telemetry`][folder-link] package provides a full telemetry suite for analyzing [simulation setup][sst-link]  
runs. The suite uses third party logging tools and system information files to generate  
6 logfiles. Those logfiles are then passed on to the [`telemetry_plots`][plot-link] 
module to  
generate 6 plots.

The [User Guide][this-wiki-link] is provided via the [repository Wiki][wiki-link]. It contains detailed instructions  
and background information as well as samples for logfiles and plots.

#### DISCLAIMER:  
The telemetry data collected by this suite does not represent perfectly accurate profiling data!


### Requirements

The contents of this package are based on linux kernel 4.19.12. They have also been tested  
with the latest kernel version at the time of writing: 5.5.4

This [telemetry][folder-link] package requires:  
- Python version 3.8.1 or higher

Additionally the following external Python modules are required:

| Package                       | Version &#8805; |
| ----------------------------- | :-------------: |
| [matplotlib][matplotlib-link] |      3.1.2      |
| [numpy][numpy-link]           |     1.18.1      |

Additionally the following system tools and drivers are required:

| Package                                                                  | Version &#8805; |
| ------------------------------------------------------------------------ | :-------------: |
| [mpstat][mpstat-link] (providing [sysstat][sysstat-link])                |     11.4.3      |
| proprietary [Nvidia driver][nvdriver] (providing [nvidia-smi][smi-link]) |     440.40      |


## Quick-Start Guide

#### [`start_telemetry_run.sh`][start-link] Script

The [`start_telemetry_run.sh`][start-link] script combines the execution of a [simulation setup][sst-link]  
with the recording of the logging data and saves the generated plots to the folder of  
the logfiles.

Call this script it directly from the command line:
```
$ ./start_telemetry_run.sh
```
More information can be found in the section ['Bash Script `start_telemetry_run.sh`'][start-wiki-link]  
of the [repository Wiki][wiki-link].

#### [`telemetry_plots`][plot-link] Module

The [`telemetry_plots`][plot-link] module is a tool to plot existing telemetry data collected by the  
[`start_telemetry_run.sh`][start-link] script or equivalent calls to the logging tools used by that  
script. The [`start_telemetry_run.sh`][start-link] script uses this module.

This module can be called directly from the command line:
```
$ ./telemetry_plots.py \
--files sys_mem_xxxxxxxxxxxxxxxxxxx.dat sys_util_xxxxxxxxxxxxxxxxxxx.dat \
gpu_xxxxxxxxxxxxxxxxxxx.dat gpu_mem_xxxxxxxxxxxxxxxxxxx.dat \
gpu_mem_long_xxxxxxxxxxxxxxxxxxx.dat gpu_long_xxxxxxxxxxxxxxxxxxx.dat \
ENDTIME_xxxxxxxxxxxxxxxxxxx.dat STARTTIME_xxxxxxxxxxxxxxxxxxx.dat \
--pname python3_mv_gpu
```

Additionally this module provides the class `Plots` for integrating the plotting 
functionality  
in custom code. It can be imported directly from the [telemetry package][folder-link]:
```python
from telemetry import Plots
```
More information can be found in the section ['Python Module `telemetry_plots`'][plot-wiki-link]  
of the [repository Wiki][wiki-link].


## Logged Data

The logged data contains:
- CPU utilization for each core and average over all cores
- System memory usage
- GPU utilization
- GPU memory utilization and usage

In addition to system telemetry data, 5 logfiles containing information about the  
simulation run are saved. These include:
- Messages to `stdout` (the simulation log)
- Configuration parameters for the [simulation setup][sst-link]
- Starttime, endtime and duration measurements


[//]: # (references: )  
[folder-link]: telemetry/
[root-link]: ./
[start-link]: telemetry/start_telemetry_run.sh
[plot-link]: telemetry/telemetry_plots.py
[sst-link]: examples/
[wiki-link]: https://openpixi.gitlab.io/pyglasma3d_numba/
[this-wiki-link]: https://openpixi.gitlab.io/pyglasma3d_numba/docs/pyglasma3d-numba/telemetry/
[start-wiki-link]: https://openpixi.gitlab.io/pyglasma3d_numba/docs/pyglasma3d-numba/telemetry/telemetry-start/
[plot-wiki-link]: https://openpixi.gitlab.io/pyglasma3d_numba/docs/pyglasma3d-numba/telemetry/telemetry-plots/
[matplotlib-link]: https://pypi.org/project/matplotlib/
[numpy-link]: https://pypi.org/project/numpy/
[mpstat-link]: http://man7.org/linux/man-pages/man1/mpstat.1.html "Link to manpage for 'mpstat(1)'"
[sysstat-link]: http://sebastien.godard.pagesperso-orange.fr/features.html "Link to homepage of 'sysstat'"
[smi-link]: https://developer.nvidia.com/nvidia-system-management-interface "Link to homepage of 'nvidia-smi'"
[nvdriver]: https://www.nvidia.com/en-us/drivers/unix/ "Link to nvidia unix drivers"
