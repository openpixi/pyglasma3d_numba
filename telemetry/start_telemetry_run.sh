#!/usr/bin/env bash

########################################################################
# This script executes the specified simulation setup `mv.py` while
# monitoring system resource usage. After the simulation setup finishes,
# plots for the telemetry data recorded during the run are created using
# the module `telemetry_plots.py`.
#
# The resulting telemetry data must not be takes as profiling data,
# as it lacks the necessary accuracy!
#
# Required resource location of the code (clone of git repository):
# $HOME/pyglasma3d_numba/
#
# Folder where generated logs and plots will be put:
# $HOME/pyglasma3d_numba_stats/$(timestamp)
# If not present, this directory will be created.
########################################################################

# exit script on error
set -e

# define function to kill all bg jobs
function kill_bg() {
  # only kill if there are jobs running in bg
  if [ $(jobs | wc -l) -ne 0 ]; then
    # terminate all background jobs
    kill -s SIGTERM $(jobs -p) > /dev/null
    # suppress 'terminated' message
    wait $(jobs -rp) 2> /dev/null
  fi
}

# set a trap for ERR to kill all bg jobs
trap kill_bg ERR

# check if code resources are accessible
if [ ! -d "$HOME/pyglasma3d_numba" ]; then
  echo -e "\nERROR\ncode resource location '${HOME}/pyglasma3d_numba' does not exits\n"
  exit 1
fi

# create custom python interpreter symlink to rename process
if ! ln -s -f $(which python3) "$(which python3)_mv_gpu"; then
  echo -e "\n\nFailed to create custom symlink for python. \
           \nPlease make sure you have writing permissions on the location of python3: \
           \n$(ls -alh $(which python3)) \
           \nThis problem will generally not occur, when using a virtual environment. \
           \nDo not use 'sudo'! This could create a symlink in undesired locations!"
  exit 1
fi

echo -e "\nsetting up telemetry files\n"

# get the starttime for the scripts
now=$(date +%Y-%m-%d_%H-%M-%S)

# create new directory for logs
mkdir -p $HOME/pyglasma3d_numba_stats/$now

# prepare / truncate logfiles
:> $HOME/pyglasma3d_numba_stats/$now/sys_mem_$now.dat
:> $HOME/pyglasma3d_numba_stats/$now/sys_util_$now.dat
:> $HOME/pyglasma3d_numba_stats/$now/gpu_$now.dat
:> $HOME/pyglasma3d_numba_stats/$now/gpu_mem_$now.dat
:> $HOME/pyglasma3d_numba_stats/$now/gpu_mem_long_$now.dat
:> $HOME/pyglasma3d_numba_stats/$now/gpu_long_$now.dat

# if setup is modified (command line args given), write args to file
if [ $# -ne 0 ]; then
  echo $* > $HOME/pyglasma3d_numba_stats/$now/CMD_ARGS_$now.dat
fi

# index of core to bind loggers
CPU_IDX=1

# list for background PIDs
bg_PID=( )

# gpu utilization
# output format:
# YYYY/MM/DD HH:MM:SS.mmm, GPU_util, MEM_util, MEM_used, MEM_total
taskset -c $CPU_IDX nvidia-smi \
--query-gpu=timestamp,utilization.gpu,utilization.memory,memory.used,memory.total \
--format=csv,noheader,nounits -lms 500 \
-f $HOME/pyglasma3d_numba_stats/$now/gpu_long_$now.dat &
# save pid
bg_PID+=( $! )

# gpu memory used by process running
# output format: YYYY/MM/DD HH:MM:SS.mmm, P_name, MEM_used
taskset -c $CPU_IDX nvidia-smi --query-compute-apps=timestamp,process_name,used_memory \
--format=csv,noheader,nounits -lms 500 \
-f $HOME/pyglasma3d_numba_stats/$now/gpu_mem_long_$now.dat &
# save pid
bg_PID+=( $! )

# system memory usage
# output format (columns):
# YYYY/MM/DD HH:MM:SS   "MemTotal:" MEM_tot "kB"   "MemFree:" MEM_free "kB"
#       "Buffers:" Buff "kB"    "Cached:" cache "kB"   "Shmem:" shmem "kB"
#       "SReclaimable:" src "kB"
# memory used by system (as seen by htop):
# MEM_tot - MemFree - Buff - Cached - SReclaimable + Shmem
taskset -c $CPU_IDX bash -c "while sleep 1; do \
(date +%Y/%m/%d\ %T && sed -n -e 1,2p -e 4,5p -e 21p -e 23p /proc/meminfo) | \
tr '\n' '\t'; echo; done >> $HOME/pyglasma3d_numba_stats/$now/sys_mem_$now.dat" &
# save pid
bg_PID+=( $! )

# system utilization
# output format (columns):
# HH:MM:SS XM   CPU_id   %usr   %nice   %sys   %iowait   %irq   %soft   %steal
#     %guest   %gnice   %idle
# line 00 is different
# for each block:
# first line headers second line avg over all cores, next lines for each core
export S_TIME_FORMAT='ISO'
taskset -c $CPU_IDX mpstat -u -P ALL 1 >> \
$HOME/pyglasma3d_numba_stats/$now/sys_util_$now.dat &
# save pid
bg_PID+=( $! )

# gpu utilization 166 ms resolution
# output format:
# GPU_id, gpuUtil, us, util_percent
taskset -c $CPU_IDX nvidia-smi stats -d gpuUtil \
-f $HOME/pyglasma3d_numba_stats/$now/gpu_$now.dat &
# save pid
bg_PID+=( $! )

# gpu memory utilization 166 ms resolution
# output format:
# GPU_id, memUtil, us, util_percent
taskset -c $CPU_IDX nvidia-smi stats -d memUtil \
-f $HOME/pyglasma3d_numba_stats/$now/gpu_mem_$now.dat &
# save pid
bg_PID+=( $! )

# check if any of the bg jobs failed
for pid in ${bg_PID[@]}; do
  if ! ps -p $pid > /dev/null ; then
    # kill all bg jobs and exit non-zero
    kill_bg
    exit 1
  fi
done

echo -e "\nall loggers running\n"
echo -e "\nstarting simulation run\n"

# get some values before sim starts
sleep 2

# prep for sim run
cd $HOME/pyglasma3d_numba
# log starttime for sim
date +%Y-%m-%d_%H-%M-%S.%N > $HOME/pyglasma3d_numba_stats/$now/STARTTIME_$now.dat

# call the GNU time command on python sim run with arguments passed on
$HOME/.conda/envs/pyglasma3d_numba/bin/time \
-o $HOME/pyglasma3d_numba_stats/$now/DURATION_$now.dat \
-f "real %e\nusr %U\nsys %S" \
python3_mv_gpu -m examples.mv --sysinfo $* > \
$HOME/pyglasma3d_numba_stats/$now/SIMLOG_$now.dat

# log endtime for sim
date +%Y-%m-%d_%H-%M-%S.%N > $HOME/pyglasma3d_numba_stats/$now/ENDTIME_$now.dat

# extend telemetry some seconds to have normal data at the end
sleep 2

echo -e "\nsimulation run finished\n"
echo -e "\nquitting telemetry log\n"

# terminate all background monitoring jobs
kill_bg

echo -e "\nplotting graphs\n"

# change to directory with logs from current run
cd $HOME/pyglasma3d_numba_stats/$now

# call module for the plots
$HOME/pyglasma3d_numba/telemetry/telemetry_plots.py \
--files sys_mem_$now.dat sys_util_$now.dat gpu_$now.dat gpu_mem_$now.dat \
gpu_mem_long_$now.dat gpu_long_$now.dat ENDTIME_$now.dat STARTTIME_$now.dat \
--pname=python3_mv_gpu

echo -e "\nall plots done\n\n\nscript finished\n\n"
