# Docker Containers for Pyglasma3d_Numba
The provided `Dockerfile` builds an image that runs the benchmark as a command 
line executable. Arguments appended to the `docker run` command will be forwarded 
to the `run_benchmark.sh` script.
