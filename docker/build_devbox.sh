#!/usr/bin/env bash

########################################################################
# This script is used to build the "devbox" Docker container. This
# container is used for testing during development.
#
# This script accepts one argument which will replace <tag> in the
# built Docker image tag: pyglasma3d_numba:<tag>-devbox. It has to be
# a valid string for tagging Docker images.
########################################################################

# exit script on error
set -eE

# change current working dir to base folder of pyglasma3d_numba
cd -P -- "$(dirname -- "${BASH_SOURCE[0]}")"
cd ..

docker build --tag pyglasma3d_numba:${1-custom}-devbox --file docker/Dockerfile.devbox .
