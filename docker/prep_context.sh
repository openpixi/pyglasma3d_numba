#!/usr/bin/env sh

cp ../benchmark/run_benchmark.sh .
cp ../benchmark/SHA512SUMS.sha512 .
cp ../benchmark/mv_test_setup.py .
cp ../benchmark/mv_init_bench.py .
cp ../benchmark/mv_full_bench.py .
cp ../benchmark/mv_evolve_bench.py .
cp ../benchmark/cython-pip-requirements.txt .
cp ../benchmark/conda-cython-environment.yml .

cp ../conda-environment.yml .
